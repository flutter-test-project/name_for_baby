import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:name_for_baby/app/data/collections/firestore_collections.dart';
import 'package:name_for_baby/features/preference_by_name/domain/i_preference_by_name_repository.dart';

/// Реализация репозитория для получения данных предпочтения имен пользователем из удаленного зранилища
/// Для авторизованных пользователей
class PreferenceByNameRepository implements IPreferenceByNameRepository {
  final FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  Future<List<String>> getFavoriteNameIds({
    required String userLogin,
  }) async {
    DocumentSnapshot<Map<String, dynamic>>? data =
        await db.collection(FirestoreCollections.userData).doc(userLogin).get();
    List<String> favoriteNameIds = [];
    if (data.exists == false) return const [];
    final Map<String, dynamic> map = data.data() ?? {};

    map.forEach((key, value) {
      if (key == FirestoreUsersFields.favoriteNameIds) {
        if (value is List) {
          favoriteNameIds = List<String>.from(value);
        }
      }
    });

    return favoriteNameIds;
  }

  @override
  Future<List<String>> getNotFavoriteNameIds({
    required String userLogin,
  }) async {
    DocumentSnapshot<Map<String, dynamic>>? data =
        await db.collection(FirestoreCollections.userData).doc(userLogin).get();
    List<String> notFavoriteNameIds = [];
    final Map<String, dynamic> map = data.data() ?? {};

    map.forEach((key, value) {
      if (key == FirestoreUsersFields.notFavoriteNameIds) {
        if (value is List) {
          notFavoriteNameIds = List<String>.from(value);
        }
      }
    });

    return notFavoriteNameIds;
  }

  @override
  Future<void> updateFavoriteNameIds({
    required List<String> favoriteNameIds,
    required String userLogin,
  }) async {
    DocumentSnapshot<Map<String, dynamic>>? data =
        await db.collection(FirestoreCollections.userData).doc(userLogin).get();
    if (data.exists == false) {
      // создать коллекцию данных пользователя
      await db.collection(FirestoreCollections.userData).doc(userLogin).set(
        {
          FirestoreUsersFields.favoriteNameIds: favoriteNameIds,
        },
      );
    } else {
      // Добавить данные понравившихся имен в коллекцию
      await db.collection(FirestoreCollections.userData).doc(userLogin).update(
        {
          FirestoreUsersFields.favoriteNameIds: favoriteNameIds,
        },
      );
    }
  }

  @override
  Future<void> updateNotFavoriteNameIds({
    required List<String> notFavoriteNameIds,
    required String userLogin,
  }) async {
    DocumentSnapshot<Map<String, dynamic>>? data =
        await db.collection(FirestoreCollections.userData).doc(userLogin).get();
    if (data.exists == false) {
      // создать коллекцию данных пользователя
      await db.collection(FirestoreCollections.userData).doc(userLogin).set(
        {
          FirestoreUsersFields.notFavoriteNameIds: notFavoriteNameIds,
        },
      );
    } else {
      // Добавить данные понравившихся имен в коллекцию
      await db.collection(FirestoreCollections.userData).doc(userLogin).update(
        {
          FirestoreUsersFields.notFavoriteNameIds: notFavoriteNameIds,
        },
      );
    }
  }
}
