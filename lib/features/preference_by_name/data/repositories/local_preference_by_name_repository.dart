import 'package:name_for_baby/features/preference_by_name/domain/i_local_preference_by_name_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Реализация репозитория для получения данных предпочтения имен пользователем хранящихся на устройсвк локально
/// Для НЕ авторизованных пользователей
class LocalPreferenceByNameRepository implements ILocalPreferenceByNameRepository {
  LocalPreferenceByNameRepository({
    required SharedPreferences prefs,
  }) : _prefs = prefs;

  final SharedPreferences _prefs;

  @override
  List<String> getFavoriteNameIds() {
    return _prefs.getStringList(DataAccessKeyStorage.favoriteNameId) ?? [];
  }

  @override
  List<String> getNotFavoriteNameIds() {
    return _prefs.getStringList(DataAccessKeyStorage.notFavoriteNameId) ?? [];
  }

  @override
  Future<void> saveFavoriteNameId(String nameId) async {
    if (!getFavoriteNameIds().contains(nameId)) {
      List<String> list = getFavoriteNameIds()..add(nameId);
      await _prefs.setStringList(
        DataAccessKeyStorage.favoriteNameId,
        list,
      );
    }
  }

  @override
  Future<void> saveNotFavoriteNameId(String nameId) async {
    if (!getNotFavoriteNameIds().contains(nameId)) {
      List<String> list = getNotFavoriteNameIds()..add(nameId);
      await _prefs.setStringList(
        DataAccessKeyStorage.notFavoriteNameId,
        list,
      );
    }
  }

  @override
  Future<void> deleteFavoriteNameId(String nameId) async {
    if (!getFavoriteNameIds().contains(nameId)) {
      List<String> list = getFavoriteNameIds()..remove(nameId);
      await _prefs.setStringList(
        DataAccessKeyStorage.favoriteNameId,
        list,
      );
    }
  }

  @override
  Future<void> deleteNotFavoriteNameId(String nameId) async {
    if (!getNotFavoriteNameIds().contains(nameId)) {
      List<String> list = getNotFavoriteNameIds()..remove(nameId);
      await _prefs.setStringList(
        DataAccessKeyStorage.notFavoriteNameId,
        list,
      );
    }
  }

  @override
  Future<void> cleanData() async {
    await _prefs.remove(DataAccessKeyStorage.favoriteNameId);
    await _prefs.remove(DataAccessKeyStorage.notFavoriteNameId);
  }
}

/// Хранилище ключей доступов к данным
class DataAccessKeyStorage {
  static const String favoriteNameId = 'favorite_name_id';
  static const String notFavoriteNameId = 'not_favorite_name_id';
}
