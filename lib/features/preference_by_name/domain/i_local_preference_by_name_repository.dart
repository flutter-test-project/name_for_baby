/// Абстрактный репозиторий для получения данных предпочтения имен пользователем хранящихся на устройсвк локально
/// Для НЕ авторизованных пользователей
abstract interface class ILocalPreferenceByNameRepository {
  /// Метод получения списка понравившихся ид имен
  List<String> getFavoriteNameIds();

  /// Метод получения списка не понравившихся ид имен
  List<String> getNotFavoriteNameIds();

  /// Метод сохранения id понравившегося имени
  ///
  /// Принимает:
  /// - [nameId] - id имени
  Future<void> saveFavoriteNameId(String nameId);

  /// Метод сохранения id не понравившегося имени
  ///
  /// Принимает:
  /// - [nameId] - id имени
  Future<void> saveNotFavoriteNameId(String nameId);

  /// Метод удаления из списка понравивщизся имен элемента
  Future<void> deleteFavoriteNameId(String nameId);

  /// Метод удаления из списка Не понравивщизся имен элемента
  Future<void> deleteNotFavoriteNameId(String nameId);

  /// Метод который очищает все данные предпочтительных имен пользователя
  Future<void> cleanData();
}
