/// Абстрактный репозиторий для получения данных предпочтения имен пользователем
/// Для авторизованных пользователей
abstract interface class IPreferenceByNameRepository {
  /// Метод который возращает список понравившихся имен
  Future<List<String>> getFavoriteNameIds({
    required String userLogin,
  });

  /// Метод который возращает список не понравившихся имен
  Future<List<String>> getNotFavoriteNameIds({
    required String userLogin,
  });

  /// Метод который сохраняет указанную коллекцию id понравившихся имен в удаленную бд
  ///
  /// Принимает
  /// - [favoriteNameIds] - список id понравившихся имен
  /// - [userLogin] - логин пользователя
  Future<void> updateFavoriteNameIds({
    required List<String> favoriteNameIds,
    required String userLogin,
  });

  /// Метод который сохраняет указанную коллекцию id не понравившихся имен в удаленную бд
  ///
  /// Принимает
  /// - [favoriteNameIds] - список id понравившихся имен
  /// - [userLogin] - логин пользователя
  Future<void> updateNotFavoriteNameIds({
    required List<String> notFavoriteNameIds,
    required String userLogin,
  });
}
