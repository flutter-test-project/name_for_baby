import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/i_local_preference_by_name_repository.dart';
import 'package:name_for_baby/features/preference_by_name/domain/i_preference_by_name_repository.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';

/// Юз кейс получения НЕ поравившихся id имен пользователя
class GetNotFavoriteNamesUsecase extends BaseUseCase<Future<List<String>>, NoParams> {
  GetNotFavoriteNamesUsecase({
    required ILocalPreferenceByNameRepository localPreferenceByNameRepository,
    required IPreferenceByNameRepository preferenceByNameRepository,
    required IUserSecureDataRepository userSecureDataRepository,
  })  : _localPreferenceByNameRepository = localPreferenceByNameRepository,
        _preferenceByNameRepository = preferenceByNameRepository,
        _userSecureDataRepository = userSecureDataRepository;

  final ILocalPreferenceByNameRepository _localPreferenceByNameRepository;
  final IPreferenceByNameRepository _preferenceByNameRepository;
  final IUserSecureDataRepository _userSecureDataRepository;

  @override
  Future<List<String>> call(NoParams params) async {
    final isUserAuthentication = await _userSecureDataRepository.isUserAuthentication;
    if (isUserAuthentication) {
      final userLogin = await _userSecureDataRepository.getLogin;
      return _preferenceByNameRepository.getNotFavoriteNameIds(userLogin: userLogin);
    } else {
      return _localPreferenceByNameRepository.getNotFavoriteNameIds();
    }
  }
}
