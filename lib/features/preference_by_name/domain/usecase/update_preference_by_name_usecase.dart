import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/i_local_preference_by_name_repository.dart';
import 'package:name_for_baby/features/preference_by_name/domain/i_preference_by_name_repository.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';

/// Юз кейс который обновляет данные коллекции прелпочтений по имени пользователя
/// (ТОлько для авторизованного пользователя)
class UpdatePreferenceByNameUsecase extends BaseUseCase<Future<void>, NoParams> {
  UpdatePreferenceByNameUsecase({
    required IPreferenceByNameRepository preferenceByNameRepository,
    required IUserSecureDataRepository userSecureDataRepository,
    required ILocalPreferenceByNameRepository localPreferenceByNameRepository,
  })  : _preferenceByNameRepository = preferenceByNameRepository,
        _userSecureDataRepository = userSecureDataRepository,
        _localPreferenceByNameRepository = localPreferenceByNameRepository;

  final IPreferenceByNameRepository _preferenceByNameRepository;
  final IUserSecureDataRepository _userSecureDataRepository;
  final ILocalPreferenceByNameRepository _localPreferenceByNameRepository;

  @override
  Future<void> call(NoParams params) async {
    final bool isUserAuthentication = await _userSecureDataRepository.isUserAuthentication;
    if (!isUserAuthentication) {
      Exception('Данная операция предназначена только для зарегестрированных пользователей');
    }
    final String userLogin = await _userSecureDataRepository.getLogin;

    final List<String> favoriteNameIds = await _getFullFavoriteNameIds(
      userLogin: userLogin,
      favoriteNameIds: _localPreferenceByNameRepository.getFavoriteNameIds(),
    );

    final List<String> notFavoriteNameIds = await _getFullNotFavoriteNameIds(
      userLogin: userLogin,
      notFavoriteNameIds: _localPreferenceByNameRepository.getNotFavoriteNameIds(),
    );

    await _preferenceByNameRepository.updateFavoriteNameIds(
      userLogin: userLogin,
      favoriteNameIds: favoriteNameIds,
    );

    await _preferenceByNameRepository.updateNotFavoriteNameIds(
      userLogin: userLogin,
      notFavoriteNameIds: notFavoriteNameIds,
    );

    // так как пользователь был авторизован и коллекция с данными предаочтительных имен была создана
    // удаляем данные предпочтительных имен из кеша

    await _localPreferenceByNameRepository.cleanData();
  }

  /// Метод возрвщает полный список понравивщихся имен (новые имена + уже сохраненные)
  Future<List<String>> _getFullFavoriteNameIds({
    required String userLogin,
    required List<String> favoriteNameIds,
  }) async {
    List<String> fullList = List.from(favoriteNameIds);
    final List<String> oldFavoriteNameIds =
        await _preferenceByNameRepository.getFavoriteNameIds(userLogin: userLogin);

    for (final oldNames in oldFavoriteNameIds) {
      fullList.add(oldNames);
    }
    return fullList.toSet().toList();
  }

  /// Метод возрвщает полный список не понравивщихся имен (новые имена + уже сохраненные)
  Future<List<String>> _getFullNotFavoriteNameIds({
    required String userLogin,
    required List<String> notFavoriteNameIds,
  }) async {
    List<String> fullList = List.from(notFavoriteNameIds);
    final List<String> oldFavoriteNameIds =
        await _preferenceByNameRepository.getNotFavoriteNameIds(userLogin: userLogin);

    for (final oldNames in oldFavoriteNameIds) {
      fullList.add(oldNames);
    }
    return fullList.toSet().toList();
  }
}
