import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/i_local_preference_by_name_repository.dart';
import 'package:name_for_baby/features/preference_by_name/domain/i_preference_by_name_repository.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';

/// Юз кейс который сохраняет id понравившегося имени
class SaveFavoriteNameIdUsecase extends BaseUseCase<Future<void>, String> {
  SaveFavoriteNameIdUsecase({
    required ILocalPreferenceByNameRepository localPreferenceByNameRepository,
    required IPreferenceByNameRepository preferenceByNameRepository,
    required IUserSecureDataRepository userSecureDataRepository,
  })  : _localPreferenceByNameRepository = localPreferenceByNameRepository,
        _preferenceByNameRepository = preferenceByNameRepository,
        _userSecureDataRepository = userSecureDataRepository;

  final ILocalPreferenceByNameRepository _localPreferenceByNameRepository;
  final IPreferenceByNameRepository _preferenceByNameRepository;
  final IUserSecureDataRepository _userSecureDataRepository;

  @override
  Future<void> call(String params) async {
    final isUserAuthentication = await _userSecureDataRepository.isUserAuthentication;
    if (isUserAuthentication) {
      final userLogin = await _userSecureDataRepository.getLogin;
      final List<String> favoriteNameIds =
          await _preferenceByNameRepository.getFavoriteNameIds(userLogin: userLogin);
      await _preferenceByNameRepository.updateFavoriteNameIds(
        favoriteNameIds: favoriteNameIds..add(params),
        userLogin: userLogin,
      );
    } else {
      await _localPreferenceByNameRepository.saveFavoriteNameId(params);
    }
  }
}
