import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/i_local_preference_by_name_repository.dart';
import 'package:name_for_baby/features/preference_by_name/domain/i_preference_by_name_repository.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';

/// Юз кейс по удалению понравившкгося имени из списка понравивщихся имен
class DeleteFavoriteNameUsecase extends BaseUseCase<Future<void>, String> {
  DeleteFavoriteNameUsecase({
    required ILocalPreferenceByNameRepository localPreferenceByNameRepository,
    required IPreferenceByNameRepository preferenceByNameRepository,
    required IUserSecureDataRepository userSecureDataRepository,
  })  : _localPreferenceByNameRepository = localPreferenceByNameRepository,
        _preferenceByNameRepository = preferenceByNameRepository,
        _userSecureDataRepository = userSecureDataRepository;

  final ILocalPreferenceByNameRepository _localPreferenceByNameRepository;
  final IPreferenceByNameRepository _preferenceByNameRepository;
  final IUserSecureDataRepository _userSecureDataRepository;

  /// - [params] - id имени
  @override
  Future<void> call(String params) async {
    final isUserAuthentication = await _userSecureDataRepository.isUserAuthentication;
    if (isUserAuthentication) {
      final userLogin = await _userSecureDataRepository.getLogin;

      List<String> favoriteNameIds = List<String>.from(
        await _preferenceByNameRepository.getFavoriteNameIds(
          userLogin: userLogin,
        ),
      );

      favoriteNameIds.remove(params);
      await _preferenceByNameRepository.updateFavoriteNameIds(
        favoriteNameIds: favoriteNameIds,
        userLogin: userLogin,
      );
    } else {
      await _localPreferenceByNameRepository.deleteFavoriteNameId(params);
    }
  }
}
