import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/personal_list_names/domain/i_personal_list_names_repository.dart';
import 'package:name_for_baby/features/preference_by_name/domain/i_local_preference_by_name_repository.dart';
import 'package:name_for_baby/features/preference_by_name/domain/i_preference_by_name_repository.dart';
import 'package:name_for_baby/features/profile/domain/entities/count_preference_by_name_name.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';

/// юз кейс который возращает данные предпочтительных имен в разных категориях
class GetCountPreferenceByNameUsecase extends BaseUseCase<Future<CountPreferenceByName>, NoParams> {
  GetCountPreferenceByNameUsecase({
    required ILocalPreferenceByNameRepository localPreferenceByNameRepository,
    required IPreferenceByNameRepository preferenceByNameRepository,
    required IUserSecureDataRepository userSecureDataRepository,
    required IPersonalListNamesRepository personalListNamesRepository,
  })  : _localPreferenceByNameRepository = localPreferenceByNameRepository,
        _preferenceByNameRepository = preferenceByNameRepository,
        _userSecureDataRepository = userSecureDataRepository,
        _personalListNamesRepository = personalListNamesRepository;

  final ILocalPreferenceByNameRepository _localPreferenceByNameRepository;
  final IPreferenceByNameRepository _preferenceByNameRepository;
  final IUserSecureDataRepository _userSecureDataRepository;
  final IPersonalListNamesRepository _personalListNamesRepository;

  @override

  /// - [params] - показывает авторизован ли пользователь
  Future<CountPreferenceByName> call(NoParams params) async {
    final isUserAuthentication = await _userSecureDataRepository.isUserAuthentication;
    if (isUserAuthentication) {
      final userLogin = await _userSecureDataRepository.getLogin;
      final favoriteNameIds =
          await _preferenceByNameRepository.getFavoriteNameIds(userLogin: userLogin);
      final notFavoriteNameIds =
          await _preferenceByNameRepository.getNotFavoriteNameIds(userLogin: userLogin);

      final personalListNames =
          await _personalListNamesRepository.getPersonalNames(userLogin: userLogin);
      return CountPreferenceByName(
        countFavoriteName: favoriteNameIds.length,
        countNotFavoriteName: notFavoriteNameIds.length,
        countPersonalNames: personalListNames.length,
      );
    } else {
      return CountPreferenceByName(
        countFavoriteName: _localPreferenceByNameRepository.getFavoriteNameIds().length,
        countNotFavoriteName: _localPreferenceByNameRepository.getNotFavoriteNameIds().length,
        countPersonalNames: 0,
      );
    }
  }
}
