import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:name_for_baby/app/data/collections/firestore_collections.dart';
import 'package:name_for_baby/app/data/dto/name_dto.dart';
import 'package:name_for_baby/features/page_name/domain/services/i_page_name_remove_db_service.dart';

class PageNameFirestoreService implements IPageNameRemoveDbService {
  final FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  Future<NameDto?> getName(String nameId) async {
    final DocumentSnapshot<Map<String, dynamic>> doc = await db
        .collection(FirestoreCollections.nameCollection)
        .doc(nameId)
        .get();

    final Map<String, dynamic>? data = doc.data();
    if (data != null) {
      final NameDto nameDto = NameDto.fromFirestore(data, doc.id);
      return nameDto;
    } else {
      return null;
    }
  }
}
