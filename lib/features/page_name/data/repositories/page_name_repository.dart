import 'package:name_for_baby/app/data/dto/name_dto.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/features/names_book/data/mappers/name_mapper.dart';
import 'package:name_for_baby/features/page_name/domain/services/i_page_name_remove_db_service.dart';
import 'package:name_for_baby/features/page_name/domain/i_page_name_repository.dart';

class PageNameRepository implements IPageNameRepository {
  PageNameRepository({
    required IPageNameRemoveDbService pageNameRemoveDbService,
  }) : _pageNameRemoveDbService = pageNameRemoveDbService;
  final IPageNameRemoveDbService _pageNameRemoveDbService;

  @override
  Future<CollectorName?> getName({required String nameId}) async {
    final NameDto? nameDto = await _pageNameRemoveDbService.getName(nameId);
    if (nameDto != null) {
      return NameMapper().deserialize(nameDto);
    } else {
      return null;
    }
  }
}
