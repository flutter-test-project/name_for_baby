import 'package:name_for_baby/app/domain/entities/collector_name.dart';

abstract interface class IPageNameRepository {
  /// Возвращает  имя
  Future<CollectorName?> getName({required String nameId});
}
