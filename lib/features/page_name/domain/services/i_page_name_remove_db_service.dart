import 'package:name_for_baby/app/data/dto/name_dto.dart';

abstract interface class IPageNameRemoveDbService {
  /// вернет конкретный продукт из удаленной бд
  Future<NameDto?> getName(String nameId);
}
