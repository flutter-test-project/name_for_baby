part of 'page_name_cubit.dart';

/// Класс состояний странички имени
@immutable
sealed class PageNameState extends Equatable {
  @override
  List<Object> get props => [];
}

final class PageNameInitial extends PageNameState {}

final class PageNameInProgressLoading extends PageNameState {}

final class PageNameInProgressSuccess extends PageNameState {
  PageNameInProgressSuccess(this.name);

  final CollectorName name;
}

final class PageNameFailure extends PageNameState {
  PageNameFailure(this.text);

  final String text;
}
