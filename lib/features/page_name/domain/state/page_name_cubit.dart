import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/features/page_name/domain/i_page_name_repository.dart';

part 'page_name_state.dart';

class PageNameCubit extends Cubit<PageNameState> {
  PageNameCubit({
    required IPageNameRepository pageNameRepository,
  })  : _repository = pageNameRepository,
        super(PageNameInitial());

  final IPageNameRepository _repository;

  Future<void> initialization({
    required String nameId,
    CollectorName? name,
  }) async {
    try {
      emit(PageNameInProgressLoading());

      if (name != null) {
        emit(PageNameInProgressSuccess(name));
        return;
      }

      final CollectorName? collectorsName = await _repository.getName(nameId: nameId);
      if (collectorsName != null) {
        emit(PageNameInProgressSuccess(collectorsName));
        return;
      } else {
        emit(PageNameFailure('Не удалось найти странуцу этого имени.'));
      }
    } catch (error, stackTrace) {
      emit(PageNameFailure('$error'));
      addError(error, stackTrace);
    }
  }
}
