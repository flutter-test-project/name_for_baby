import 'package:flutter/cupertino.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/app/presentation/theme/nfb_text_styles.dart';
import 'package:name_for_baby/l10n/delegate.dart';

class CardNameContent extends StatelessWidget {
  const CardNameContent(this.collectorsName, {super.key});

  final CollectorName collectorsName;

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 18),
      children: [
        FullNamePanel(collectorsName.fullName),
        if (collectorsName.churchName.isNotEmpty) ChurchNamePanel(collectorsName.churchName),
        if (collectorsName.shortForms.isNotEmpty) ShortFormsNamePanel(collectorsName.shortForms),
        if (collectorsName.synonyms.isNotEmpty) SynonymsNamePanel(collectorsName.synonyms),
        OriginAndValue(
          origin: collectorsName.origin,
          value: collectorsName.value,
        ),
        if (collectorsName.personality.isNotEmpty) PersonalityPanel(collectorsName.personality),
        const IndentVertical(12.0),
      ],
    );
  }
}

class FullNamePanel extends StatelessWidget {
  const FullNamePanel(this.text, {super.key});

  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: Text.rich(
        TextSpan(
          text: '${l10n.fullNameText} ',
          style: NfbTextStyles.captionBold,
          children: [
            TextSpan(
              text: text,
              style: NfbTextStyles.captionMedium,
            ),
          ],
        ),
      ),
    );
  }
}

class ChurchNamePanel extends StatelessWidget {
  const ChurchNamePanel(this.text, {super.key});

  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: Text.rich(
        TextSpan(
          text: '${l10n.churchNameText} ',
          style: NfbTextStyles.captionBold,
          children: [
            TextSpan(
              text: text,
              style: NfbTextStyles.captionMedium,
            ),
          ],
        ),
      ),
    );
  }
}

class ShortFormsNamePanel extends StatelessWidget {
  const ShortFormsNamePanel(this.text, {super.key});

  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: Text.rich(
        TextSpan(
          text: '${l10n.shortFormNameText} ',
          style: NfbTextStyles.captionBold,
          children: [
            TextSpan(
              text: text,
              style: NfbTextStyles.captionMedium,
            ),
          ],
        ),
      ),
    );
  }
}

class SynonymsNamePanel extends StatelessWidget {
  const SynonymsNamePanel(this.text, {super.key});

  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: Text.rich(
        TextSpan(
          text: '${l10n.synonymsNameText} ',
          style: NfbTextStyles.captionBold,
          children: [
            TextSpan(
              text: text,
              style: NfbTextStyles.captionMedium,
            ),
          ],
        ),
      ),
    );
  }
}

class OriginAndValue extends StatelessWidget {
  const OriginAndValue({
    super.key,
    required this.origin,
    required this.value,
  });

  final String origin;
  final String value;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const IndentVertical(16.0),
        Text(
          l10n.originAndValueNameText,
          style: NfbTextStyles.body2Bold,
        ),
        const IndentVertical(8.0),
        Text(
          value,
          style: NfbTextStyles.captionBold,
        ),
        const IndentVertical(8.0),
        Text(
          origin,
          style: NfbTextStyles.captionMedium,
        ),
        const IndentVertical(16.0),
      ],
    );
  }
}

class PersonalityPanel extends StatelessWidget {
  const PersonalityPanel(this.text, {super.key});

  final String text;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          l10n.personalityText,
          style: NfbTextStyles.body1Bold,
        ),
        const IndentVertical(8.0),
        Text(
          text,
          style: NfbTextStyles.captionMedium,
        ),
      ],
    );
  }
}
