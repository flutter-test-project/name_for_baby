import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/presentation/components/dialogs/error_dialog.dart';
import 'package:name_for_baby/app/presentation/components/loading/loading_indicator.dart';
import 'package:name_for_baby/app/presentation/theme/nfb_text_styles.dart';
import 'package:name_for_baby/features/page_name/domain/state/page_name_cubit.dart';
import 'package:name_for_baby/features/page_name/presentation/components/widgets/card_name_content.dart';
import 'package:name_for_baby/l10n/delegate.dart';

class CardNameScreen extends StatelessWidget {
  const CardNameScreen({
    super.key,
    required this.nameId,
    required this.collectorName,
  });

  final String nameId;
  final CollectorName? collectorName;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => PageNameCubit(
        pageNameRepository: GetIt.instance(),
      ),
      child: NamePage(
        nameId: nameId,
        collectorsName: collectorName,
      ),
    );
  }
}

class NamePage extends StatefulWidget {
  const NamePage({
    super.key,
    required this.nameId,
    this.collectorsName,
  });

  final String nameId;
  final CollectorName? collectorsName;

  @override
  State<NamePage> createState() => _NamePageState();
}

class _NamePageState extends State<NamePage> {
  @override
  void initState() {
    context.read<PageNameCubit>().initialization(
          nameId: widget.nameId,
          name: widget.collectorsName,
        );
    super.initState();
  }

  String title = 'Карточка имени';

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PageNameCubit, PageNameState>(
      listener: (context, state) {
        if (state is PageNameFailure) {
          _showErrorDialog(state.text);
        }

        if (state is PageNameInProgressSuccess) {
          _updateTitle(state.name.fullName);
        }
      },
      builder: (context, state) {
        String title = l10n.cardNameTitle;
        Widget body;
        switch (state) {
          case PageNameInProgressLoading():
            body = const LoadingIndicator();
          case PageNameInProgressSuccess():
            body = CardNameContent(state.name);
            title = state.name.fullName;
          default:
            body = const SizedBox();
        }

        return Scaffold(
          appBar: AppBar(
            title: Text(
              title,
              style: NfbTextStyles.normalTitle,
            ),
          ),
          body: body,
        );
      },
    );
  }

  void _showErrorDialog(String errorText) {
    showErrorDialog(
      context: context,
      description: errorText,
    );
  }

  void _updateTitle(value) {
    setState(() {
      title = value;
    });
  }
}
