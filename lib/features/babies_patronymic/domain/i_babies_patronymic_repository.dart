import 'package:name_for_baby/app/domain/entities/babies_patronymic.dart';

/// Абстрактный репозиторий для работы с данными отчесва для ребенка
abstract interface class IBabiesPatronymicRepository {
  /// Метод который вернет данные для отчесва ребенка разных полов
  Future<BabiesPatronymic> getBabiesPatronymic({
    required String userLogin,
  });

  /// Метод сохранения данных отчесва ребенка разных полов
  Future<void> updateBabiesPatronymic({
    required String userLogin,
    required BabiesPatronymic babiesPatronymic,
  });
}
