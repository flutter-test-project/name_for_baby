import 'package:name_for_baby/app/domain/entities/babies_patronymic.dart';
import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';
import 'package:name_for_baby/features/babies_patronymic/domain/i_babies_patronymic_repository.dart';
import 'package:name_for_baby/l10n/delegate.dart';

/// Юз кейс получения данных отчесва для ребенка
class GetBabiesPatronymicUsecase extends BaseUseCase<Future<BabiesPatronymic>, NoParams> {
  GetBabiesPatronymicUsecase({
    required IUserSecureDataRepository userSecureDataRepository,
    required IBabiesPatronymicRepository babiesPatronymicRepository,
  })  : _userSecureDataRepository = userSecureDataRepository,
        _babiesPatronymicRepository = babiesPatronymicRepository;

  final IUserSecureDataRepository _userSecureDataRepository;
  final IBabiesPatronymicRepository _babiesPatronymicRepository;

  @override
  Future<BabiesPatronymic> call(NoParams params) async {
    final isUserAuthentication = await _userSecureDataRepository.isUserAuthentication;
    if (!isUserAuthentication) throw Exception(l10n.operationIsOnlyUserAuth);
    final userLogin = await _userSecureDataRepository.getLogin;
    return _babiesPatronymicRepository.getBabiesPatronymic(userLogin: userLogin);
  }
}
