import 'package:name_for_baby/app/domain/entities/babies_patronymic.dart';
import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';
import 'package:name_for_baby/features/babies_patronymic/domain/i_babies_patronymic_repository.dart';
import 'package:name_for_baby/l10n/delegate.dart';

class UpdateBabiesPatronymicUsecase extends BaseUseCase<Future<void>, BabiesPatronymic> {
  UpdateBabiesPatronymicUsecase({
    required IUserSecureDataRepository userSecureDataRepository,
    required IBabiesPatronymicRepository babiesPatronymicRepository,
  })  : _userSecureDataRepository = userSecureDataRepository,
        _babiesPatronymicRepository = babiesPatronymicRepository;
  final IUserSecureDataRepository _userSecureDataRepository;
  final IBabiesPatronymicRepository _babiesPatronymicRepository;

  @override
  Future<void> call(BabiesPatronymic params) async {
    final isUserAuthentication = await _userSecureDataRepository.isUserAuthentication;
    if (!isUserAuthentication) throw Exception(l10n.operationIsOnlyUserAuth);
    final userLogin = await _userSecureDataRepository.getLogin;
    await _babiesPatronymicRepository.updateBabiesPatronymic(
      userLogin: userLogin,
      babiesPatronymic: params,
    );
  }
}
