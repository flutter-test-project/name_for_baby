part of 'babies_patronymic_bloc.dart';

/// Класс событий для работы с отчеством
@immutable
sealed class BabiesPatronymicEvent {}

/// Событие загрузки данных
final class BabiesPatronymicStartedLoading extends BabiesPatronymicEvent {}

/// Млбытие обновления данных Отчесва для ребенка
final class BabiesPatronymicUpdatedData extends BabiesPatronymicEvent {
  BabiesPatronymicUpdatedData({
    required this.babiesPatronymic,
  });

  /// Отчесво для ребенка
  final BabiesPatronymic babiesPatronymic;
}
