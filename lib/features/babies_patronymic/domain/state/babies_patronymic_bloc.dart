import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/entities/babies_patronymic.dart';
import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/babies_patronymic/domain/usecase/get_babies_patronymic_usecase.dart';
import 'package:name_for_baby/features/babies_patronymic/domain/usecase/update_babies_patronymic_usecase.dart';
import 'package:name_for_baby/l10n/delegate.dart';

part 'babies_patronymic_event.dart';

part 'babies_patronymic_state.dart';

/// Блок по работе с данными отчества ребенка
class BabiesPatronymicBloc extends Bloc<BabiesPatronymicEvent, BabiesPatronymicState> {
  BabiesPatronymicBloc({
    required GetBabiesPatronymicUsecase getBabiesPatronymicUsecase,
    required UpdateBabiesPatronymicUsecase updateBabiesPatronymicUsecase,
  })  : _getBabiesPatronymicUsecase = getBabiesPatronymicUsecase,
        _updateBabiesPatronymicUsecase = updateBabiesPatronymicUsecase,
        super(BabiesPatronymicInitial()) {
    on<BabiesPatronymicStartedLoading>(_onLoadStarted);
    on<BabiesPatronymicUpdatedData>(_updateData);
  }

  final GetBabiesPatronymicUsecase _getBabiesPatronymicUsecase;
  final UpdateBabiesPatronymicUsecase _updateBabiesPatronymicUsecase;

  Future<void> _onLoadStarted(
    BabiesPatronymicStartedLoading event,
    Emitter<BabiesPatronymicState> emit,
  ) async {
    try {
      emit(BabiesPatronymicLoading());
      final BabiesPatronymic babiesPatronymic = await _getBabiesPatronymicUsecase.call(NoParams());
      return emit(
        BabiesPatronymicLoadSuccess(
          babiesPatronymic: babiesPatronymic,
        ),
      );
    } catch (error, stackTrace) {
      emit(BabiesPatronymicFailure('$error'));
      addError(error, stackTrace);
    }
  }

  Future<void> _updateData(
    BabiesPatronymicUpdatedData event,
    Emitter<BabiesPatronymicState> emit,
  ) async {
    final oldState = state;
    if (oldState is BabiesPatronymicLoadSuccess) {
      try {
        emit(BabiesPatronymicLoading());
        await _updateBabiesPatronymicUsecase.call(event.babiesPatronymic);
        final BabiesPatronymic babiesPatronymic =
            await _getBabiesPatronymicUsecase.call(NoParams());
        emit(BabiesPatronymicInfoMessage(l10n.updateDataBabiesPatronymic));
        emit(oldState.copyWith(babiesPatronymic: babiesPatronymic));
      } catch (error, stackTrace) {
        emit(BabiesPatronymicFailure('$error'));
        addError(error, stackTrace);
      }
    }
  }
}
