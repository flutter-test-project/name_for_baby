part of 'babies_patronymic_bloc.dart';

/// Класс состояний для работы с данными отчесва пользователя
@immutable
sealed class BabiesPatronymicState extends Equatable {
  @override
  List<Object> get props => [];
}

/// Класс начального состояния
final class BabiesPatronymicInitial extends BabiesPatronymicState {}

/// Класс состояния загрузки данных
final class BabiesPatronymicLoading extends BabiesPatronymicState {}

/// Класс состояния полученнных данных
final class BabiesPatronymicLoadSuccess extends BabiesPatronymicState {
  BabiesPatronymicLoadSuccess({
    required this.babiesPatronymic,
  });

  /// Отчесво для ребенка
  final BabiesPatronymic babiesPatronymic;

  BabiesPatronymicLoadSuccess copyWith({
    BabiesPatronymic? babiesPatronymic,
  }) {
    return BabiesPatronymicLoadSuccess(
      babiesPatronymic: babiesPatronymic ?? this.babiesPatronymic,
    );
  }

  @override
  List<Object> get props => [
        babiesPatronymic,
      ];
}

/// Класс вывода критической ошибки
final class BabiesPatronymicFailure extends BabiesPatronymicState {
  BabiesPatronymicFailure(this.text);

  final String text;
}

/// Класс состония вывода сообщения ошибки
final class BabiesPatronymicErrorMessage extends BabiesPatronymicState {}

/// Класс для вывода информационного сообщения пользователя
final class BabiesPatronymicInfoMessage extends BabiesPatronymicState {
  BabiesPatronymicInfoMessage(this.message);

  final String message;
}
