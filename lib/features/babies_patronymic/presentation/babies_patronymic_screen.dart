import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:name_for_baby/app/domain/entities/babies_patronymic.dart';
import 'package:name_for_baby/app/presentation/components/dialogs/info_dialog.dart';
import 'package:name_for_baby/app/presentation/components/loading/loading_indicator.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/features/babies_patronymic/domain/state/babies_patronymic_bloc.dart';
import 'package:name_for_baby/l10n/delegate.dart';
import 'package:name_for_baby/ui_kit/buttons/ui_button_outline.dart';
import 'package:name_for_baby/ui_kit/fields/ui_text_field.dart';
import 'package:name_for_baby/ui_kit/general/ui_failure_panel.dart';

/// Класс описывающий экран отчесва для ребенка
class BabiesPatronymicScreen extends StatelessWidget {
  const BabiesPatronymicScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BabiesPatronymicBloc>(
      create: (BuildContext context) => GetIt.instance()..add(BabiesPatronymicStartedLoading()),
      child: Scaffold(
        appBar: AppBar(
          title: Text(l10n.patronymic),
        ),
        body: const Padding(
          padding: EdgeInsets.symmetric(horizontal: 12),
          child: BabiesPatronymicContent(),
        ),
      ),
    );
  }
}

class BabiesPatronymicContent extends StatelessWidget {
  const BabiesPatronymicContent({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<BabiesPatronymicBloc, BabiesPatronymicState>(
      listener: (context, state) {
        if (state is BabiesPatronymicInfoMessage) {
          _showInfoMessage(
            context: context,
            message: state.message,
          );
        }
      },
      builder: (context, state) {
        if (state is BabiesPatronymicFailure) {
          return UiFailurePanel(
            state.text,
            onReload: () => onRefresh(context),
          );
        }
        return Stack(
          children: [
            if (state is BabiesPatronymicLoading) const Center(child: LoadingIndicator()),
            _BabiesPatronymicForm(),
          ],
        );
      },
      buildWhen: (context, state) {
        if (state is BabiesPatronymicInfoMessage) return false;
        return true;
      },
    );
  }

  /// Метод вывода информационного сообщения пользователю
  void _showInfoMessage({
    required BuildContext context,
    required String message,
  }) {
    showInfoDialog(
      context: context,
      description: message,
      onPressed: () => Navigator.of(context).pop(),
    );
  }

  /// метод запускающий событие перезвгрузки страницы
  void onRefresh(BuildContext context) {
    context.read<BabiesPatronymicBloc>().add(
          BabiesPatronymicStartedLoading(),
        );
  }
}

class _BabiesPatronymicForm extends StatefulWidget {
  @override
  State<_BabiesPatronymicForm> createState() => _BabiesPatronymicFormState();
}

class _BabiesPatronymicFormState extends State<_BabiesPatronymicForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _boysPatronymicController = TextEditingController();
  TextEditingController _girlsPatronymicController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _boysPatronymicController.dispose();
    _girlsPatronymicController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BabiesPatronymicBloc, BabiesPatronymicState>(
      builder: (context, state) {
        if (state is BabiesPatronymicLoadSuccess) {
          _boysPatronymicController = TextEditingController(text: state.babiesPatronymic.boysValue);
          _girlsPatronymicController =
              TextEditingController(text: state.babiesPatronymic.girlsValue);
        }
        return Form(
          key: _formKey,
          child: Column(
            children: [
              const IndentVertical(12),
              UiTextFormField(
                controller: _boysPatronymicController,
                labelText: l10n.forBoy,
              ),
              const IndentVertical(12),
              UiTextFormField(
                controller: _girlsPatronymicController,
                labelText: l10n.forGirl,
              ),
              const IndentVertical(12),
              UiButtonOutline(
                onPressed: state is! BabiesPatronymicLoadSuccess
                    ? null
                    : () => _onPressedUpdateData(context),
                child: Text(l10n.createAccount),
              ),
            ],
          ),
        );
      },
    );
  }

  /// Метод обрабочик нажатия на кнопку обновления данных отчесва
  void _onPressedUpdateData(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      final boysPatronymic = _boysPatronymicController.text.trim();
      final girlsPatronymic = _girlsPatronymicController.text.trim();

      context.read<BabiesPatronymicBloc>().add(
            BabiesPatronymicUpdatedData(
              babiesPatronymic: BabiesPatronymic(
                boysValue: boysPatronymic,
                girlsValue: girlsPatronymic,
              ),
            ),
          );
    }
  }
}
