import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:name_for_baby/app/data/collections/firestore_collections.dart';
import 'package:name_for_baby/app/domain/entities/babies_patronymic.dart';
import 'package:name_for_baby/features/babies_patronymic/domain/i_babies_patronymic_repository.dart';

/// Реализация репозитория по работе с данными отчества для ребенка
class BabiesPatronymicRepository implements IBabiesPatronymicRepository {
  final FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  Future<BabiesPatronymic> getBabiesPatronymic({
    required String userLogin,
  }) async {
    String boysValue = '';

    String girlsValue = '';

    DocumentSnapshot<Map<String, dynamic>>? data =
        await db.collection(FirestoreCollections.userData).doc(userLogin).get();
    final Map<String, dynamic> map = data.data() ?? {};

    map.forEach((key, value) {
      if (key == FirestoreUsersFields.boysPatronymic) {
        if (value is String) boysValue = value;
      } else if (key == FirestoreUsersFields.girlsPatronymic) {
        if (value is String) girlsValue = value;
      }
    });

    return BabiesPatronymic(
      boysValue: boysValue,
      girlsValue: girlsValue,
    );
  }

  @override
  Future<void> updateBabiesPatronymic({
    required String userLogin,
    required BabiesPatronymic babiesPatronymic,
  }) async {
    DocumentSnapshot<Map<String, dynamic>>? data =
        await db.collection(FirestoreCollections.userData).doc(userLogin).get();
    if (data.exists == false) {
      // создать коллекцию данных пользователя
      await db.collection(FirestoreCollections.userData).doc(userLogin).set(
        {
          FirestoreUsersFields.boysPatronymic: babiesPatronymic.boysValue,
          FirestoreUsersFields.girlsPatronymic: babiesPatronymic.girlsValue,
        },
      );
    } else {
      // Добавить данные в коллекцию
      await db.collection(FirestoreCollections.userData).doc(userLogin).update(
        {
          FirestoreUsersFields.boysPatronymic: babiesPatronymic.boysValue,
          FirestoreUsersFields.girlsPatronymic: babiesPatronymic.girlsValue,
        },
      );
    }
  }
}
