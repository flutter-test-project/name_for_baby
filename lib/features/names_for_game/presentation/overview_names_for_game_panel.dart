import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/domain/entities/name.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/features/names_for_game/presentation/components/check_panel.dart';
import 'package:name_for_baby/features/names_for_game/domain/state/names_for_game_bloc.dart';
import 'package:name_for_baby/l10n/delegate.dart';
import 'package:name_for_baby/routing/routes.dart';
import 'package:name_for_baby/ui_kit/buttons/ui_button_outline.dart';

/// Панель просмотра достурных имен для выбора для участия в игре
class OverviewNamesForGamePanel extends StatelessWidget {
  const OverviewNamesForGamePanel({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        _YouCanAddToCountNameText(),
        const IndentVertical(6),
        _SelectedCountNameText(),
        const IndentVertical(6),
        _FavoriteNamesPanel(),
        const IndentVertical(6),
        _PersonalNamesPanel(),
        const IndentVertical(6),
      ],
    );
  }
}

/// Текст о том скольео можно добавить имен
class _YouCanAddToCountNameText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NamesForGameBloc, NamesForGameState>(
      builder: (context, state) {
        if (state is NamesForGameLoadSuccess) {
          return Text(l10n.youCanAddToCountName(state.quantityName.toString()));
        }
        return const SizedBox();
      },
    );
  }
}

/// Текст о том сколько имен было выбрано
class _SelectedCountNameText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NamesForGameBloc, NamesForGameState>(
      builder: (context, state) {
        if (state is NamesForGameLoadSuccess) {
          return Text(
            l10n.selectedCountName(
              state.selectedNameIds.length.toString(),
              state.quantityName.toString(),
            ),
          );
        }
        return const SizedBox();
      },
    );
  }
}

/// Панель отображающая понравившиеся имена пользователя
class _FavoriteNamesPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NamesForGameBloc, NamesForGameState>(
      builder: (context, state) {
        if (state is NamesForGameLoadSuccess) {
          return Column(
            children: [
              Text(l10n.namesYouLike),
              const IndentVertical(6),
              ...state.favoriteNames.map(
                (name) => CheckPanel(
                  isCheck: state.selectedNameIds.contains(name.id),
                  onChanged: (bool? isCheck) {
                    if (state.selectedNameIds.length == state.quantityName) return;
                    if (isCheck != null) {
                      _selectedName(
                        context: context,
                        name: name,
                      );
                    }
                  },
                  child: Row(
                    children: [
                      Expanded(child: Text(name.fullName)),
                      IconButton(
                        onPressed: () => _goToCardName(
                          context: context,
                          name: name,
                        ),
                        icon: const Icon(Icons.info_outline),
                      ),
                    ],
                  ),
                ),
              ),
              const IndentVertical(6),
              UiButtonOutline(
                onPressed: null,
                child: Text(l10n.findMoreNames),
              ),
            ],
          );
        }
        return const SizedBox();
      },
    );
  }

  /// Метод позволяюший перейти на страничку карточки имени
  void _goToCardName({
    required BuildContext context,
    required CollectorName name,
  }) {
    CardNameRoute(
      nameId: name.id,
      $extra: name,
    ).push(context);
  }
}

/// Панель отображающая имена пользователя, которые он сам создал
class _PersonalNamesPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NamesForGameBloc, NamesForGameState>(
      builder: (context, state) {
        if (state is NamesForGameLoadSuccess) {
          return Column(
            children: [
              Text(l10n.addedNames),
              const IndentVertical(6),
              ...state.inventedNames.map(
                (name) => CheckPanel(
                  isCheck: state.selectedNameIds.contains(name.id),
                  child: Text(name.fullName),
                  onChanged: (bool? isCheck) {
                    if (state.selectedNameIds.length == state.quantityName) return;
                    if (isCheck != null) {
                      _selectedName(
                        context: context,
                        name: name,
                      );
                    }
                  },
                ),
              ),
              //Text(state.inventedNames.toString()),
              const IndentVertical(6),
              UiButtonOutline(
                onPressed: null,
                child: Text(l10n.addMoreNames),
              ),
            ],
          );
        }
        return const SizedBox();
      },
    );
  }
}

/// Метод обработчик события нажатия на кнопку чек бокса
///
/// Принимает
/// - [context] -
/// - [name] - имя которое выбранно/ не выбрано
void _selectedName({
  required BuildContext context,
  required Name name,
}) {
  context.read<NamesForGameBloc>().add(SelectedNameForGame(name));
}
