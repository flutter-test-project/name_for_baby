import 'package:flutter/material.dart';

/// Панель изобрадающая чекбокс и выджет к которому относится данный чек бокс
class CheckPanel extends StatelessWidget {
  const CheckPanel({
    super.key,
    required this.isCheck,
    required this.child,
    this.onChanged,
  });

  /// виджет отображающий объект к которому относится ланный чекбокс
  final Widget child;

  /// Переключалеть чек бокса
  final bool isCheck;

  /// метод измененения значения чек бокса
  final ValueChanged<bool?>? onChanged;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        children: [
          Checkbox(
            value: isCheck,
            onChanged: onChanged,
          ),
          Expanded(child: child),
        ],
      ),
    );
  }
}
