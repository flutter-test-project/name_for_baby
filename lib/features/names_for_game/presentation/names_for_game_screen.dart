import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';
import 'package:name_for_baby/app/presentation/components/dialogs/error_dialog.dart';
import 'package:name_for_baby/app/presentation/components/loading/loading_indicator.dart';
import 'package:name_for_baby/features/names_for_game/domain/state/names_for_game_bloc.dart';
import 'package:name_for_baby/features/names_for_game/presentation/overview_names_for_game_panel.dart';
import 'package:name_for_baby/l10n/delegate.dart';
import 'package:name_for_baby/ui_kit/general/ui_failure_panel.dart';

/// Класс описывающий экран выбора имен для иры по выбору имени
class NamesForGameScreen extends StatelessWidget {
  const NamesForGameScreen({
    super.key,
    required this.sex,
    required this.quantityName,
    required this.selectedNameIds,
  });

  /// пол по которому делаем выборку
  final SexValue sex;

  /// Кол-во имен для учасвия в выборке
  final int quantityName;

  /// Список id выбранных имен для участия в игре
  final List<String> selectedNameIds;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<NamesForGameBloc>(
      create: (BuildContext context) => GetIt.instance()
        ..add(
          NamesForGameEventStarted(
            sex: sex,
            quantityName: quantityName,
            selectedNameIds: selectedNameIds,
          ),
        ),
      child: Scaffold(
        appBar: AppBar(
          title: Text(l10n.addingNameToGame),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: NamesForGameContent(
            sex: sex,
            quantityName: quantityName,
            selectedNameIds: selectedNameIds,
          ),
        ),
      ),
    );
  }
}

/// Виджет определяюший наполненение эрана в зависисмости от состояния
class NamesForGameContent extends StatefulWidget {
  const NamesForGameContent({
    super.key,
    required this.sex,
    required this.quantityName,
    required this.selectedNameIds,
  });

  /// пол по которому делаем выборку
  final SexValue sex;

  /// Кол-во имен для учасвия в выборке
  final int quantityName;

  /// Список id выбранных имен для участия в игре
  final List<String> selectedNameIds;

  @override
  State<NamesForGameContent> createState() => _NamesForGameContentState();
}

class _NamesForGameContentState extends State<NamesForGameContent> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<NamesForGameBloc, NamesForGameState>(
      listener: (context, state) {
        if (state is NamesForGameErrorMessage) {
          _showErrorDialog(
            title: state.title,
            description: state.description,
          );
        }
      },
      builder: (context, state) {
        return switch (state) {
          NamesForGameInitial() => const SizedBox(),
          NamesForGameLoading() => const LoadingIndicator(),
          NamesForGameLoadSuccess() => const OverviewNamesForGamePanel(),
          NamesForGameErrorMessage() => const SizedBox(),
          NamesForGameLoadFailure() => UiFailurePanel(
              state.text,
              onReload: () => onErrorReload(
                selectedNameIds: state.selectedNameIds,
              ),
            ),
        };
      },
      buildWhen: (context, state) {
        if (state is NamesForGameErrorMessage) return false;
        return true;
      },
    );
  }

  void _showErrorDialog({
    String? title,
    required String description,
  }) {
    showErrorDialog(
      context: context,
      title: title ?? l10n.mistake,
      description: description,
    );
  }

  /// метод запускающий событие перезвгрузки страницы при ошибки
  void onErrorReload({
    List<String>? selectedNameIds,
  }) {
    context.read<NamesForGameBloc>().add(
          NamesForGameEventStarted(
            sex: widget.sex,
            quantityName: widget.quantityName,
            selectedNameIds: selectedNameIds ?? widget.selectedNameIds,
          ),
        );
  }
}
