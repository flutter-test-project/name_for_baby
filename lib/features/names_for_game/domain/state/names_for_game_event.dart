part of 'names_for_game_bloc.dart';

/// Класс для работы с событиями экрана выбора имен для иры по выбору имени
@immutable
sealed class NamesForGameEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

/// Класс загрузки данных для экрана выбора имен для иры по выбору имени
final class NamesForGameEventStarted extends NamesForGameEvent {
  NamesForGameEventStarted({
    required this.sex,
    required this.quantityName,
    required this.selectedNameIds,
  });

  /// пол по которому делаем выборку
  final SexValue sex;

  /// Кол-во имен для учасвия в выборке
  final int quantityName;

  /// Список id выбранных имен для участия в игре
  final List<String> selectedNameIds;
}

/// Событие выбора / отмены выбора имени для игры
final class SelectedNameForGame extends NamesForGameEvent {
  SelectedNameForGame(this.name);

  /// Имя
  final Name name;
}
