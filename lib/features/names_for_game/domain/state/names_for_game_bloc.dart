import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/domain/entities/invented_name.dart';
import 'package:name_for_baby/app/domain/entities/name.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';
import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/event_bus/event_bus.dart';
import 'package:name_for_baby/features/event_bus/event_bus_events.dart';
import 'package:name_for_baby/features/names_book/domain/usecase/get_names_usecase.dart';
import 'package:name_for_baby/features/personal_list_names/domain/usecase/get_personal_names_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/get_favorite_name_ids_usecase.dart';
import 'package:name_for_baby/l10n/delegate.dart';

part 'names_for_game_event.dart';

part 'names_for_game_state.dart';

/// Блок по работе с эераном  выбора имен для иры по выбору имени
class NamesForGameBloc extends Bloc<NamesForGameEvent, NamesForGameState> {
  NamesForGameBloc({
    required GetFavoriteNamesUsecase getFavoriteNamesUsecase,
    required GetNamesUsecase getNamesUsecase,
    required GetPersonalNamesUsecase getPersonalNamesUsecase,
  })  : _getFavoriteNamesUsecase = getFavoriteNamesUsecase,
        _getNamesUsecase = getNamesUsecase,
        _getPersonalNamesUsecase = getPersonalNamesUsecase,
        super(NamesForGameInitial()) {
    on<NamesForGameEvent>(
      (event, emit) => switch (event) {
        NamesForGameEventStarted() => _onLoadStarted(event, emit),
        SelectedNameForGame() => _onSelectedNamesForGame(event, emit),
      },
    );
  }

  final GetFavoriteNamesUsecase _getFavoriteNamesUsecase;
  final GetNamesUsecase _getNamesUsecase;
  final GetPersonalNamesUsecase _getPersonalNamesUsecase;

  Future<void> _onLoadStarted(
    NamesForGameEventStarted event,
    Emitter<NamesForGameState> emit,
  ) async {
    try {
      emit(NamesForGameLoading());
      final List<String> favoriteNameIds = await _getFavoriteNamesUsecase.call(NoParams());
      final favoriteNames = await _getNamesUsecase.call(favoriteNameIds);
      final favoriteNamesFilter = favoriteNames.where((name) => name.sex == event.sex).toList();
      final personalNames = await _getPersonalNamesUsecase.call(NoParams());
      final personalNamesFilter = personalNames.where((name) => name.sex == event.sex).toList();
      emit(
        NamesForGameLoadSuccess(
          sex: event.sex,
          quantityName: event.quantityName,
          favoriteNames: favoriteNamesFilter,
          inventedNames: personalNamesFilter,
          selectedNameIds: event.selectedNameIds,
        ),
      );
    } catch (error, stackTrace) {
      emit(
        NamesForGameLoadFailure('$error'),
      );
      addError(error, stackTrace);
    }
  }

  Future<void> _onSelectedNamesForGame(
    SelectedNameForGame event,
    Emitter<NamesForGameState> emit,
  ) async {
    final oldState = state;
    if (oldState is NamesForGameLoadSuccess) {
      try {
        List<String> selectedNameIds = List.from(oldState.selectedNameIds);
        final bool isAddOperation = !selectedNameIds.contains(event.name.id);
        if (isAddOperation) {
          // добавляем ид имени в список выбранных имен
          selectedNameIds.add(event.name.id);
        } else {
          // удаляем ид продукта в список выбранных имен
          selectedNameIds.remove(event.name.id);
        }
        if (selectedNameIds.length > oldState.quantityName) {
          // Было выбранно больше разрешенного кол-ва имен
          emit(NamesForGameErrorMessage(description: l10n.youHaveChosenTooManyNames));
          emit(
            oldState.copyWith(
              selectedNameIds: oldState.selectedNameIds.length == oldState.quantityName
                  ? oldState.selectedNameIds
                  : [],
            ),
          );
        }

        // Отправим событие добавление / удаление  имени из коллекции имен для игры
        EventBus().addEvent(
          SelectedNameForGameEvent(
            event.name,
            isAddOperation: isAddOperation,
          ),
        );

        emit(
          oldState.copyWith(
            selectedNameIds: selectedNameIds,
          ),
        );
      } catch (error, stackTrace) {
        emit(NamesForGameErrorMessage(description: '$error'));
        emit(oldState);
        addError(error, stackTrace);
      }
    }
  }
}
