part of 'names_for_game_bloc.dart';

/// Коасс состояний для работы с экраном выбора имен для иры по выбору имени
@immutable
sealed class NamesForGameState extends Equatable {
  @override
  List<Object> get props => [];
}

/// Класс начального состояния
final class NamesForGameInitial extends NamesForGameState {}

/// Класс состояния загрузки данных
final class NamesForGameLoading extends NamesForGameState {}

/// Класс сотояний с успешной загрузкой данных для работы
final class NamesForGameLoadSuccess extends NamesForGameState {
  NamesForGameLoadSuccess({
    required this.sex,
    required this.quantityName,
    required this.favoriteNames,
    required this.inventedNames,
    required this.selectedNameIds,
  });

  /// пол по которому делаем выборку
  final SexValue sex;

  /// Кол-во имен для учасвия в выборке
  final int quantityName;

  /// Список понравившмхся имен
  final List<CollectorName> favoriteNames;

  /// Список личных имен пользователя
  final List<InventedName> inventedNames;

  /// Список id выбранных имен для участия в игре
  final List<String> selectedNameIds;

  NamesForGameLoadSuccess copyWith({
    List<String>? selectedNameIds,
  }) {
    return NamesForGameLoadSuccess(
      sex: sex,
      quantityName: quantityName,
      favoriteNames: favoriteNames,
      inventedNames: inventedNames,
      selectedNameIds: selectedNameIds ?? this.selectedNameIds,
    );
  }

  @override
  List<Object> get props => [
        sex,
        quantityName,
        favoriteNames,
        inventedNames,
        selectedNameIds,
      ];
}

/// Класс вывода ошибки для пользователя
final class NamesForGameErrorMessage extends NamesForGameState {
  NamesForGameErrorMessage({
    this.title,
    required this.description,
  });

  final String? title;
  final String description;
}

/// Класс вывода критической ошибки
final class NamesForGameLoadFailure extends NamesForGameState {
  NamesForGameLoadFailure(
    this.text, {
    this.selectedNameIds,
  });

  /// Текст ошибки
  final String text;

  /// Список id выбранных имен для участия в игре
  final List<String>? selectedNameIds;
}
