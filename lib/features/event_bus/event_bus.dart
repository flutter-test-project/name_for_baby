import 'dart:async';

import 'package:name_for_baby/features/event_bus/event_bus_events.dart';

/// Класс реализующий глобальную шину событий
final class EventBus {
  EventBus._internal();

  static final EventBus _instance = EventBus._internal();

  factory EventBus() => _instance;

  final StreamController _streamController = StreamController.broadcast();

  /// Подписка на определенный event
  Stream<T> on<T>() => _streamController.stream.where((event) => event is T).cast<T>();

  void addEvent(EventBusEvents event) {
    _streamController.add(event);
  }
}
