import 'package:equatable/equatable.dart';
import 'package:name_for_baby/app/domain/entities/name.dart';

/// Класс реализующий события определенноого тиа для eventBus
sealed class EventBusEvents<T> extends Equatable {
  const EventBusEvents(
    this.data,
  );

  /// Не типизированный дженерик data
  final T? data;

  @override
  List<Object?> get props => [
        data,
      ];
}

/// Глобальное событие когда пользователь выбирает понравившееся / не понравившееся имя
///
/// Принимает
/// - [countFavoriteName] - кол-во понравившихся имен
/// - [countNotFavoriteName] - кол-во не понравившихся имен
/// - [countPersonalName] - Кол-во имен в личной коллекции пользователя
final class EditCountSelectedNameEvent extends EventBusEvents {
  const EditCountSelectedNameEvent({
    this.countFavoriteName,
    this.countNotFavoriteName,
    this.countPersonalName,
  }) : super(null);

  /// Кол-во понравившихся имен пользователю
  final int? countFavoriteName;

  /// Кол-во Не понравившихся имен пользователю
  final int? countNotFavoriteName;

  /// Кол-во имен в личной коллекции пользователя
  final int? countPersonalName;

  @override
  List<Object?> get props => [
        countFavoriteName,
        countNotFavoriteName,
        countPersonalName,
      ];
}

/// Глобальное событие входа пользователя в приложение
final class UserAuthEvent extends EventBusEvents {
  const UserAuthEvent() : super(null);
}

/// Глобальное событие добавление имени / удаление имени для игры
final class SelectedNameForGameEvent extends EventBusEvents {
  const SelectedNameForGameEvent(
    this.name, {
    this.isAddOperation = true,
  }) : super(null);

  /// Имя
  final Name name;

  /// Флаг показывающий нужно ди добавить это имя в коллекцию имен для игры
  final bool isAddOperation;
}
