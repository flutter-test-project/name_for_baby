import 'package:flutter/material.dart';
import 'package:name_for_baby/app/domain/entities/invented_name.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';

class PersonalNameCard extends StatelessWidget {
  const PersonalNameCard({
    super.key,
    required this.inventedName,
    this.onDeletedPressed,
  });

  /// Имя личной коллекции пользователя
  final InventedName inventedName;

  /// Дейсвие при нажатии на иконку информации
  final void Function()? onDeletedPressed;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 1,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 12),
        child: Row(
          children: [
            Expanded(
              child: _PersonalNameContent(
                personalName: inventedName,
              ),
            ),
            _IconButton(
              icon: Icons.delete,
              onPressed: onDeletedPressed,
            ),
          ],
        ),
      ),
    );
  }
}

/// Виджет показывающий основную информацию имени
class _PersonalNameContent extends StatelessWidget {
  const _PersonalNameContent({
    required this.personalName,
  });

  /// Имя личной коллекции пользователя
  final InventedName personalName;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(personalName.fullName),
        if (personalName.valueName.isNotEmpty) const IndentHorizontal(6),
        if (personalName.valueName.isNotEmpty) Text(personalName.valueName),
      ],
    );
  }
}

/// Информационная иконка
class _IconButton extends StatelessWidget {
  const _IconButton({
    required this.icon,
    this.onPressed,
  });

  /// Надатие на кнопку
  final void Function()? onPressed;

  /// Иконка
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: onPressed,
      icon: Icon(icon),
    );
  }
}
