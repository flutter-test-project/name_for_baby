import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:name_for_baby/app/presentation/components/loading/loading_indicator.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/features/personal_list_names/domain/state/personal_list_names_bloc.dart';
import 'package:name_for_baby/features/personal_list_names/presentation/components/personal_name_card.dart';
import 'package:name_for_baby/l10n/delegate.dart';
import 'package:name_for_baby/routing/routes.dart';
import 'package:name_for_baby/ui_kit/buttons/ui_button_outline.dart';
import 'package:name_for_baby/ui_kit/general/ui_empty_panel.dart';
import 'package:name_for_baby/ui_kit/general/ui_failure_panel.dart';

/// Экран работы с личным списком имен пользователя
class PersonalListNamesScreen extends StatelessWidget {
  const PersonalListNamesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PersonalListNamesBloc>(
      create: (BuildContext context) => GetIt.instance()..add(PersonalListNamesStartedLoading()),
      child: Scaffold(
        appBar: AppBar(
          title: Text(l10n.addedNames),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: _PersonalListNamesPage(),
        ),
      ),
    );
  }
}

/// Класс описывающий общий вид экрана в зависисмости от состояния
class _PersonalListNamesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PersonalListNamesBloc, PersonalListNamesState>(
      listener: (context, state) {},
      builder: (context, state) {
        return RefreshIndicator(
          child: switch (state) {
            PersonalListNamesInitial() => const SizedBox(),
            PersonalListNamesLoading() => const LoadingIndicator(),
            PersonalListNamesLoadSuccess() => Column(
                children: [
                  const IndentVertical(6),
                  Expanded(child: _PersonalListNamesContent()),
                  const IndentVertical(6),
                  UiButtonOutline(
                    padding: const EdgeInsets.all(12.0),
                    onPressed: () {
                      /// Перейти на страничку создания персонального имени пользователя
                      //Navigator.push(context, AddPersonalNameRoute());
                      const AddPersonalNameRoute().go(context);
                    },
                    child: Text(l10n.addName),
                  ),
                  const IndentVertical(12),
                ],
              ),
            PersonalListNamesFailure() => UiFailurePanel(
                state.text,
                onReload: () => onRefresh(context),
              ),
            PersonalListNamesMessage() => const SizedBox(),
          },
          onRefresh: () async => onRefresh(context),
        );
      },
    );
  }

  /// метод запускающий событие перезвгрузки страницы
  void onRefresh(BuildContext context) {
    context.read<PersonalListNamesBloc>().add(
          PersonalListNamesStartedLoading(),
        );
  }
}

class _PersonalListNamesContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PersonalListNamesBloc, PersonalListNamesState>(
      builder: (context, state) {
        if (state is PersonalListNamesLoadSuccess) {
          if (state.personalNames.isEmpty) {
            return UiEmptyPanel(
              text: l10n.noDataAvailable,
            );
          }
          return ListView(
            children: state.personalNames
                .map(
                  (name) => Padding(
                    padding: const EdgeInsets.only(top: 4),
                    child: PersonalNameCard(
                      inventedName: name,
                      onDeletedPressed: () => onDeletedPressed(
                        context: context,
                        idName: name.id,
                      ),
                    ),
                  ),
                )
                .toList(),
          );
        }
        return const SizedBox();
      },
      buildWhen: (context, state) {
        if (state is PersonalListNamesLoadSuccess) return true;
        return false;
      },
    );
  }

  /// метод запускающий событие удаления элемента имени пользователя
  void onDeletedPressed({
    required BuildContext context,
    required String idName,
  }) {
    context.read<PersonalListNamesBloc>().add(
          PersonalListNameDeleted(idName),
        );
  }
}
