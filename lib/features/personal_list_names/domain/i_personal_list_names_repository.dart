import 'package:name_for_baby/app/domain/entities/invented_name.dart';

/// Интерфейс описывающий работу с репозиторием имен добавленных пользователем в личную коллекцию
abstract interface class IPersonalListNamesRepository {
  /// Метод добавления имени в персональную коллекцию пользователя
  ///
  /// Принимает
  /// - [inventedNames] - добавленное имея пользователя
  /// - [userLogin] - логин пользователя
  Future<void> addPersonalName({
    required InventedName inventedNames,
    required String userLogin,
  });

  /// Метод возвращает список персональных имен пользователя
  ///
  /// Принимает
  /// - [userLogin] - логин пользователя
  Future<List<InventedName>> getPersonalNames({
    required String userLogin,
  });

  /// Метод удаляет позицию из списка
  ///
  /// Принимает
  /// - [userLogin] - логин пользователя
  /// - [idName] - логин пользователя
  Future<void> deletePersonalName({
    required String userLogin,
    required String idName,
  });
}
