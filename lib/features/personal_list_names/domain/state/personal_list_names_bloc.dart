import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/entities/invented_name.dart';
import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/event_bus/event_bus.dart';
import 'package:name_for_baby/features/event_bus/event_bus_events.dart';
import 'package:name_for_baby/features/personal_list_names/domain/usecase/delete_personal_name_usecase.dart';
import 'package:name_for_baby/features/personal_list_names/domain/usecase/get_personal_names_usecase.dart';
import 'package:name_for_baby/features/user_secure_data/domain/usecase/is_user_authentication_usecase.dart';
import 'package:name_for_baby/l10n/delegate.dart';

part 'personal_list_names_event.dart';

part 'personal_list_names_state.dart';

/// Блок по работе с именами пользователями которые он сам добавил
class PersonalListNamesBloc extends Bloc<PersonalListNamesEvent, PersonalListNamesState> {
  PersonalListNamesBloc({
    required IsUserAuthenticationUsecase isUserAuthenticationUsecase,
    required GetPersonalNamesUsecase getPersonalNamesUsecase,
    required DeletePersonalNameUsecase deletePersonalNameUsecase,
    required EventBus eventBus,
  })  : _isUserAuthenticationUsecase = isUserAuthenticationUsecase,
        _getPersonalNamesUsecase = getPersonalNamesUsecase,
        _deletePersonalNameUsecase = deletePersonalNameUsecase,
        _eventBus = eventBus,
        super(PersonalListNamesInitial()) {
    on<PersonalListNamesEvent>((event, emit) => switch (event) {
          PersonalListNamesStartedLoading() => _onLoadStarted(event, emit),
          PersonalListNameDeleted() => _onDeleted(event, emit),
        });

    _streamSubscription = _eventBus.on<EditCountSelectedNameEvent>().listen((event) {
      if (event.countPersonalName != null) add(PersonalListNamesStartedLoading());
    });
  }

  final IsUserAuthenticationUsecase _isUserAuthenticationUsecase;
  final GetPersonalNamesUsecase _getPersonalNamesUsecase;
  final DeletePersonalNameUsecase _deletePersonalNameUsecase;

  final EventBus _eventBus;
  late final StreamSubscription _streamSubscription;

  @override
  Future<void> close() {
    _streamSubscription.cancel();
    return super.close();
  }

  Future<void> _onLoadStarted(
    PersonalListNamesStartedLoading event,
    Emitter<PersonalListNamesState> emit,
  ) async {
    try {
      emit(PersonalListNamesLoading());
      final bool isUserBeenIdentified = await _isUserAuthenticationUsecase.call(NoParams());
      if (!isUserBeenIdentified) {
        // Эта странича только для зареганых пользователей
        emit(PersonalListNamesFailure(l10n.pageIsOnlyUserAuth));
      }
      final personalNames = await _getPersonalNamesUsecase.call(NoParams());
      emit(
        PersonalListNamesLoadSuccess(
          personalNames: personalNames,
        ),
      );
    } catch (error, stackTrace) {
      emit(PersonalListNamesFailure('$error'));
      addError(error, stackTrace);
    }
  }

  Future<void> _onDeleted(
    PersonalListNameDeleted event,
    Emitter<PersonalListNamesState> emit,
  ) async {
    final oldState = state;
    if (oldState is PersonalListNamesLoadSuccess) {
      try {
        await _deletePersonalNameUsecase.call(event.idName);
        final personalNames = await _getPersonalNamesUsecase.call(NoParams());
        EventBus().addEvent(
          EditCountSelectedNameEvent(
            countPersonalName: personalNames.length,
          ),
        );
        emit(oldState.copyWith(
          inventedNames: personalNames,
        ));
      } catch (error, stackTrace) {
        emit(PersonalListNamesFailure('$error'));
        addError(error, stackTrace);
      }
    }
  }
}
