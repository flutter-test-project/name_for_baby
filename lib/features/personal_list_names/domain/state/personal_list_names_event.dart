part of 'personal_list_names_bloc.dart';

/// Класс событий личной коллекции имен пользователя
@immutable
sealed class PersonalListNamesEvent extends Equatable{
  @override
  List<Object?> get props => [];
}

/// Событие начала загрузки данных
final class PersonalListNamesStartedLoading extends PersonalListNamesEvent {}

/// Событие удаления элемента
final class PersonalListNameDeleted extends PersonalListNamesEvent {
  PersonalListNameDeleted(this.idName);

  /// ид элемента имени личной коллекци пользователя
  final String idName;
}
