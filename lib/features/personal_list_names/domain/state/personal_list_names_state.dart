part of 'personal_list_names_bloc.dart';

/// Класс состояний для работы с данными личной коллекции имен пользователя
@immutable
sealed class PersonalListNamesState extends Equatable {
  @override
  List<Object> get props => [];
}

/// Класс начального состояния
final class PersonalListNamesInitial extends PersonalListNamesState {}

/// Класс состояния загрузки данных
final class PersonalListNamesLoading extends PersonalListNamesState {}

/// Класс состояния полученнных данных личной коллекции имен пользователя
final class PersonalListNamesLoadSuccess extends PersonalListNamesState {
  PersonalListNamesLoadSuccess({
    required this.personalNames,
  });

  /// Список личных имен пользователя
  final List<InventedName> personalNames;

  PersonalListNamesLoadSuccess copyWith({
    List<InventedName>? inventedNames,
  }) {
    return PersonalListNamesLoadSuccess(
      personalNames: inventedNames ?? personalNames,
    );
  }

  @override
  List<Object> get props => [
        personalNames,
      ];
}

/// Класс вывода критической ошибки
final class PersonalListNamesFailure extends PersonalListNamesState {
  PersonalListNamesFailure(this.text);

  final String text;
}

/// Класс состония вывода сообщения ошибки
final class PersonalListNamesMessage extends PersonalListNamesState {
  PersonalListNamesMessage(this.text);

  final String text;
}
