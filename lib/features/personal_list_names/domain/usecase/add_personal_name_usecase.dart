import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/app/domain/entities/invented_name.dart';
import 'package:name_for_baby/features/personal_list_names/domain/i_personal_list_names_repository.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';
import 'package:name_for_baby/l10n/delegate.dart';

// Юз кейс добавдения имени в личную коллекцию пользователя
class AddPersonalNameUsecase extends BaseUseCase<Future<void>, InventedName> {
  AddPersonalNameUsecase({
    required IUserSecureDataRepository userSecureDataRepository,
    required IPersonalListNamesRepository personalListNameRepository,
  })  : _userSecureDataRepository = userSecureDataRepository,
        _personalListNameRepository = personalListNameRepository;

  final IUserSecureDataRepository _userSecureDataRepository;
  final IPersonalListNamesRepository _personalListNameRepository;

  @override
  Future<void> call(InventedName params) async {
    final isUserAuthentication = await _userSecureDataRepository.isUserAuthentication;
    if (!isUserAuthentication) throw Exception(l10n.operationIsOnlyUserAuth);
    final userLogin = await _userSecureDataRepository.getLogin;
    await _personalListNameRepository.addPersonalName(
      inventedNames: params,
      userLogin: userLogin,
    );
  }
}
