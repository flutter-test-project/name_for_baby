import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/personal_list_names/domain/i_personal_list_names_repository.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';
import 'package:name_for_baby/l10n/delegate.dart';

/// Юз кейс который удаляет запись личной коллекции имен пользователя
class DeletePersonalNameUsecase extends BaseUseCase<Future<void>, String> {
  DeletePersonalNameUsecase({
    required IUserSecureDataRepository userSecureDataRepository,
    required IPersonalListNamesRepository personalListNameRepository,
  })  : _userSecureDataRepository = userSecureDataRepository,
        _personalListNameRepository = personalListNameRepository;
  final IUserSecureDataRepository _userSecureDataRepository;
  final IPersonalListNamesRepository _personalListNameRepository;

  /// Метод который удаляет запись личной коллекции имен пользователя
  ///
  /// Принимает
  /// - [params] - id записи которую необходимо удалить
  @override
  Future<void> call(String params) async {
    final isUserAuthentication = await _userSecureDataRepository.isUserAuthentication;
    if (!isUserAuthentication) throw Exception(l10n.operationIsOnlyUserAuth);
    final userLogin = await _userSecureDataRepository.getLogin;
    await _personalListNameRepository.deletePersonalName(
      userLogin: userLogin,
      idName: params,
    );
  }
}
