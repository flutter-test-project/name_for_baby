import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:name_for_baby/app/data/collections/firestore_collections.dart';
import 'package:name_for_baby/app/domain/entities/invented_name.dart';
import 'package:name_for_baby/features/personal_list_names/domain/i_personal_list_names_repository.dart';

/// Класс описывающий работу с персональным списком имен пользователя
class PersonalListNamesRepository implements IPersonalListNamesRepository {
  final FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  Future<void> addPersonalName({
    required InventedName inventedNames,
    required String userLogin,
  }) async {
    final String collection =
        '${FirestoreCollections.userData}/$userLogin/${FirestoreCollections.personalNameCollection}';

    await db.collection(collection).doc(inventedNames.id).set(inventedNames.toJson());
  }

  @override
  Future<List<InventedName>> getPersonalNames({
    required String userLogin,
  }) async {
    List<InventedName> personalNames = [];
    final String collection =
        '${FirestoreCollections.userData}/$userLogin/${FirestoreCollections.personalNameCollection}';

    final QuerySnapshot<Map<String, dynamic>> map = await db.collection(collection).get();

    for (final doc in map.docs) {
      personalNames.add(
        InventedName.fromJson(
          json: doc.data(),
          id: doc.id,
        ),
      );
    }

    return personalNames;
  }

  @override
  Future<void> deletePersonalName({
    required String userLogin,
    required String idName,
  }) async {
    final String collection =
        '${FirestoreCollections.userData}/$userLogin/${FirestoreCollections.personalNameCollection}';

    await db.collection(collection).doc(idName).delete();
  }
}
