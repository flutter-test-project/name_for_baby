/// Варианты вида ввода кол-ва имен для участия в игре
enum GameNamesCountViewOption {
  /// Выбрать из списка
  list,

  /// Ввести вручную
  manual;
}
