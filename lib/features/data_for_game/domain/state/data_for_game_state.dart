part of 'data_for_game_bloc.dart';

/// Класс состояний для работы с экраном получения данных для создания игры по выбору имени
@immutable
sealed class DataForGameState extends Equatable {
  @override
  List<Object> get props => [];
}

/// Класс начального состояния
final class DataForGameInitial extends DataForGameState {}

/// Класс состояния загрузки данных
final class DataForGameLoading extends DataForGameState {}

/// Класс состояний для выбора пола
final class SelectionSexForGame extends DataForGameState {}

/// Класс состоний для выбора кол-ва имен для участия в выборке
final class ChoosingQuantityNamesForGame extends DataForGameState {
  ChoosingQuantityNamesForGame({
    required this.sex,
    required this.listQuantityNamesForGame,
    this.gameNamesCountViewOption = GameNamesCountViewOption.list,
  });

  /// пол по которому делаем выборку
  final SexValue sex;

  /// Список вариантов кол-в имен для участия в финальной
  final List<int> listQuantityNamesForGame;

  /// Вариант ввода кол-ва имен для участия в игре
  final GameNamesCountViewOption gameNamesCountViewOption;

  ChoosingQuantityNamesForGame copyWith({
    GameNamesCountViewOption? gameNamesCountViewOption,
  }) {
    return ChoosingQuantityNamesForGame(
      sex: sex,
      listQuantityNamesForGame: listQuantityNamesForGame,
      gameNamesCountViewOption: gameNamesCountViewOption ?? this.gameNamesCountViewOption,
    );
  }

  @override
  List<Object> get props => [
        sex,
        listQuantityNamesForGame,
        gameNamesCountViewOption,
      ];
}

/// Класс состояний для просмотра имен которые будут учавсвовать в выборке
final class ShowNameListForGame extends DataForGameState {
  ShowNameListForGame({
    required this.sex,
    required this.quantityName,
    required this.selectedNames,
  });

  /// пол по которому делаем выборку
  final SexValue sex;

  /// Кол-во имен для учасвия в выборке
  final int quantityName;

  /// Список id выбранных имен для участия в игре
  final List<Name> selectedNames;

  ShowNameListForGame copyWith({
    List<Name>? selectedNames,
  }) {
    return ShowNameListForGame(
      sex: sex,
      quantityName: quantityName,
      selectedNames: selectedNames ?? this.selectedNames,
    );
  }

  @override
  List<Object> get props => [
        sex,
        quantityName,
        selectedNames,
      ];
}

/// Класс вывода сообщения
final class DataForGameMessage extends DataForGameState {
  DataForGameMessage({
    required this.title,
    required this.description,
  });

  /// Заголовок
  final String title;

  /// Описание
  final String description;
}

/// Класс вывода ошибки для пользователя
final class DataForGameErrorMessage extends DataForGameState {
  DataForGameErrorMessage({
    required this.description,
    this.title,
  });

  final String? title;
  final String description;
}

/// Класс вывода критической ошибки
final class DataForGameFailure extends DataForGameState {
  DataForGameFailure(this.text);

  final String text;
}
