part of 'data_for_game_bloc.dart';

/// Класс для работы с событиями экрана для задания настроек игры по выбору имени
@immutable
sealed class DataForGameEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

/// Событие загрузки страницы профиля
final class DataForGameStarted extends DataForGameEvent {}

/// Событие выбора пола для участия в выборке
final class SelectedSex extends DataForGameEvent {
  SelectedSex(this.sex);

  final SexValue sex;
}

/// Событие выбора кол-ва имен которые будут учавствовать в игре
final class ChooseQuantityNameForGame extends DataForGameEvent {
  ChooseQuantityNameForGame(this.quantityName);

  /// Кол-во имен которые будут учавствовать в игре
  final int quantityName;
}

/// Событие измененения значения вида ввода кол-ва имен для участия в игре
final class ChangedViewOptionGameNameCount extends DataForGameEvent {
  ChangedViewOptionGameNameCount(this.gameNamesCountViewOption);

  /// Вариант ввода кол-ва имен для участия в игре
  final GameNamesCountViewOption gameNamesCountViewOption;
}

/// Событие добавления имени для игры
final class AddedNameForGame extends DataForGameEvent {
  AddedNameForGame(
    this.name, {
    required this.isAddOperation,
  });

  /// Имя
  final Name name;

  /// Флаг показывающий нужно ди добавить это имя в коллекцию имен для игры
  final bool isAddOperation;
}
