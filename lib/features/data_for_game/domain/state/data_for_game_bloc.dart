import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/entities/name.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';
import 'package:name_for_baby/features/data_for_game/domain/emums/game_names_count_view_option.dart';
import 'package:name_for_baby/features/event_bus/event_bus.dart';
import 'package:name_for_baby/features/event_bus/event_bus_events.dart';

part 'data_for_game_event.dart';

part 'data_for_game_state.dart';

/// Блок описывающий работу с с экраном получения данных для создания игры по выбору имени
class DataForGameBloc extends Bloc<DataForGameEvent, DataForGameState> {
  DataForGameBloc({
    required EventBus eventBus,
  })  : _eventBus = eventBus,
        super(DataForGameInitial()) {
    on<DataForGameEvent>(
      (event, emit) => switch (event) {
        DataForGameStarted() => _onLoadStarted(event, emit),
        SelectedSex() => _onSelectedGender(event, emit),
        ChooseQuantityNameForGame() => _onChooseQuantityNameForGame(event, emit),
        ChangedViewOptionGameNameCount() => _onChangeViewOptionGameNameCount(event, emit),
        AddedNameForGame() => _addNameForGame(event, emit),
      },
    );
    _streamSubscription = _eventBus.on<SelectedNameForGameEvent>().listen(
          (event) => add(
            AddedNameForGame(
              event.name,
              isAddOperation: event.isAddOperation,
            ),
          ),
        );
  }

  @override
  Future<void> close() {
    _streamSubscription.cancel();
    return super.close();
  }

  final EventBus _eventBus;
  late final StreamSubscription _streamSubscription;

  Future<void> _onLoadStarted(
    DataForGameStarted event,
    Emitter<DataForGameState> emit,
  ) async {
    emit(DataForGameLoading());
    emit(SelectionSexForGame());
  }

  Future<void> _onSelectedGender(
    SelectedSex event,
    Emitter<DataForGameState> emit,
  ) async {
    final oldState = state;
    if (oldState is SelectionSexForGame) {
      emit(DataForGameLoading());
      emit(
        ChoosingQuantityNamesForGame(
          sex: event.sex,
          listQuantityNamesForGame: const [6, 10, 20],
        ),
      );
    }
  }

  Future<void> _onChangeViewOptionGameNameCount(
    ChangedViewOptionGameNameCount event,
    Emitter<DataForGameState> emit,
  ) async {
    final oldState = state;
    if (oldState is ChoosingQuantityNamesForGame) {
      try {
        emit(DataForGameLoading());
        emit(
          oldState.copyWith(
            gameNamesCountViewOption: event.gameNamesCountViewOption,
          ),
        );
      } catch (error, stackTrace) {
        emit(DataForGameErrorMessage(description: '$error'));
        emit(oldState);
        addError(error, stackTrace);
      }
    }
  }

  Future<void> _onChooseQuantityNameForGame(
    ChooseQuantityNameForGame event,
    Emitter<DataForGameState> emit,
  ) async {
    final oldState = state;
    if (oldState is ChoosingQuantityNamesForGame) {
      emit(DataForGameLoading());
      emit(
        ShowNameListForGame(
          sex: oldState.sex,
          quantityName: event.quantityName,
          selectedNames: const [],
        ),
      );
    }
  }

  Future<void> _addNameForGame(
    AddedNameForGame event,
    Emitter<DataForGameState> emit,
  ) async {
    final oldState = state;
    if (oldState is ShowNameListForGame) {
      List<Name> selectedNames = List<Name>.from(oldState.selectedNames);
      if (event.isAddOperation) {
        // Добавление имени
        selectedNames.add(event.name);
      } else {
        // удаление имени
        selectedNames.remove(event.name);
      }
      emit(
        oldState.copyWith(
          selectedNames: selectedNames,
        ),
      );
    }
  }
}
