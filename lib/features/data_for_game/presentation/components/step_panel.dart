import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/features/data_for_game/domain/state/data_for_game_bloc.dart';

/// Панель отображающая шаг заполненения данных перед запуском игры
class StepPanel extends StatefulWidget {
  const StepPanel({super.key});

  @override
  State<StepPanel> createState() => _StepPanelState();
}

class _StepPanelState extends State<StepPanel> {
  int step = 1;

  @override
  Widget build(BuildContext context) {
    return BlocListener<DataForGameBloc, DataForGameState>(
      listener: (context, state) {
        if (state is SelectionSexForGame) setState(() => step = 1);
        if (state is ChoosingQuantityNamesForGame) setState(() => step = 2);
        if (state is ShowNameListForGame) setState(() => step = 3);

      },
      child: Row(
        children: [
          Text('Step $step'),
        ],
      ),
    );
  }
}
