import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/features/data_for_game/domain/state/data_for_game_bloc.dart';
import 'package:name_for_baby/l10n/delegate.dart';
import 'package:name_for_baby/ui_kit/buttons/ui_button_round_icon.dart';

/// Панель выбора пола по которому будет осуществляться выбор имени
class SelectionSexForGamePanel extends StatelessWidget {
  const SelectionSexForGamePanel({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(l10n.whatGenderForGame),
        const IndentVertical(24.0),
        _SexVariantsPanel(),
      ],
    );
  }
}

/// Панель кнопок выбора пола для выборки
class _SexVariantsPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        UiButtonRoundIcon(
          icon: Icons.boy,
          backgroundColor: Colors.blueAccent,
          onPressed: () => _onSelectGender(context, SexValue.boy),
        ),
        UiButtonRoundIcon(
          icon: Icons.girl,
          backgroundColor: Colors.pinkAccent,
          onPressed: () => _onSelectGender(context, SexValue.girl),
        ),
      ],
    );
  }

  /// Событие выбора пола для игры ао выбору имени
  void _onSelectGender(BuildContext context, SexValue sex) {
    context.read<DataForGameBloc>().add(SelectedSex(sex));
  }
}
