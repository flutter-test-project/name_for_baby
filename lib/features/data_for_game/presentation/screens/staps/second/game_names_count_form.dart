import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/features/data_for_game/domain/emums/game_names_count_view_option.dart';
import 'package:name_for_baby/features/data_for_game/domain/state/data_for_game_bloc.dart';
import 'package:name_for_baby/l10n/delegate.dart';
import 'package:name_for_baby/ui_kit/buttons/ui_button_outline.dart';
import 'package:name_for_baby/ui_kit/fields/ui_text_field.dart';

/// Виджет описывающий форму водда колличесва имен для игры
class GameNamesCountForm extends StatelessWidget {
  const GameNamesCountForm({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _InputValueForm(),
            ],
          ),
        ),
        const IndentVertical(24.0),
        _GameNamesCountOptionSwitchButton(),
        const IndentVertical(24.0),
      ],
    );
  }
}

/// Кнопка для выбора значения кол-ва имен для игры из списка
class _GameNamesCountOptionSwitchButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return UiButtonOutline(
      child: Text(l10n.selectFromTheList),
      onPressed: () {
        // Переключение на ручной ввод кол-ва имен для выборки
        context.read<DataForGameBloc>().add(
              ChangedViewOptionGameNameCount(
                GameNamesCountViewOption.list,
              ),
            );
      },
    );
  }
}

class _InputValueForm extends StatefulWidget {
  @override
  State<_InputValueForm> createState() => _InputValueFormState();
}

class _InputValueFormState extends State<_InputValueForm> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _countController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          UiTextFormField(
            controller: _countController,
            validator: _countValidator,
            keyboardType: TextInputType.number,
          ),
          const IndentVertical(12),
          UiButtonOutline(
            onPressed: () => onPressed(context),
            child: Text(l10n.further),
          ),
        ],
      ),
    );
  }

  /// Оюработчик нажатия на кнопку
  void onPressed(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      final count = int.parse(_countController.text.trim());
      _onCountSelection(context, count);
    }
  }

  /// Обработчик события выбора кол-ва имен для игры
  void _onCountSelection(BuildContext context, int quantityName) {
    context.read<DataForGameBloc>().add(ChooseQuantityNameForGame(quantityName));
  }

  /// Метод вадидации поля ввода логина/email
  String? _countValidator(String? value) {
    if (value == null || value.isEmpty) {
      return l10n.validMessageEmpty;
    }
    final count = int.parse(value);
    if (count <= 2) {
      return l10n.valueCannotBeLess('2');
    } else if (count > 100) {
      return l10n.valueCannotBeMore('100');
    }
    return null;
  }
}
