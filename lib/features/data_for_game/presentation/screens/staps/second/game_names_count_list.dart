import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/features/data_for_game/domain/emums/game_names_count_view_option.dart';
import 'package:name_for_baby/features/data_for_game/domain/state/data_for_game_bloc.dart';
import 'package:name_for_baby/l10n/delegate.dart';
import 'package:name_for_baby/ui_kit/buttons/ui_button_outline.dart';
import 'package:name_for_baby/ui_kit/buttons/ui_button_round.dart';

class GameNamesCountList extends StatelessWidget {
  const GameNamesCountList({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                l10n.selectMaximumQuantityNamesForGame,
                textAlign: TextAlign.center,
              ),
              const IndentVertical(24.0),
              _GameNamesCountInputPanel(),
            ],
          ),
        ),
        const IndentVertical(24.0),
        _GameNamesCountOptionSwitchButton(),
        const IndentVertical(24.0),
      ],
    );
  }
}

/// Панель кнопок выбора кол-ва имен для выборки
class _GameNamesCountInputPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DataForGameBloc, DataForGameState>(
      builder: (context, state) {
        if (state is ChoosingQuantityNamesForGame &&
            state.gameNamesCountViewOption == GameNamesCountViewOption.list) {
          return Column(
            children: state.listQuantityNamesForGame
                .map(
                  (quantity) => UiButtonRound(
                    child: Text(
                      quantity.toString(),
                    ),
                    onPressed: () => _onCountSelection(context, quantity),
                  ),
                )
                .toList(),
          );
        }
        return const SizedBox();
      },
      buildWhen: (context, state) {
        if (state is ChoosingQuantityNamesForGame) return true;
        return false;
      },
    );
  }

  /// Обработчик события выбора кол-ва имен для игры
  void _onCountSelection(BuildContext context, int quantityName) {
    context.read<DataForGameBloc>().add(ChooseQuantityNameForGame(quantityName));
  }
}

/// Кнопка для ввода собсвенного значения кол-ва имен для игры
class _GameNamesCountOptionSwitchButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return UiButtonOutline(
      child: Text(l10n.keepYourOwnValue),
      onPressed: () {
        // Переключение на ручной ввод кол-ва имен для выборки
        context.read<DataForGameBloc>().add(
              ChangedViewOptionGameNameCount(
                GameNamesCountViewOption.manual,
              ),
            );
      },
    );
  }
}
