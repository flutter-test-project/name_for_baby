import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';
import 'package:name_for_baby/app/presentation/components/cards/name_card.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/features/data_for_game/domain/state/data_for_game_bloc.dart';
import 'package:name_for_baby/l10n/delegate.dart';
import 'package:name_for_baby/routing/routes.dart';
import 'package:name_for_baby/ui_kit/buttons/ui_button_outline.dart';

/// Панель просмотра имен которые будут учавсвовать в выбооке
class ShowNameListForGamePanel extends StatelessWidget {
  const ShowNameListForGamePanel({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: ListView(
            children: [
              Text(l10n.addNamesFromYourListInOrder),
              const IndentVertical(6),
              _SelectedNamesPanel()
            ],
          ),
        ),
        const IndentVertical(12),
        UiButtonOutline(
          //onPressed: () => onPressed(context),
          child: Text(l10n.launchTheGame),
        ),
        const IndentVertical(12),
      ],
    );
  }
}

/// Панель выбранных имен для игры
class _SelectedNamesPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(l10n.yourList),
        const IndentVertical(6),
        _YourListNamesPanel(),
        const IndentVertical(6),
        _AddNamesButton(),
      ],
    );
  }
}

/// Кнопка добавить имена в выборку
class _AddNamesButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DataForGameBloc, DataForGameState>(
      builder: (context, state) {
        if (state is ShowNameListForGame) {
          return UiButtonOutline(
            onPressed: () => _onPressed(
              context: context,
              sex: state.sex,
              quantityName: state.quantityName,
              selectedNameIds: List<String>.generate(
                state.selectedNames.length,
                (index) => state.selectedNames[index].id,
              ),
            ),
            child: Text(l10n.addNames),
          );
        }
        return const SizedBox();
      },
    );
  }

  /// Метод обработчик события нажатия на кнопку
  ///
  /// Принимает:
  /// - [sex] - пол по которому делаем выборку
  /// - [quantityName] - Кол-во имен для учасвия в выборке
  /// - [selectedNameIds] - Список id выбранных имен для участия в игре
  void _onPressed({
    required BuildContext context,
    required SexValue sex,
    required int quantityName,
    required List<String> selectedNameIds,
  }) {
    NamesForGameRoute(
      sex: sex,
      quantityName: quantityName,
      selectedNameIds: selectedNameIds,
    ).go(context);
  }
}

/// Список имен выбранных пользователем
class _YourListNamesPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DataForGameBloc, DataForGameState>(
      builder: (context, state) {
        if (state is ShowNameListForGame) {
          return Column(
            children: state.selectedNames
                .map(
                  (name) => Padding(
                    padding: const EdgeInsets.only(top: 4),
                    child: NameCard(
                      fullName: name.fullName,
                      onInfoPressed: name is CollectorName
                          ? () => _goToCardName(
                                context: context,
                                name: name,
                              )
                          : null,
                    ),
                  ),
                )
                .toList(),
          );
        }
        return const SizedBox();
      },
    );
  }

  /// Метод позволяюший перейти на страничку карточки имени
  void _goToCardName({
    required BuildContext context,
    required CollectorName name,
  }) {
    CardNameRoute(
      nameId: name.id,
      $extra: name,
    ).push(context);
  }
}
