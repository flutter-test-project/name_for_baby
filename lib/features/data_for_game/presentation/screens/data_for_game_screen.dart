import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:name_for_baby/app/presentation/components/dialogs/error_dialog.dart';
import 'package:name_for_baby/app/presentation/components/dialogs/info_dialog.dart';
import 'package:name_for_baby/app/presentation/components/loading/loading_indicator.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/features/data_for_game/domain/emums/game_names_count_view_option.dart';
import 'package:name_for_baby/features/data_for_game/domain/state/data_for_game_bloc.dart';
import 'package:name_for_baby/features/data_for_game/presentation/components/step_panel.dart';
import 'package:name_for_baby/features/data_for_game/presentation/screens/staps/second/game_names_count_form.dart';
import 'package:name_for_baby/features/data_for_game/presentation/screens/staps/second/game_names_count_list.dart';
import 'package:name_for_baby/features/data_for_game/presentation/screens/staps/first/selection_sex_for_game_panel.dart';
import 'package:name_for_baby/features/data_for_game/presentation/screens/staps/third/show_name_list_for_game_panel.dart';
import 'package:name_for_baby/l10n/delegate.dart';

/// Класс описыыающий экран данных для игры по выбооу имени
class DataForGameScreen extends StatelessWidget {
  const DataForGameScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<DataForGameBloc>(
      create: (BuildContext context) => GetIt.instance()..add(DataForGameStarted()),
      child: Scaffold(
        appBar: AppBar(
          title: Text(l10n.addName),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            children: [
              const StepPanel(),
              const IndentVertical(12.0),
              Expanded(
                child: _DataForGameContent(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/// Виджет определяюший наполненение эрана в зависисмости от состояния
class _DataForGameContent extends StatefulWidget {
  @override
  State<_DataForGameContent> createState() => _DataForGameContentState();
}

class _DataForGameContentState extends State<_DataForGameContent> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<DataForGameBloc, DataForGameState>(
      listener: (context, state) {
        if (state is DataForGameMessage) {
          _showInfoDialog(state.description);
        }
        if (state is DataForGameErrorMessage) {
          _showErrorDialog(
            title: state.title,
            description: state.description,
          );
        }
      },
      builder: (context, state) {
        return switch (state) {
          DataForGameInitial() => const SizedBox(),
          DataForGameLoading() => const LoadingIndicator(),
          SelectionSexForGame() => const SelectionSexForGamePanel(),
          ChoosingQuantityNamesForGame() =>
            state.gameNamesCountViewOption == GameNamesCountViewOption.list
                ? const GameNamesCountList()
                : const GameNamesCountForm(),
          ShowNameListForGame() => const ShowNameListForGamePanel(),

          DataForGameMessage() => const SizedBox(),
          DataForGameFailure() => const SizedBox(),
          DataForGameErrorMessage() => const SizedBox(),
        };
      },
      buildWhen: (context, state) {
        if (state is DataForGameMessage || state is DataForGameErrorMessage) return false;
        return true;
      },
    );
  }

  void _showErrorDialog({
    String? title,
    required String description,
  }) {
    showErrorDialog(
      context: context,
      title: title ?? l10n.mistake,
      description: description,
    );
  }

  void _showInfoDialog(String description) {
    showInfoDialog(
      context: context,
      description: description,
      onPressed: () => Navigator.of(context).pop(),
    );
  }
}
