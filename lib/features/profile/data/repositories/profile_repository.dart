import 'package:name_for_baby/features/preference_by_name/domain/i_local_preference_by_name_repository.dart';
import 'package:name_for_baby/features/profile/domain/entities/count_preference_by_name_name.dart';
import 'package:name_for_baby/features/profile/domain/i_profile_repository.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';

class ProfileRepository implements IProfileRepository {
  ProfileRepository({
    required ILocalPreferenceByNameRepository localPreferenceByNameRepository,
    required IUserSecureDataRepository userSecureDataRepository,
  })  : _localPreferenceByNameRepository = localPreferenceByNameRepository,
        _userSecureDataRepository = userSecureDataRepository;

  final ILocalPreferenceByNameRepository _localPreferenceByNameRepository;
  final IUserSecureDataRepository _userSecureDataRepository;

  @override
  Future<CountPreferenceByName> get countSelectedName async {
    final isAuthentication = await _userSecureDataRepository.isUserAuthentication;
    if (isAuthentication) {
      //todo сделать для авторизованного пользователя поиск в firebase countFavoriteName, countNotFavoriteName, countMyAddName
      return CountPreferenceByName(
        countFavoriteName: 0,
        countNotFavoriteName: 0,
        countPersonalNames: 0,
      );
    } else {
      return CountPreferenceByName(
        countFavoriteName: _localPreferenceByNameRepository.getFavoriteNameIds().length,
        countNotFavoriteName: _localPreferenceByNameRepository.getNotFavoriteNameIds().length,
        countPersonalNames: 0,
      );
    }
  }

  @override
  Future<bool> get isAuthentication => _userSecureDataRepository.isUserAuthentication;
}
