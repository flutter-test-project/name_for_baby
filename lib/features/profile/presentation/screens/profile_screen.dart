import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:name_for_baby/app/presentation/components/dialogs/error_dialog.dart';
import 'package:name_for_baby/app/presentation/components/dialogs/or_dialog.dart';
import 'package:name_for_baby/app/presentation/components/loading/loading_indicator.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/features/profile/domain/state/profile_bloc.dart';
import 'package:name_for_baby/features/profile/presentation/components/info_panel_about_count_items_in_collection.dart';
import 'package:name_for_baby/features/profile/presentation/components/profile_menu_item.dart';
import 'package:name_for_baby/generated/assets/assets.gen.dart';
import 'package:name_for_baby/l10n/delegate.dart';
import 'package:name_for_baby/routing/routes.dart';
import 'package:name_for_baby/ui_kit/general/ui_failure_panel.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProfileBloc>(
      create: (BuildContext context) => GetIt.instance(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Profile'),
        ),
        body: const Padding(
          padding: EdgeInsets.symmetric(horizontal: 12),
          child: ProfileContent(),
        ),
      ),
    );
  }
}

class ProfileContent extends StatefulWidget {
  const ProfileContent({super.key});

  @override
  State<ProfileContent> createState() => _ProfileContentState();
}

class _ProfileContentState extends State<ProfileContent> {
  @override
  void initState() {
    context.read<ProfileBloc>().add(ProfileLoadStarted());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ProfileBloc, ProfileState>(
      listener: (context, state) {
        if (state is ProfileErrorMessage) {
          _showErrorDialog(state.text);
        }
      },
      builder: (context, state) {
        return RefreshIndicator(
          onRefresh: _onRefresh,
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: switch (state) {
              ProfileInitial() => const SizedBox(),
              ProfileProgressLoading() => const LoadingIndicator(),
              ProfileLoadSuccess() => Column(
                  children: [
                    const UsersNameCollectionsPanel(),
                    const IndentVertical(12.0),
                    _MenuContent(),
                  ],
                ),
              ProfileLoadFailure() => UiFailurePanel(
                  state.text,
                  onReload: () => onErrorReload(context),
                ),
              ProfileErrorMessage() => const SizedBox(),
            },
          ),
        );
      },
      buildWhen: (context, state) {
        if (state is ProfileErrorMessage) return false;
        return true;
      },
    );
  }

  /// Метод вывода окна сообщения об ошибке
  void _showErrorDialog(String errorText) {
    showErrorDialog(
      context: context,
      description: errorText,
    );
  }

  /// Метод перезагрузки экрана
  Future<void> _onRefresh() async {
    context.read<ProfileBloc>().add(ProfileLoadStarted());
  }

  /// метод запускающий событие перезвгрузки страницы при ошибки
  void onErrorReload(BuildContext context) {
    context.read<ProfileBloc>().add(ProfileLoadStarted());
  }
}

/// Виджет информ панели кол-ва предпочтительных имен
class UsersNameCollectionsPanel extends StatelessWidget {
  const UsersNameCollectionsPanel({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        if (state is ProfileLoadSuccess) {
          return SizedBox(
            height: 84,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: InfoPanelAboutCountItemsInCollection(
                    iconPath: Assets.svg.icons.dislike.path,
                    count: state.countPreferenceByName.countNotFavoriteName,
                    onTap: () => const NotFavoriteNamesRoute().go(context),
                  ),
                ),
                Expanded(
                  child: InfoPanelAboutCountItemsInCollection(
                    iconPath: Assets.svg.icons.heart.path,
                    count: state.countPreferenceByName.countFavoriteName,
                    onTap: () => const FavoriteNamesRoute().go(context),
                  ),
                ),
                Expanded(
                  child: InfoPanelAboutCountItemsInCollection(
                    iconPath: Assets.svg.icons.add.path,
                    count: state.countPreferenceByName.countPersonalNames,
                    onTap: () {
                      if (state.isAuthorization) {
                        const PersonalListNamesRoute().go(context);
                      } else {
                        /// Скажем пользоваетдю что данный раздел только для зарегестрированных пользователей
                        _showPageOnlyAuthUserDialog(context);
                      }
                    },
                  ),
                ),
              ],
            ),
          );
        }
        return const SizedBox();
      },
      buildWhen: (context, state) {
        if (state is ProfileLoadSuccess) return true;
        return false;
      },
    );
  }
}

class _MenuContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        if (state is ProfileLoadSuccess) {
          return Column(
            children: [
              ProfileMenuItem(
                text: l10n.myPartner,
              ),
              const IndentVertical(4.0),
              ProfileMenuItem(
                text: l10n.patronymic,
                onTap: () {
                  if (state.isAuthorization) {
                    const BabiesPatronymicRoute().go(context);
                  } else {
                    // Скажем пользоваетдю что данный раздел только для зарегестрированных пользователей
                    _showPageOnlyAuthUserDialog(context);
                  }
                },
              ),
              const IndentVertical(4.0),
              ProfileMenuItem(
                text: state.isAuthorization ? l10n.exit : l10n.entrance,
                showArrowRightIcon: false,
                onTap: () {
                  if (!state.isAuthorization) {
                    const AuthorizationRoute().push(context).then((value) {
                      //context.read<ProfileBloc>().add(ProfileLoadStarted());
                    });
                  } else {
                    _showAreYouWantToGetOutDialog(context);
                  }
                },
              ),
            ],
          );
        }
        return const SizedBox();
      },
      buildWhen: (context, state) {
        if (state is ProfileLoadSuccess) return true;
        return false;
      },
    );
  }

  /// Метод вывода окна предупреждения об выходе из аккаунта
  void _showAreYouWantToGetOutDialog(BuildContext context) {
    showOrDialog(
      context: context,
      title: l10n.warning,
      description: l10n.areYouWantToGetOut,
      onLeftPressed: () => context.read<ProfileBloc>().add(ToGetOutEvent()),
    );
  }
}

/// показать диалоговое окно что раздел досткпен только щареганым пользователем
void _showPageOnlyAuthUserDialog(BuildContext context) {
  showOrDialog(
      context: context,
      isShowIcon: false,
      title: l10n.ups,
      description: l10n.sectionForAuthorizedUsers,
      buttonLeftText: l10n.clear,
      buttonRightText: l10n.entrance,
      onRightPressed: () {
        // Перейти к страничке регистрации
        const AuthorizationRoute().go(context);
      });
}
