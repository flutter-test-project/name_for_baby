import 'package:flutter/material.dart';
import 'package:name_for_baby/app/presentation/icons/nfb_svg_icon.dart';
import 'package:name_for_baby/app/presentation/theme/nfb_colors.dart';
import 'package:name_for_baby/app/presentation/theme/nfb_text_styles.dart';
import 'package:name_for_baby/generated/assets/assets.gen.dart';

class ProfileMenuItem extends StatelessWidget {
  const ProfileMenuItem({
    super.key,
    required this.text,
    this.showArrowRightIcon = true,
    this.onTap,
  });

  final String text;
  final bool showArrowRightIcon;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 12.0),
        decoration: BoxDecoration(
          color: Colors.black12,
          border: Border.all(color: Colors.black38),
          borderRadius: const BorderRadius.all(Radius.circular(6.0)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              text,
              style: NfbTextStyles.body1Bold,
            ),
            if (showArrowRightIcon)
              NfbSvgIcon(
                asset: Assets.svg.icons.arrowRight.path,
                color: NfbColors.globalBlack,
                size: 24,
              ),
          ],
        ),
      ),
    );
  }
}
