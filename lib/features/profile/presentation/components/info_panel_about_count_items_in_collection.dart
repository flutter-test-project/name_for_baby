import 'package:flutter/material.dart';
import 'package:name_for_baby/app/presentation/icons/nfb_svg_icon.dart';
import 'package:name_for_baby/app/presentation/theme/nfb_colors.dart';
import 'package:name_for_baby/app/presentation/theme/nfb_text_styles.dart';

class InfoPanelAboutCountItemsInCollection extends StatelessWidget {
  const InfoPanelAboutCountItemsInCollection({
    super.key,
    required this.iconPath,
    required this.count,
    this.onTap,
  });

  final String iconPath;
  final int count;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(border: Border.all(color: Colors.black12)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            NfbSvgIcon(
              asset: iconPath,
              color: NfbColors.globalBlack,
              size: 24,
            ),
            Text(
              '$count',
              style: NfbTextStyles.body1Bold,
            ),
          ],
        ),
      ),
    );
  }
}
