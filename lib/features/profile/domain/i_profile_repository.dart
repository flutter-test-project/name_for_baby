import 'package:name_for_baby/features/profile/domain/entities/count_preference_by_name_name.dart';

/// Интерфейс описывающий работу с репозиторием профиля
abstract interface class IProfileRepository {
  /// Вывести информацию о колличестве товара для расных групп имен
  Future<CountPreferenceByName> get countSelectedName;

  /// Флаг покащывающий авторизован ли пользователь
  Future<bool> get isAuthentication;
}
