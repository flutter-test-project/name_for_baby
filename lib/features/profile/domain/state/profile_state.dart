part of 'profile_bloc.dart';

/// Класс состояний для работы с профитем пользователя
sealed class ProfileState extends Equatable {
  @override
  List<Object> get props => [];
}

/// Класс начального состояния
final class ProfileInitial extends ProfileState {}

/// Класс состояния загрузки данных
final class ProfileProgressLoading extends ProfileState {}

/// Класс состояния полученнных данных о пользователе
final class ProfileLoadSuccess extends ProfileState {
  ProfileLoadSuccess({
    required this.isAuthorization,
    required this.countPreferenceByName,
  });

  /// Флаг показывающий авторизован ли пользователь
  final bool isAuthorization;
  final CountPreferenceByName countPreferenceByName;

  ProfileLoadSuccess copyWith({
    CountPreferenceByName? countNameCollection,
  }) {
    return ProfileLoadSuccess(
      isAuthorization: isAuthorization,
      countPreferenceByName: countNameCollection ?? countPreferenceByName,
    );
  }

  @override
  List<Object> get props => [
        isAuthorization,
        countPreferenceByName,
      ];
}

/// Класс вывода критической ошибки
final class ProfileLoadFailure extends ProfileState {
  ProfileLoadFailure(this.text);

  final String text;
}

/// Класс состония вывода сообщения ошибки
final class ProfileErrorMessage extends ProfileState {
  ProfileErrorMessage(this.text);

  final String text;
}
