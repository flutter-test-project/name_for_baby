import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/event_bus/event_bus.dart';
import 'package:name_for_baby/features/event_bus/event_bus_events.dart';
import 'package:name_for_baby/features/names_book/domain/usecase/delete_last_visible_name_id_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/i_local_preference_by_name_repository.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/get_count_preference_by_name_usecase.dart';
import 'package:name_for_baby/features/profile/domain/entities/count_preference_by_name_name.dart';
import 'package:name_for_baby/features/user_secure_data/domain/usecase/clean_user_secure_data_usecase.dart';
import 'package:name_for_baby/features/user_secure_data/domain/usecase/is_user_authentication_usecase.dart';

part 'profile_event.dart';

part 'profile_state.dart';

/// Блок описывающий работу с профилем пользователя
class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc({
    required ILocalPreferenceByNameRepository localPreferenceByNameRepository,
    required GetCountPreferenceByNameUsecase getCountPreferenceByNameUsecase,
    required IsUserAuthenticationUsecase isUserAuthenticationUsecase,
    required CleanUserSecureDataUsecase cleanUserSecureDataUsecase,
    required DeleteLastVisibleNameIdUsecase deleteLastVisibleNameIdUsecase,
    required EventBus eventBus,
  })  : _getCountPreferenceByNameUsecase = getCountPreferenceByNameUsecase,
        _isUserAuthenticationUsecase = isUserAuthenticationUsecase,
        _cleanUserSecureDataUsecase = cleanUserSecureDataUsecase,
        _deleteLastVisibleNameIdUsecase = deleteLastVisibleNameIdUsecase,
        _eventBus = eventBus,
        super(ProfileInitial()) {
    on<ProfileLoadStarted>(_onLoadStarted);
    on<ProfileEditedCountSelectedNameEvent>(_editCountSelectedName);
    on<ToGetOutEvent>(_toGetOutEvent);

    _streamSubscription = _eventBus.on<EventBusEvents>().listen(
      (event) {
        if (event is EditCountSelectedNameEvent) {
          add(
            ProfileEditedCountSelectedNameEvent(
              countFavoriteName: event.countFavoriteName,
              countNotFavoriteName: event.countNotFavoriteName,
              countPersonalName: event.countPersonalName,
            ),
          );
        }
        if (event is UserAuthEvent) {
          add(ProfileLoadStarted());
        }
      },
    );
  }

  final GetCountPreferenceByNameUsecase _getCountPreferenceByNameUsecase;
  final IsUserAuthenticationUsecase _isUserAuthenticationUsecase;
  final CleanUserSecureDataUsecase _cleanUserSecureDataUsecase;
  final DeleteLastVisibleNameIdUsecase _deleteLastVisibleNameIdUsecase;

  final EventBus _eventBus;
  late final StreamSubscription _streamSubscription;

  @override
  Future<void> close() {
    _streamSubscription.cancel();
    return super.close();
  }

  Future<void> _onLoadStarted(
    ProfileLoadStarted event,
    Emitter<ProfileState> emit,
  ) async {
    try {
      emit(ProfileProgressLoading());
      final CountPreferenceByName countPreferenceByName =
          await _getCountPreferenceByNameUsecase.call(NoParams());
      final bool isAuthentication = await _isUserAuthenticationUsecase.call(NoParams());

      emit(
        ProfileLoadSuccess(
          isAuthorization: isAuthentication,
          countPreferenceByName: countPreferenceByName,
        ),
      );
    } catch (error, stackTrace) {
      emit(ProfileLoadFailure('$error'));
      addError(error, stackTrace);
    }
  }

  Future<void> _editCountSelectedName(
    ProfileEditedCountSelectedNameEvent event,
    Emitter<ProfileState> emit,
  ) async {
    final oldState = state;
    try {
      if (oldState is ProfileLoadSuccess) {
        final CountPreferenceByName countSelectedName = oldState.countPreferenceByName.copyWith(
          countFavoriteName: event.countFavoriteName,
          countNotFavoriteName: event.countNotFavoriteName,
          countPersonalName: event.countPersonalName,
        );
        emit(oldState.copyWith(countNameCollection: countSelectedName));
      }
    } catch (error, stackTrace) {
      emit(ProfileLoadFailure('$error'));
      addError(error, stackTrace);
    }
  }

  Future<void> _toGetOutEvent(
    ToGetOutEvent event,
    Emitter<ProfileState> emit,
  ) async {
    try {
      emit(ProfileProgressLoading());
      await _cleanUserSecureDataUsecase.call(NoParams());
      _deleteLastVisibleNameIdUsecase.call(NoParams());
      add(ProfileLoadStarted());
    } catch (error, stackTrace) {
      emit(ProfileErrorMessage('$error'));
      addError(error, stackTrace);
    }
  }
}
