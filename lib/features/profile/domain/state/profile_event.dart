part of 'profile_bloc.dart';

@immutable
sealed class ProfileEvent {
  const ProfileEvent();
}

/// Событие загрузки страницы профиля
final class ProfileLoadStarted extends ProfileEvent {}

/// Событие измененения кол-ва выбранных коллекций имен
final class ProfileEditedCountSelectedNameEvent extends ProfileEvent {
  const ProfileEditedCountSelectedNameEvent({
    this.countFavoriteName,
    this.countNotFavoriteName,
    this.countPersonalName,
  });

  /// Кол-во понравившихся имен пользователю
  final int? countFavoriteName;

  /// Кол-во Не понравившихся имен пользователю
  final int? countNotFavoriteName;

  /// Кол-во имен в личной коллекции пользователя
  final int? countPersonalName;
}

/// Событие выхода из приложения
final class ToGetOutEvent extends ProfileEvent {}
