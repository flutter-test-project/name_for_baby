/// Класс описывающий кол-во имен в предпочтительных коллекциях
///
/// Принимает
/// - [countFavoriteName] - кол-во понравившихся имен
/// - [countNotFavoriteName] - кол-во не понравившихся имен
/// - [countPersonalNames] - кол-во добавленных имен
class CountPreferenceByName {
  CountPreferenceByName({
    required this.countFavoriteName,
    required this.countNotFavoriteName,
    required this.countPersonalNames,
  });

  final int countFavoriteName;
  final int countNotFavoriteName;
  final int countPersonalNames;

  CountPreferenceByName copyWith({
    int? countFavoriteName,
    int? countNotFavoriteName,
    int? countPersonalName,
  }) {
    return CountPreferenceByName(
      countFavoriteName: countFavoriteName ?? this.countFavoriteName,
      countNotFavoriteName: countNotFavoriteName ?? this.countNotFavoriteName,
      countPersonalNames: countPersonalName ?? countPersonalNames,
    );
  }
}
