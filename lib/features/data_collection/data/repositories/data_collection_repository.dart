import 'package:excel/excel.dart';
import 'package:flutter/material.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/domain/entities/tag.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';
import 'package:name_for_baby/features/data_collection/data/collection/name/boy/boy_collection.dart';
import 'package:name_for_baby/features/data_collection/data/collection/name/girl/girl_collection.dart';
import 'package:name_for_baby/features/data_collection/data/collection/tag_collection.dart';
import 'package:name_for_baby/features/data_collection/data/mappers/celebrity_collection_mapper.dart';
import 'package:name_for_baby/features/data_collection/data/mappers/name_collection_mapper.dart';
import 'package:name_for_baby/features/data_collection/domain/i_data_collection_repository.dart';
import 'package:name_for_baby/features/data_collection/domain/services/i_data_collection_service.dart';
import 'package:flutter/services.dart' show ByteData, rootBundle;

class DataCollectionRepository implements IDataCollectionRepository {
  DataCollectionRepository({
    required IDataCollectionService dataCollectionService,
  }) : _dataCollectionService = dataCollectionService;

  final IDataCollectionService _dataCollectionService;

  @override
  Future<void> insertDataInNameCollection() async {
    List<CollectorName> names = [];
    names.addAll(girlCollection);
    names.addAll(boyCollection);
    for (final name in names) {
      final Map<String, dynamic> map = NameCollectionMapper().serialize(name);
      final String docId = name.id;
      await _dataCollectionService.insertDataInNameCollection(
        data: map,
        docId: docId,
      );
      for (final celebrity in name.celebrities) {
        final Map<String, dynamic> map = CelebrityCollectionMapper().serialize(celebrity);

        await _dataCollectionService.insertCelebrities(data: map, docId: docId);
      }
    }
  }

  @override
  Future<List<CollectorName>> readCsvCollection(
    SexValue sex,
    Map<String, List> tagsCollection,
  ) async {
    List<CollectorName> list = [];
    late ByteData data;
    if (sex == SexValue.boy) {
      data = await rootBundle.load("assets/csv/boy_collection.xlsx");
    } else {
      data = await rootBundle.load("assets/csv/girl_collection.xlsx");
    }

    var bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    var excel = Excel.decodeBytes(bytes);
    for (var table in excel.tables.keys) {
      debugPrint(table);
      for (final row in excel.tables[table]!.rows) {
        late int popularity;
        try {
          popularity = int.parse(row[3]!.value.toString());
        } catch (error) {
          popularity = 0;
        }
        if (row[4]!.value != null && row[0]!.value != null && row[2]!.value != null) {
          try {
            final String fullName = row[0]!.value.toString();
            final List<Tag> tags = _getTags(fullName, tagsCollection);
            final CollectorName name = CollectorName(
              id: row[4]!.value.toString(),
              fullName: fullName,
              churchName: row[5]!.value == null ? '' : row[5]!.value.toString(),
              shortForms: row[6]!.value == null ? '' : row[6]!.value.toString(),
              synonyms: row[7] == null ? '' : row[7]!.value.toString(),
              sex: sex,
              origin: row[8] == null ? '' : row[8]!.value.toString(),
              value: row[2]!.value.toString(),
              history: '',
              personality: row[9] == null ? '' : row[9]!.value.toString(),
              popularity: popularity,
              tags: tags,
              celebrities: const [],
            );
            list.add(name);
          } catch (error) {
            debugPrint('$error');
          }
        } else {
          debugPrint('$row');
        }
      }
    }
    return list;
  }

  @override
  Future<Map<String, List>> readCvgTagCollection() async {
    Map<String, List> map = {};

    final ByteData data = await rootBundle.load("assets/csv/tags_collections.xlsx");
    var bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    var excel = Excel.decodeBytes(bytes);
    for (final table in excel.tables.keys) {
      debugPrint(table.toString());
      List<String> list = [];
      final Tag? tag = getTagValue(table);
      if (tag != null) {
        for (final row in excel.tables[table]!.rows) {
          if (row[0] != null) {
            list.add(row[0]!.value.toString());
          }
        }
        map[table] = list;
      }
    }
    return map;
  }

  @override
  Future<void> saveCollection(List<CollectorName> list) async {
    for (final name in list) {
      final Map<String, dynamic> map = NameCollectionMapper().serialize(name);
      final String docId = name.id;
      await _dataCollectionService.insertDataInNameCollection(
        data: map,
        docId: docId,
      );
    }
  }

  List<Tag> _getTags(String name, Map<String, List> map) {
    List<Tag> tags = [];
    map.forEach((key, list) {
      final index =
          list.indexWhere((value) => value.toString().toUpperCase() == name.toUpperCase());
      if (index != -1) {
        final Tag? tag = getTagValue(key);
        if (tag != null) {
          tags.add(tag);
        }
      }
    });
    return tags;
  }
}
