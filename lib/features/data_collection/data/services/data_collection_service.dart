import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:name_for_baby/app/data/collections/firestore_collections.dart';
import 'package:name_for_baby/features/data_collection/domain/services/i_data_collection_service.dart';

class DataCollectionService implements IDataCollectionService {
  final FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  Future<void> insertDataInNameCollection({
    required Map<String, dynamic> data,
    required String docId,
  }) async {
    await db
        .collection(FirestoreCollections.nameCollection)
        .doc(docId)
        .set(data);
  }

  @override
  Future<void> insertCelebrities(
      {required Map<String, dynamic> data, required String docId}) async {
    final String collection =
        '${FirestoreCollections.nameCollection}/$docId/${FirestoreNameCollections.celebrityCollection}';

    await db.collection(collection).doc().set(data);
  }
}
