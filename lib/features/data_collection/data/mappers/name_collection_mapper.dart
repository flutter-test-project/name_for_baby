import 'package:name_for_baby/app/data/collections/fields/name_data_fields.dart';
import 'package:name_for_baby/app/data/mapper.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';

class NameCollectionMapper implements Mapper<CollectorName, Map<String, dynamic>> {
  @override
  CollectorName deserialize(Map<String, dynamic> data) {
    // TODO: implement deserialize
    throw UnimplementedError();
  }

  @override
  Map<String, dynamic> serialize(CollectorName data) {
    List<String> tags = [];
    for (final tag in data.tags) {
      tags.add(tag.value);
    }
    return {
      NameDataFields.id: data.id,
      NameDataFields.fullName: data.fullName,
      NameDataFields.churchName: data.churchName,
      NameDataFields.shortForms: data.shortForms,
      NameDataFields.synonyms: data.synonyms,
      NameDataFields.sex: data.sex.name,
      NameDataFields.origin: data.origin,
      NameDataFields.value: data.value,
      NameDataFields.history: data.history,
      NameDataFields.popularity: data.popularity,
      NameDataFields.personality: data.personality,
      NameDataFields.tags: tags,
    };
  }
}
