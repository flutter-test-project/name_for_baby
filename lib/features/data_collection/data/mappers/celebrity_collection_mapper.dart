import 'package:name_for_baby/app/data/collections/fields/celebrity_data_fields.dart';
import 'package:name_for_baby/app/data/mapper.dart';
import 'package:name_for_baby/features/data_collection/domain/entities/celebrity.dart';

class CelebrityCollectionMapper
    implements Mapper<Celebrity, Map<String, dynamic>> {
  @override
  Celebrity deserialize(Map<String, dynamic> data) {
    // TODO: implement deserialize
    throw UnimplementedError();
  }

  @override
  Map<String, dynamic> serialize(Celebrity data) {
    return {
      CelebrityDataFields.fullName: data.fullname,
      CelebrityDataFields.description: data.description,
      CelebrityDataFields.photo: data.photo,
    };
  }
}
