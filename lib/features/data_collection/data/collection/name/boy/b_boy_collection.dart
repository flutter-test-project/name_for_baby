
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';

List<CollectorName> get bBoyCollection => [];


const name = CollectorName(
  id: 'b_ru_',
  fullName: '',
  churchName: '',
  shortForms: '',
  synonyms: '',
  sex: SexValue.boy,
  origin: '',
  value: '',
  history: '',
  personality: '',
  popularity: 0,
  tags: [],
  celebrities: [],
);
