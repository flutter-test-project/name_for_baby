import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/features/data_collection/data/collection/name/boy/a_boy_collection.dart';
import 'package:name_for_baby/features/data_collection/data/collection/name/boy/b_boy_collection.dart';

List<CollectorName> get boyCollection => [
      ...aBoyCollection,
      ...bBoyCollection,
    ];
