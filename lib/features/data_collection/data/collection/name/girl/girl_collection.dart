import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/features/data_collection/data/collection/name/girl/a_girl_collection.dart';

List<CollectorName> get girlCollection => [
      ...aGirlCollection,
    ];
