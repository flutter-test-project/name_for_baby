import 'package:name_for_baby/app/domain/entities/tag.dart';

List<Tag> get getTags => [
      rus,
      latin,
      gre,
      dreGre,
      evro,
      slava,
    ];

Tag? getTagValue(String value) {
  final List<Tag> tags = getTags;
  for (final tag in tags) {
    if (tag.value == value) {
      return tag;
    }
  }
  return null;
}

const Tag rus = Tag(id: '1', value: 'Русское');
const Tag latin = Tag(id: '2', value: 'Латинское');
const Tag gre = Tag(id: '3', value: 'Греческое');
const Tag dreGre = Tag(id: '4', value: 'Древнегреческое');
const Tag evro = Tag(id: '5', value: 'Еврейское');
const Tag slava = Tag(id: '6', value: 'Славянское');
