import 'package:equatable/equatable.dart';

class TagCollection extends Equatable {
  const TagCollection({
    required this.russians,
    required this.latin,
    required this.greek,
    required this.ancientGreek,
    required this.jewish,
    required this.slavic,
  });

  final List<String> russians;
  final List<String> latin;
  final List<String> greek;
  final List<String> ancientGreek;
  final List<String> jewish;
  final List<String> slavic;

  @override
  List<Object?> get props => [];
}
