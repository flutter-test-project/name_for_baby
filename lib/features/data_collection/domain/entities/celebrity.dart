import 'package:equatable/equatable.dart';

/// Класс описываюший знаменитую личность
class Celebrity extends Equatable {
  const Celebrity({
    required this.fullname,
    required this.description,
    required this.photo,
  });

  /// имя
  final String fullname;

  /// описание чем популярна лисность
  final String description;

  /// фото
  final String photo;

  @override
  List<Object?> get props => [fullname, description, photo];
}
