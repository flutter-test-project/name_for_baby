import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';

abstract interface class IDataCollectionRepository {
  Future<void> insertDataInNameCollection();

  /// Метод, который считывает имена из csv файла
  ///
  /// Принимает:
  /// - [sex] - пол
  /// - [tagsCollection] - коллекция групп имен
  Future<List<CollectorName>> readCsvCollection(
    SexValue sex,
    Map<String, List> tagsCollection,
  );

  Future<Map<String, List>> readCvgTagCollection();

  Future<void> saveCollection(List<CollectorName> names);
}
