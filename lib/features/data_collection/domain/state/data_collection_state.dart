part of 'data_collection_cubit.dart';

sealed class DataCollectionState extends Equatable {
  @override
  List<Object> get props => [];
}

class DataCollectionInitial extends DataCollectionState {}

class DataCollectionLoading extends DataCollectionState {}

class DataCollectionSuccess extends DataCollectionState {}

class DataCollectionError extends DataCollectionState {
  DataCollectionError({
    required this.message,
  });

  /// Сообщение об ошибке
  final String message;
}
