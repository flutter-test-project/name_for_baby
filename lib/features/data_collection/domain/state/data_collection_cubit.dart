import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';
import 'package:name_for_baby/features/data_collection/domain/i_data_collection_repository.dart';

part 'data_collection_state.dart';

class AddProductQrScanCubit extends Cubit<DataCollectionState> {
  AddProductQrScanCubit(
    this.dataCollectionRepository,
  ) : super(DataCollectionInitial());
  final IDataCollectionRepository dataCollectionRepository;

  Future<void> createNameCollection() async {
    try {
      emit(DataCollectionLoading());
      //await iDataCollectionRepository.insertDataInNameCollection();
      final Map<String, List> tags =
          await dataCollectionRepository.readCvgTagCollection();
      final List<CollectorName> boyNames =
          await dataCollectionRepository.readCsvCollection(SexValue.boy, tags);
      final List<CollectorName> girlNames =
          await dataCollectionRepository.readCsvCollection(SexValue.girl, tags);
      debugPrint('$girlNames');
      await dataCollectionRepository.saveCollection(boyNames);
      await dataCollectionRepository.saveCollection(girlNames);
      emit(DataCollectionSuccess());
    } catch (error) {
      emit(DataCollectionError(message: error.toString()));
    }
  }
}
