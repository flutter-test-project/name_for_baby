abstract interface class IDataCollectionService {
  Future<void> insertDataInNameCollection({
    required Map<String, dynamic> data,
    required String docId,
  });

  Future<void> insertCelebrities({
    required Map<String, dynamic> data,
    required String docId,
  });
}
