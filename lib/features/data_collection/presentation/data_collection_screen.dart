import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:name_for_baby/app/presentation/components/loading/loading_indicator.dart';
import 'package:name_for_baby/features/data_collection/domain/state/data_collection_cubit.dart';

class DataCollectionScreen extends StatelessWidget {
  const DataCollectionScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => AddProductQrScanCubit(
        GetIt.instance(),
      ),
      child: const CreateNamePage(),
    );
  }
}

class CreateNamePage extends StatelessWidget {
  const CreateNamePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: BlocBuilder<AddProductQrScanCubit, DataCollectionState>(
        builder: (context, state) {
          switch (state) {
            case DataCollectionInitial():
              return _InitialContent();
            case DataCollectionLoading():
              return const Center(child: LoadingIndicator());
            case DataCollectionSuccess():
              return _SuccessMessageContent();
            case DataCollectionError():
              return const _ErrorMessageContent();
          }
        },
      ),
    );
  }
}

class _InitialContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: () => _createNameCollection(context),
        child: const Text('Cоздать новые модели имени'),
      ),
    );
  }
}

class _SuccessMessageContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Новые модели были созданы'),
    );
  }
}

class _ErrorMessageContent extends StatelessWidget {
  const _ErrorMessageContent();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AddProductQrScanCubit, DataCollectionState>(
      builder: (context, state) {
        if (state is DataCollectionError) {
          return Center(
            child: Text(state.message),
          );
        }
        return const SizedBox();
      },
    );
  }
}

void _createNameCollection(BuildContext context) {
  context.read<AddProductQrScanCubit>().createNameCollection();
}
