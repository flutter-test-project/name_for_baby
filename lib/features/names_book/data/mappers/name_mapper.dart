import 'package:name_for_baby/app/data/mapper.dart';
import 'package:name_for_baby/app/data/dto/name_dto.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';

class NameMapper implements Mapper<CollectorName, NameDto> {
  @override
  CollectorName deserialize(NameDto data) {
    return CollectorName(
      id: data.id,
      fullName: data.fullName,
      churchName: data.churchName,
      shortForms: data.shortForms,
      synonyms: data.synonyms,
      sex: getMarkState(data.sex) ?? SexValue.notDefined,
      origin: data.origin,
      value: data.value,
      history: data.history,
      personality: data.personality,
      popularity: 0,
      tags: const [],
      celebrities: const [],
    );
  }

  @override
  NameDto serialize(CollectorName data) {
    return NameDto(
      id: data.id,
      fullName: data.fullName,
      churchName: data.churchName,
      shortForms: data.shortForms,
      synonyms: data.synonyms,
      sex: data.sex.description,
      origin: data.origin,
      value: data.value,
      history: data.history,
      personality: data.personality,
    );
  }
}
