import 'package:name_for_baby/app/data/dto/tag_dto.dart';
import 'package:name_for_baby/app/data/mapper.dart';
import 'package:name_for_baby/app/domain/entities/tag.dart';

class TagMapper implements Mapper<Tag, TagDto> {
  @override
  Tag deserialize(TagDto data) {
    return Tag(
      id: data.id,
      value: data.value,
    );
  }

  @override
  TagDto serialize(Tag data) {
    return TagDto(
      id: data.id,
      value: data.value,
    );
  }
}
