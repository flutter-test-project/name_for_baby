import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:name_for_baby/app/data/collections/fields/name_data_fields.dart';
import 'package:name_for_baby/app/data/collections/firestore_collections.dart';
import 'package:name_for_baby/app/data/dto/name_dto.dart';
import 'package:name_for_baby/app/data/dto/tag_dto.dart';
import 'package:name_for_baby/app/domain/entities/tag.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';
import 'package:name_for_baby/features/names_book/domain/services/i_names_book_remove_db_service.dart';

class NamesBookFirestoreService implements INamesBookRemoveDbService {
  final FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  Future<List<NameDto>> getNames({
    SexValue? sex,
    List<Tag> selectTags = const [],
    int limit = 10,
    String lastVisibleNameId = '',
  }) async {
    //отпределим последний просмотренный документ имени
    DocumentSnapshot<Map<String, dynamic>>? lastVisibleName;
    if (lastVisibleNameId.isNotEmpty) {
      lastVisibleName =
          await db.collection(FirestoreCollections.nameCollection).doc(lastVisibleNameId).get();
    }

    List<NameDto> names = [];
    late QuerySnapshot<Map<String, dynamic>> querySnapshot;

    // формиркем поисковый запрос
    Query<Map<String, dynamic>> query = db
        .collection(FirestoreCollections.nameCollection)
        .orderBy(NameDataFields.popularity, descending: true);

    // добавить в запрос фильтр по полу
    if (sex != null) {
      query = query.where(
        NameDataFields.sex,
        isEqualTo: sex.name,
      );
    }

    // добавить фильтр по группе имени
    if (selectTags.isNotEmpty) {
      final tagCollection = List<String>.generate(
        selectTags.length,
        (index) => selectTags[index].value,
      );

      query = query.where(
        NameDataFields.tags,
        arrayContainsAny: tagCollection,
      );
    }

    // добавить в запрос поиск от последнего просмотренного имени
    if (lastVisibleName != null) {
      query = query.startAfterDocument(lastVisibleName);
    }

    query = query.limit(limit);

    querySnapshot = await query.get();

    for (var doc in querySnapshot.docs) {
      final NameDto nameDto = NameDto.fromFirestore(doc.data(), doc.id);
      names.add(nameDto);
    }

    return names;
  }

  @override
  Future<List<TagDto>> getTags() async {
    List<TagDto> tags = [];

    final QuerySnapshot<Map<String, dynamic>> querySnapshot =
        await db.collection(FirestoreCollections.tagCollection).get();

    for (var doc in querySnapshot.docs) {
      final TagDto nameDto = TagDto.fromFirestore(doc.data(), doc.id);
      tags.add(nameDto);
    }

    return tags;
  }
}
