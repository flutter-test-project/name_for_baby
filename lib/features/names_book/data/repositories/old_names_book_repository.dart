import 'package:name_for_baby/app/data/dto/name_dto.dart';
import 'package:name_for_baby/app/data/dto/tag_dto.dart';
import 'package:name_for_baby/app/domain/entities/tag.dart';
import 'package:name_for_baby/features/names_book/data/mappers/name_mapper.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/features/names_book/data/mappers/tag_mapper.dart';
import 'package:name_for_baby/features/names_book/domain/entities/names_book_filter.dart';
import 'package:name_for_baby/features/names_book/domain/i_old_names_book_repository.dart';
import 'package:name_for_baby/features/names_book/domain/services/i_names_book_remove_db_service.dart';
import 'package:name_for_baby/features/preference_by_name/domain/i_local_preference_by_name_repository.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Класс для реализации методов фичи списка имен
class OldNamesBookRepository implements IOldNamesBookRepository {
  OldNamesBookRepository({
    required INamesBookRemoveDbService removeDbService,
    required ILocalPreferenceByNameRepository localPreferenceByNameRepository,
    required IUserSecureDataRepository userSecureDataRepository,
    required SharedPreferences prefs,
  })  : _removeDbService = removeDbService,
        _prefs = prefs;
  final INamesBookRemoveDbService _removeDbService;
  final SharedPreferences _prefs;

  List<Tag> _tags = [];

  @override
  Future<List<CollectorName>> getNames({
    required NamesBookFilter filter,
    int limit = 10,
  }) async {
    var excludeNameIds = Set<String>.of(filter.favoriteNameIds);
    excludeNameIds.addAll(filter.notFavoriteNameIds);

    final List<NameDto> namesDtos = await _removeDbService.getNames(
      sex: filter.selectSexCollection.length == 1 ? filter.selectSexCollection.first : null,
      selectTags: filter.selectTags,
      limit: limit,
      lastVisibleNameId: lastVisibleNameId,
    );
    if (namesDtos.isEmpty) return [];

    List<CollectorName> names = [];

    for (var dto in namesDtos) {
      if (!excludeNameIds.contains(dto.id)) {
        names.add(NameMapper().deserialize(dto));
      }
    }

    if (names.length < limit) {
      await saveLastVisibleNameId(namesDtos.last.id);
      final list = await getNames(filter: filter, limit: limit);
      for (final l in list) {
        names.add(l);
      }
    }

    return names;
  }

  @override
  String get lastVisibleNameId => _prefs.getString(DataAccessKeyStorage.lastNameId) ?? '';

  @override
  Future<List<Tag>> getTags() async {
    if (_tags.isNotEmpty) return _tags;
    final List<TagDto> tags = await _removeDbService.getTags();
    _tags = List.generate(
      tags.length,
      (index) => TagMapper().deserialize(
        tags[index],
      ),
    );
    return _tags;
  }

  @override
  Future<void> saveLastVisibleNameId(String value) async {
    await _prefs.setString(
      DataAccessKeyStorage.lastNameId,
      value,
    );
  }

  @override
  Future<void> deleteLastVisibleNameId() async {
    await _prefs.remove(DataAccessKeyStorage.lastNameId);
  }
}

/// Хранилище ключей доступов к данным
class DataAccessKeyStorage {
  static const String lastNameId = 'last_name_id';
}
