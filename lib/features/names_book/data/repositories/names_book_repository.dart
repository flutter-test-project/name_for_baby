import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:name_for_baby/app/data/collections/fields/name_data_fields.dart';
import 'package:name_for_baby/app/data/collections/firestore_collections.dart';
import 'package:name_for_baby/app/data/dto/name_dto.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/features/names_book/data/mappers/name_mapper.dart';
import 'package:name_for_baby/features/names_book/domain/i_names_book_repository.dart';

class NamesBookRepository implements INamesBookRepository {
  final FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  Future<List<CollectorName>> getNames({required List<String> nameIds}) async {
    List<CollectorName> names = [];
    late QuerySnapshot<Map<String, dynamic>> querySnapshot;
    Query<Map<String, dynamic>> query = db.collection(FirestoreCollections.nameCollection);

    query = query.where(
      NameDataFields.id,
      whereIn: nameIds,
    );

    querySnapshot = await query.get();

    for (var doc in querySnapshot.docs) {
      final NameDto nameDto = NameDto.fromFirestore(doc.data(), doc.id);
      names.add(NameMapper().deserialize(nameDto));
    }
    return names;
  }
}
