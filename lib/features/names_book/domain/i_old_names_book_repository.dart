import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/domain/entities/tag.dart';
import 'package:name_for_baby/features/names_book/domain/entities/names_book_filter.dart';

abstract interface class IOldNamesBookRepository {
  /// Возвращает список имен
  Future<List<CollectorName>> getNames({
    required NamesBookFilter filter,
    int limit = 10,
  });

  /// Возращает id последнего просмотренного имени
  String get lastVisibleNameId;

  /// Возвращает список тегов имен
  Future<List<Tag>> getTags();

  /// Сохраняет значение id последнего просмотренного имени
  ///
  /// Принимает
  /// - [value] - id имени
  Future<void> saveLastVisibleNameId(String value);

  /// Метод который удалят id последнего просмотренного имени
  Future<void> deleteLastVisibleNameId();
}
