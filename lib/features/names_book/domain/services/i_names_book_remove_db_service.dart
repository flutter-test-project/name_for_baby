import 'package:name_for_baby/app/data/dto/name_dto.dart';
import 'package:name_for_baby/app/data/dto/tag_dto.dart';
import 'package:name_for_baby/app/domain/entities/tag.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';

/// Абстрактный сервис для работы с удаленной бд
abstract interface class INamesBookRemoveDbService {
  /// вернет список продуктов из удаленной бд
  ///
  /// Принимает:
  /// - [selectSexCollection] - Коллекция всех возможных тегов имен
  /// - [selectTags] - Коллекция выбранных тегов
  /// - [excludeNameIds] - Исключенные имена из поиска
  /// - [limit] - лимит по выводу записей
  /// - [lastVisibleNameId] - последнее полученное имя
  Future<List<NameDto>> getNames({
    SexValue? sex,
    List<Tag> selectTags = const [],
    int limit = 10,
    String lastVisibleNameId = '',
  });

  Future<List<TagDto>> getTags();
}
