import 'package:equatable/equatable.dart';
import 'package:name_for_baby/app/domain/entities/tag.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';

class NamesBookFilter extends Equatable {
  /// Класс фильтра коллекции имен
  ///
  /// Принимает:
  /// - [allTags] - Коллекция всех возможных тегов имен
  /// - [selectTags] - Коллекция выбранных тегов
  /// - [allSexCollection] - Коллекция всех возможных полов имен для фильтра
  /// - [selectSexCollection] - Коллекция выбранных имен для фильтра
  /// - [favoriteNameIds] - Коллекция ид понравившихся имен
  /// - [notFavoriteNameIds] - Коллекция ид не понравившихся имен
  const NamesBookFilter({
    required this.allTags,
    this.selectTags = const [],
    this.allSexCollection = const [SexValue.boy, SexValue.girl],
    this.selectSexCollection = const [SexValue.boy, SexValue.girl],
    this.favoriteNameIds = const [],
    this.notFavoriteNameIds = const [],
  });

  final List<Tag> allTags;
  final List<Tag> selectTags;
  final List<SexValue> allSexCollection;
  final List<SexValue> selectSexCollection;
  final List<String> favoriteNameIds;
  final List<String> notFavoriteNameIds;

  NamesBookFilter copyWith({
    List<Tag>? selectTags,
    List<SexValue>? selectSexCollection,
    List<String>? favoriteNameIds,
    List<String>? notFavoriteNameIds,
  }) {
    return NamesBookFilter(
      allTags: allTags,
      selectTags: selectTags ?? this.selectTags,
      allSexCollection: allSexCollection,
      selectSexCollection: selectSexCollection ?? this.selectSexCollection,
      favoriteNameIds: favoriteNameIds ?? this.favoriteNameIds,
      notFavoriteNameIds: notFavoriteNameIds ?? this.notFavoriteNameIds,
    );
  }

  @override
  List<Object?> get props => [
        allTags,
        selectTags,
        allSexCollection,
        selectSexCollection,
        favoriteNameIds,
        notFavoriteNameIds,
      ];
}
