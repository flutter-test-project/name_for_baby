import 'package:name_for_baby/app/domain/entities/collector_name.dart';

/// Интерфейс репозитория по работе с именами
abstract interface class INamesBookRepository {
  /// Метод который возвращает списое имен
  ///
  /// Принимает
  /// - [nameIds] - Список id имен которые необходимо вернуть
  Future<List<CollectorName>> getNames({
    required List<String> nameIds,
  });
}
