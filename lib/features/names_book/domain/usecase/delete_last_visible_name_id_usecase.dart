import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/names_book/domain/i_old_names_book_repository.dart';

/// Юз кейс который удаляет данные о id последнего просмотренном имени
class DeleteLastVisibleNameIdUsecase extends BaseUseCase<Future<void>, NoParams> {
  DeleteLastVisibleNameIdUsecase({
    required IOldNamesBookRepository oldNamesBlocRepository,
  }) : _oldNamesBlocRepository = oldNamesBlocRepository;

  final IOldNamesBookRepository _oldNamesBlocRepository;

  @override
  Future<void> call(NoParams params) async {
    await _oldNamesBlocRepository.deleteLastVisibleNameId();
  }
}
