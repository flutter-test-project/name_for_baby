import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/names_book/domain/i_names_book_repository.dart';

/// Юз кейс который возвращает список имен∆
class GetNamesUsecase extends BaseUseCase<Future<List<CollectorName>>, List<String>> {
  GetNamesUsecase({
    required INamesBookRepository namesBlocRepository,
  }) : _namesBlocRepository = namesBlocRepository;

  final INamesBookRepository _namesBlocRepository;

  @override
  Future<List<CollectorName>> call(List<String> params) async {
    return _namesBlocRepository.getNames(nameIds: params);
  }
}
