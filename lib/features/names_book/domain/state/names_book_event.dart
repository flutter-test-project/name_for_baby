part of 'names_book_bloc.dart';

/// Класс событий книги имен
@immutable
sealed class NamesBookEvent {}

/// Класс загрузки имен
final class NamesBookStartedLoading extends NamesBookEvent {}

/// Класc события просмотра последнего загруженного имени
///
/// Принимает:
/// - [lastVisibleName] - последнее просмотренное имя
final class NamesBookShowedTheLastName extends NamesBookEvent {
  NamesBookShowedTheLastName(this.lastVisibleName);

  final CollectorName lastVisibleName;
}

/// Кдасс события изменения вида отображения имен
final class NamesBookChangedDisplayingNames extends NamesBookEvent {
  NamesBookChangedDisplayingNames(this.displayingName);

  /// Вид отображения списка имен
  final DisplayingName displayingName;
}

/// Класс события изменения фильтра пола
final class NamesBookChangedSexFilter extends NamesBookEvent {
  NamesBookChangedSexFilter(this.sex);

  /// Значение всех выбранные полов для фильтра
  final SexValue sex;
}

/// Класс события изменения фильтра групп имен
final class NamesBookChangedTagFilter extends NamesBookEvent {
  NamesBookChangedTagFilter(this.tag);

  /// Тег гркппв имени
  final Tag tag;
}

/// Класс события сохранения понравившегося имени
///
/// Принимает
/// - [name] - имя
final class SaveFavoriteName extends NamesBookEvent {
  SaveFavoriteName(this.name);

  final CollectorName name;
}

/// Класс события сохранения не понравившегося имени
///
/// Принимает
/// - [name] - имя
final class SaveNotFavoriteName extends NamesBookEvent {
  SaveNotFavoriteName(this.name);

  final CollectorName name;
}
