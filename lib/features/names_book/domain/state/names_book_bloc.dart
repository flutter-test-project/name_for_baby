import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/domain/entities/tag.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';
import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/event_bus/event_bus.dart';
import 'package:name_for_baby/features/event_bus/event_bus_events.dart';
import 'package:name_for_baby/features/names_book/domain/entities/names_book_filter.dart';
import 'package:name_for_baby/features/names_book/domain/i_old_names_book_repository.dart';
import 'package:name_for_baby/features/names_book/displaying_name.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/get_favorite_name_ids_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/get_not_favorite_name_ids_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/save_favorite_name_id_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/save_not_favorite_name_id_usecase.dart';

part 'names_book_event.dart';

part 'names_book_state.dart';

/// Блок по работе с карточками имен
class NamesBookBloc extends Bloc<NamesBookEvent, NamesBookState> {
  NamesBookBloc({
    required IOldNamesBookRepository oldNamesBlocRepository,
    required GetFavoriteNamesUsecase getFavoriteNameIdsUsecase,
    required GetNotFavoriteNamesUsecase getNotFavoriteNameIdsUsecase,
    required SaveFavoriteNameIdUsecase saveFavoriteNameIdUsecase,
    required SaveNotFavoriteNameIdUsecase saveNotFavoriteNameIdUsecase,
  })  : _oldNamesBlocRepository = oldNamesBlocRepository,
        _getFavoriteNameIdsUsecase = getFavoriteNameIdsUsecase,
        _getNotFavoriteNameIdsUsecase = getNotFavoriteNameIdsUsecase,
        _saveFavoriteNameIdUsecase = saveFavoriteNameIdUsecase,
        _saveNotFavoriteNameIdUsecase = saveNotFavoriteNameIdUsecase,
        super(NamesBookInitial()) {
    on<NamesBookStartedLoading>(_onNamesBookStarted);
    on<NamesBookShowedTheLastName>(_showedTheLastName);
    on<NamesBookChangedDisplayingNames>(_onChangeDisplaying);
    on<NamesBookChangedSexFilter>(_onChangeSexFilter);
    on<NamesBookChangedTagFilter>(_onChangeTagFilter);
    on<SaveFavoriteName>(_saveFavoriteName);
    on<SaveNotFavoriteName>(_saveNotFavoriteName);
  }

  final IOldNamesBookRepository _oldNamesBlocRepository;
  final GetFavoriteNamesUsecase _getFavoriteNameIdsUsecase;
  final GetNotFavoriteNamesUsecase _getNotFavoriteNameIdsUsecase;
  final SaveFavoriteNameIdUsecase _saveFavoriteNameIdUsecase;
  final SaveNotFavoriteNameIdUsecase _saveNotFavoriteNameIdUsecase;

  Future<void> _onNamesBookStarted(
    NamesBookStartedLoading event,
    Emitter<NamesBookState> emit,
  ) async {
    final oldState = state;
    try {
      emit(NamesBookInProgressLoading());

      final List<Tag> allTags = await _oldNamesBlocRepository.getTags();
      final List<String> favoriteNameIds = await _getFavoriteNameIdsUsecase.call(NoParams());
      final List<String> notFavoriteNameIds = await _getNotFavoriteNameIdsUsecase.call(NoParams());
      final NamesBookFilter filter = NamesBookFilter(
        allTags: allTags,
        favoriteNameIds: favoriteNameIds,
        notFavoriteNameIds: notFavoriteNameIds,
      );
      await _oldNamesBlocRepository.saveLastVisibleNameId('');
      final List<CollectorName> names = await _oldNamesBlocRepository.getNames(filter: filter);
      DisplayingName displayingName = DisplayingName.stack;
      if (oldState is NamesBookLoadSuccess) {
        displayingName = oldState.displayingName;
      }

      emit(
        NamesBookLoadSuccess(
          names: names,
          filter: filter,
          displayingName: displayingName,
        ),
      );
    } catch (error, stackTrace) {
      emit(NamesBookLoadFailure('$error'));
      addError(error, stackTrace);
    }
  }

  Future<void> _showedTheLastName(
    NamesBookShowedTheLastName event,
    Emitter<NamesBookState> emit,
  ) async {
    final oldState = state;
    try {
      if (oldState is NamesBookLoadSuccess) {
        emit(
          oldState.copyWith(
            inProgressLoading: true,
          ),
        );
        await _oldNamesBlocRepository.saveLastVisibleNameId(event.lastVisibleName.id);
        final List<CollectorName> names = await _oldNamesBlocRepository.getNames(filter: oldState.filter);
        emit(
          oldState.copyWith(
            names: oldState.names..addAll(names),
            inProgressLoading: false,
          ),
        );
      }
    } catch (error, stackTrace) {
      emit(NamesBookLoadFailure('$error'));
      addError(error, stackTrace);
    }
  }

  Future<void> _onChangeDisplaying(
    NamesBookChangedDisplayingNames event,
    Emitter<NamesBookState> emit,
  ) async {
    final oldState = state;
    try {
      if (oldState is NamesBookLoadSuccess) {
        emit(
          oldState.copyWith(
            displayingName: event.displayingName,
          ),
        );
      }
    } catch (error, stackTrace) {
      emit(NamesBookLoadFailure('$error'));
      addError(error, stackTrace);
    }
  }

  Future<void> _onChangeSexFilter(
    NamesBookChangedSexFilter event,
    Emitter<NamesBookState> emit,
  ) async {
    final oldState = state;
    try {
      if (oldState is NamesBookLoadSuccess) {
        List<SexValue> selectSexCollection = List.of(oldState.filter.selectSexCollection);

        if (selectSexCollection.contains(event.sex)) {
          // запрос на удаоение варианта пола
          selectSexCollection.remove(event.sex);
        } else {
          selectSexCollection.add(event.sex);
        }

        if (selectSexCollection.isNotEmpty) {
          emit(NamesBookInProgressLoading());
          await Future.delayed(const Duration(seconds: 2));
          NamesBookFilter filter = oldState.filter.copyWith(
            selectSexCollection: selectSexCollection,
          );
          emit(oldState.copyWith(filter: filter));
          await _oldNamesBlocRepository.saveLastVisibleNameId('');
          final List<CollectorName> names = await _oldNamesBlocRepository.getNames(filter: filter);

          emit(
            oldState.copyWith(
              names: names,
              filter: filter,
            ),
          );
        } else {
          emit(oldState);
        }
      }
    } catch (error, stackTrace) {
      emit(NamesBookLoadFailure('$error'));
      addError(error, stackTrace);
    }
  }

  Future<void> _onChangeTagFilter(
    NamesBookChangedTagFilter event,
    Emitter<NamesBookState> emit,
  ) async {
    final oldState = state;
    try {
      if (oldState is NamesBookLoadSuccess) {
        List<Tag> newSelectTags = List.of(oldState.filter.selectTags);
        if (newSelectTags.contains(event.tag)) {
          newSelectTags.remove(event.tag);
        } else {
          newSelectTags.add(event.tag);
        }

        NamesBookFilter filter = oldState.filter.copyWith(
          selectTags: newSelectTags,
        );

        emit(oldState.copyWith(filter: filter));

        final List<CollectorName> names = await _oldNamesBlocRepository.getNames(filter: filter);

        emit(oldState.copyWith(
          names: names,
          filter: filter,
        ));
      }
    } catch (error, stackTrace) {
      emit(NamesBookLoadFailure('$error'));
      addError(error, stackTrace);
    }
  }

  Future<void> _saveFavoriteName(
    SaveFavoriteName event,
    Emitter<NamesBookState> emit,
  ) async {
    final oldState = state;
    try {
      if (oldState is NamesBookLoadSuccess) {
        await _saveFavoriteNameIdUsecase.call(event.name.id);
        final List<String> favoriteNameIds = await _getFavoriteNameIdsUsecase.call(NoParams());
        NamesBookFilter filter = oldState.filter.copyWith(
          favoriteNameIds: favoriteNameIds,
        );
        var names = List<CollectorName>.of(oldState.names);
        names.remove(event.name);
        EventBus().addEvent(
          EditCountSelectedNameEvent(
            countFavoriteName: favoriteNameIds.length,
          ),
        );
        emit(
          oldState.copyWith(
            names: names,
            filter: filter,
          ),
        );
      }
    } catch (error, stackTrace) {
      emit(NamesBookLoadFailure('$error'));
      addError(error, stackTrace);
    }
  }

  Future<void> _saveNotFavoriteName(
    SaveNotFavoriteName event,
    Emitter<NamesBookState> emit,
  ) async {
    final oldState = state;
    try {
      if (oldState is NamesBookLoadSuccess) {
        await _saveNotFavoriteNameIdUsecase.call(event.name.id);
        final List<String> notFavoriteNameIds =
            await _getNotFavoriteNameIdsUsecase.call(NoParams());
        NamesBookFilter filter = oldState.filter.copyWith(
          notFavoriteNameIds: notFavoriteNameIds,
        );

        EventBus().addEvent(
          EditCountSelectedNameEvent(
            countNotFavoriteName: notFavoriteNameIds.length,
          ),
        );
        emit(oldState.copyWith(filter: filter));
      }
    } catch (error, stackTrace) {
      emit(NamesBookLoadFailure('$error'));
      addError(error, stackTrace);
    }
  }
}
