part of 'names_book_bloc.dart';

/// Класс состояний книги имен
@immutable
sealed class NamesBookState extends Equatable {
  @override
  List<Object> get props => [];
}

final class NamesBookInitial extends NamesBookState {}

final class NamesBookInProgressLoading extends NamesBookState {}

/// Основное состояние загруженной странички карточек
///
/// Принимает:
/// - [names] - Список карточек имен
/// - [filter] - Фильтр
/// - [displayingName] - Вид отображения списка имен
/// - [inProgressLoading] - Флаг показывающий дополнительную загрузку данных
final class NamesBookLoadSuccess extends NamesBookState {
  NamesBookLoadSuccess({
    required this.names,
    required this.filter,
    this.displayingName = DisplayingName.stack,
    this.inProgressLoading = false,
  });

  final List<CollectorName> names;

  final NamesBookFilter filter;

  final DisplayingName displayingName;

  final bool inProgressLoading;

  NamesBookLoadSuccess copyWith({
    List<CollectorName>? names,
    NamesBookFilter? filter,
    DisplayingName? displayingName,
    bool? inProgressLoading,
  }) {
    return NamesBookLoadSuccess(
      names: names ?? this.names,
      filter: filter ?? this.filter,
      displayingName: displayingName ?? this.displayingName,
      inProgressLoading: inProgressLoading ?? this.inProgressLoading,
    );
  }

  @override
  List<Object> get props => [
        names,
        filter,
        displayingName,
        inProgressLoading,
      ];
}

/// Состояние отображение ошибки приложения
final class NamesBookLoadFailure extends NamesBookState {
  NamesBookLoadFailure(this.text);

  final String text;
}
