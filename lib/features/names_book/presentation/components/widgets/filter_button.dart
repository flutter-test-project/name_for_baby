import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:name_for_baby/app/domain/entities/tag.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/app/presentation/icons/nfb_svg_icon.dart';
import 'package:name_for_baby/app/presentation/theme/nfb_text_styles.dart';
import 'package:name_for_baby/features/names_book/domain/state/names_book_bloc.dart';
import 'package:name_for_baby/generated/assets/assets.gen.dart';
import 'package:name_for_baby/l10n/delegate.dart';
import 'package:name_for_baby/ui_kit/buttons/ui_button_outline.dart';
import 'package:name_for_baby/ui_kit/buttons/ui_button_size.dart';
import 'package:name_for_baby/ui_kit/ui_colors.dart';
import 'package:name_for_baby/utils/additions.dart';

/// Кнопка фильтра имен
class FilterButton extends StatelessWidget {
  const FilterButton({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(context),
      child: NfbSvgIcon(
        asset: Assets.svg.icons.filter.path,
        size: 24,
      ),
    );
  }

  /// Метод обработчик нажатия на кнопку фильтра
  void onTap(
    BuildContext context,
  ) {
    final NamesBookBloc bloc = context.read<NamesBookBloc>();
    showMaterialModalBottomSheet(
      context: context,
      builder: (context) {
        return BlocProvider.value(
          value: bloc,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 6.0),
            child: _FilterButtonContent(),
          ),
        );
      },
    );
  }
}

/// Виджет основного наполнения фильтра
class _FilterButtonContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NamesBookBloc, NamesBookState>(
      builder: (context, state) {
        if (state is NamesBookLoadSuccess) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const IndentVertical(12.0),
              _FilterText(),
              const IndentVertical(12.0),
              _Text(l10n.sexColonText),
              _FilterSexPanel(
                allSexCollection: state.filter.allSexCollection,
                selectSexCollection: state.filter.selectSexCollection,
              ),
              _Text(l10n.namesGroupText.capitalize()),
              _FilterTagPanel(
                allTags: state.filter.allTags,
                selectTags: state.filter.selectTags,
              ),
            ],
          );
        }
        return const SizedBox();
      },
      buildWhen: (context, state) {
        if (state is NamesBookLoadSuccess) return true;
        return false;
      },
    );
  }
}

class _Text extends StatelessWidget {
  const _Text(this.text);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: NfbTextStyles.body2Bold,
    );
  }
}

class _FilterText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        l10n.filter,
        style: NfbTextStyles.body1Bold,
      ),
    );
  }
}

class _FilterSexPanel extends StatelessWidget {
  /// Показывает панель фильтра пола
  ///
  /// Принимает
  /// - [allSexCollection] - Коллекция всех возможных полов имен для фильтра
  /// - [selectSexCollection] - Коллекция выбранных имен для фильтра
  const _FilterSexPanel({
    required this.allSexCollection,
    required this.selectSexCollection,
  });

  final List<SexValue> allSexCollection;
  final List<SexValue> selectSexCollection;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: allSexCollection.map((sex) {
        final isSelect = selectSexCollection.contains(sex);
        return Padding(
          padding: const EdgeInsets.only(right: 4, top: 4),
          child: UiButtonOutline(
            foregroundColor: isSelect ? UiColors.accentGreenBlock : UiColors.secondaryLight,
            borderColor: isSelect ? UiColors.accentGreenBlock : UiColors.secondaryLight,
            buttonSize: UiButtonSize.s,
            onPressed: () => onChangedSexFilter(
              context: context,
              sex: sex,
            ),
            child: Text(sex.description),
          ),
        );
      }).toList(),
    );
  }

  /// Метод для изменения значения фильтра по полу
  void onChangedSexFilter({
    required BuildContext context,
    required SexValue sex,
  }) {
    context.read<NamesBookBloc>().add(NamesBookChangedSexFilter(sex));
  }
}

class _FilterTagPanel extends StatelessWidget {
  /// Панель фильтрации по тегам
  ///
  /// Принимает
  /// - [allTags] - Коллекция всех возможных тегов имен
  /// - [selectTags] - Коллекция выбранных тегов
  const _FilterTagPanel({
    required this.allTags,
    required this.selectTags,
  });

  final List<Tag> allTags;
  final List<Tag> selectTags;

  @override
  Widget build(BuildContext context) {
    return Wrap(
      direction: Axis.horizontal,
      children: allTags.map(
        (tag) {
          final isSelect = selectTags.contains(tag);
          return Padding(
            padding: const EdgeInsets.only(right: 4, top: 4),
            child: UiButtonOutline(
              foregroundColor: isSelect ? UiColors.accentGreenBlock : UiColors.secondaryLight,
              borderColor: isSelect ? UiColors.accentGreenBlock : UiColors.secondaryLight,
              buttonSize: UiButtonSize.s,
              onPressed: () => onChangedSexFilter(
                context: context,
                tag: tag,
              ),
              child: Text(tag.value),
            ),
          );
        },
      ).toList(),
    );
  }

  /// Метод для изменения значения фильтра по группе имен
  void onChangedSexFilter({
    required BuildContext context,
    required Tag tag,
  }) {
    context.read<NamesBookBloc>().add(NamesBookChangedTagFilter(tag));
  }
}
