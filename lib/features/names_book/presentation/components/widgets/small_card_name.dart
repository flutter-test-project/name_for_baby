import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/features/names_book/domain/state/names_book_bloc.dart';
import 'package:name_for_baby/routing/routes.dart';
import 'package:name_for_baby/ui_kit/buttons/ui_button_round_icon.dart';

/// Маленькая карточка товара
class SmallCardName extends StatelessWidget {
  const SmallCardName(this.collectorName, {super.key});

  final CollectorName collectorName;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _goCardName(context),
      child: Card(
        margin: const EdgeInsets.only(top: 12, bottom: 6),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 24),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(collectorName.fullName),
              UiButtonRoundIcon(
                icon: Icons.favorite,
                onPressed: () => _saveFavoriteName(
                  context: context,
                  name: collectorName,
                ),
              ),
              UiButtonRoundIcon(
                icon: Icons.clear,
                onPressed: () => _saveNotFavoriteName(
                  context: context,
                  name: collectorName,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Метод сохранения понравившегося имени
  void _saveFavoriteName({
    required BuildContext context,
    required CollectorName name,
  }) {
    context.read<NamesBookBloc>().add(SaveFavoriteName(name));
  }

  /// Метод сохранения не понравившегося имени
  void _saveNotFavoriteName({
    required BuildContext context,
    required CollectorName name,
  }) {
    context.read<NamesBookBloc>().add(SaveNotFavoriteName(name));
  }

  void _goCardName(BuildContext context) {
    CardNameRoute(
      nameId: collectorName.id,
      $extra: collectorName,
    ).push(context);
  }
}
