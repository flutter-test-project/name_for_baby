import 'package:flutter/material.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/presentation/theme/nfb_text_styles.dart';
import 'package:name_for_baby/routing/routes.dart';

/// Большая карточка имени
class BigCardName extends StatelessWidget {
  const BigCardName(this.name, {super.key});

  final CollectorName name;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _goCardName(context),
      child: Card(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              name.fullName,
              style: NfbTextStyles.normalTitle,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }

  void _goCardName(BuildContext context) {
    CardNameRoute(
      nameId: name.id,
      $extra: name,
    ).push(context);
  }
}
