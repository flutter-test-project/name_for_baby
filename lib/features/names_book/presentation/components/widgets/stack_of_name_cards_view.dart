import 'package:appinio_swiper/appinio_swiper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/features/names_book/domain/state/names_book_bloc.dart';
import 'package:name_for_baby/features/names_book/presentation/components/widgets/big_card_name.dart';

/// Предсавление карточек имен колодой
class StackOfNameCardsView extends StatefulWidget {
  const StackOfNameCardsView(this.names, {super.key});

  final List<CollectorName> names;

  @override
  State<StackOfNameCardsView> createState() => _StackOfNameCardsViewState();
}

class _StackOfNameCardsViewState extends State<StackOfNameCardsView> {
  final AppinioSwiperController controller = AppinioSwiperController();

  @override
  void initState() {
    Future.delayed(const Duration(seconds: 1)).then((_) {
      _shakeCard();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.75,
      child: Padding(
        padding: const EdgeInsets.only(
          left: 25,
          right: 25,
          top: 22,
          bottom: 40,
        ),
        child: LayoutBuilder(
          builder: (
            BuildContext context,
            BoxConstraints constraint,
          ) =>
              AppinioSwiper(
            controller: controller,
            invertAngleOnBottomDrag: true,
            backgroundCardCount: 2,
            backgroundCardScale: 0.95,
            backgroundCardOffset: Offset(0, constraint.maxHeight * 0.04),
            maxAngle: 30,
            swipeOptions: const SwipeOptions.symmetric(horizontal: true),
            onSwipeEnd: _swipeEnd,
            onEnd: () {},
            cardCount: widget.names.length,
            cardBuilder: (BuildContext context, int index) {
              return BigCardName(widget.names[index]);
            },
          ),
        ),
      ),
    );
  }

  /// Метод обработчик дейсвий над карточки
  ///
  /// принимает
  /// - [previousIndex] - предыдущий индекс
  /// - [targetIndex] - текущий индекс
  /// - [activity] - дейсвие
  void _swipeEnd(int previousIndex, int targetIndex, SwiperActivity activity) {
    if (activity is Swipe) {
      // Если осталось карточек меньше 1
      if (widget.names.length - (targetIndex + 1) == 1) {
        _loadedNewNames(widget.names.last);
      }

      if (activity.direction == AxisDirection.left) {
        _saveNotFavoriteName(widget.names[previousIndex]);
      } else if (activity.direction == AxisDirection.right) {
        _saveFavoriteName(widget.names[previousIndex]);
      }
    }
  }

  /// Метод сохранения понравившегося имени
  void _saveFavoriteName(CollectorName name) {
    context.read<NamesBookBloc>().add(SaveFavoriteName(name));
  }

  /// Метод сохранения не понравившегося имени
  void _saveNotFavoriteName(CollectorName name) {
    context.read<NamesBookBloc>().add(SaveNotFavoriteName(name));
  }

  /// Запуск события загркзки новых имен
  ///
  /// Принимает
  /// - [lastVisibleName] - последнее увиденное имя
  void _loadedNewNames(CollectorName lastVisibleName) {
    context.read<NamesBookBloc>().add(
          NamesBookShowedTheLastName(widget.names.last),
        );
  }

  Future<void> _shakeCard() async {
    const double distance = 30;
    await controller.animateTo(
      const Offset(-distance, 0),
      duration: const Duration(milliseconds: 200),
      curve: Curves.easeInOut,
    );
    await controller.animateTo(
      const Offset(distance, 0),
      duration: const Duration(milliseconds: 400),
      curve: Curves.easeInOut,
    );
    await controller.animateTo(
      const Offset(0, 0),
      duration: const Duration(milliseconds: 200),
      curve: Curves.easeInOut,
    );
  }
}
