import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/presentation/components/loading/loading_indicator.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/features/names_book/domain/state/names_book_bloc.dart';
import 'package:name_for_baby/features/names_book/presentation/components/widgets/small_card_name.dart';

/// Предствление карточек имен списком
class ListOfNameCardsView extends StatefulWidget {
  const ListOfNameCardsView(
    this.names, {
    super.key,
    required this.inProgressLoading,
  });

  final List<CollectorName> names;

  final bool inProgressLoading;

  @override
  State<ListOfNameCardsView> createState() => _ListOfNameCardsViewState();
}

class _ListOfNameCardsViewState extends State<ListOfNameCardsView> {
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    _scrollController.addListener(_loadMore);
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.removeListener(_loadMore);
    super.dispose();
  }

  void _loadMore() {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      context.read<NamesBookBloc>().add(
        NamesBookShowedTheLastName(widget.names.last),
          );
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      controller: _scrollController,
      itemCount: widget.inProgressLoading
          ? widget.names.length + 1
          : widget.names.length,
      itemBuilder: (context, index) {
        if (index < widget.names.length) {
          return SmallCardName(widget.names[index]);
        } else {
          return const Padding(
            padding: EdgeInsets.symmetric(horizontal: 12, vertical: 16),
            child: Center(child: LoadingIndicator()),
          );
        }
      },
    );
  }
}
