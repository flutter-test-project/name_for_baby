import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:name_for_baby/app/presentation/components/loading/loading_indicator.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/app/presentation/icons/nfb_svg_icon.dart';
import 'package:name_for_baby/app/presentation/theme/nfb_colors.dart';
import 'package:name_for_baby/features/names_book/displaying_name.dart';
import 'package:name_for_baby/features/names_book/domain/state/names_book_bloc.dart';
import 'package:name_for_baby/features/names_book/presentation/components/widgets/filter_button.dart';
import 'package:name_for_baby/features/names_book/presentation/components/widgets/list_of_name_cards_view.dart';
import 'package:name_for_baby/features/names_book/presentation/components/widgets/stack_of_name_cards_view.dart';
import 'package:name_for_baby/generated/assets/assets.gen.dart';
import 'package:name_for_baby/routing/routes.dart';
import 'package:name_for_baby/ui_kit/general/ui_empty_panel.dart';
import 'package:name_for_baby/ui_kit/general/ui_failure_panel.dart';

class NamesBookScreen extends StatelessWidget {
  const NamesBookScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider<NamesBookBloc>(
      create: (BuildContext context) => GetIt.instance()..add(NamesBookStartedLoading()),
      child: const NameBookPage(),
    );
  }
}

class NameBookPage extends StatefulWidget {
  const NameBookPage({super.key});

  @override
  State<NameBookPage> createState() => _NameBookPageState();
}

class _NameBookPageState extends State<NameBookPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(''),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: RefreshIndicator(
          onRefresh: () async {
            context.read<NamesBookBloc>().add(NamesBookStartedLoading());
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _SettingsPanel(),
              _CreateDataCollectionPanel(),
              Expanded(child: _NamesBookContent()),
            ],
          ),
        ),
      ),
    );
  }
}

class _NamesBookContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<NamesBookBloc, NamesBookState>(
      listener: (context, state) {},
      builder: (context, state) {
        if (state is NamesBookInProgressLoading) {
          return const LoadingIndicator();
        }
        if (state is NamesBookLoadSuccess && state.names.isEmpty) {
          return const UiEmptyPanel(
            text:
                'Имена для данной подборки закончились. Попробуйте изменить значения фильтра, что бы увидеть еще варианты.',
          );
        }
        if (state is NamesBookLoadSuccess && state.names.isNotEmpty) {
          return state.displayingName == DisplayingName.stack
              ? StackOfNameCardsView(state.names)
              : ListOfNameCardsView(
                  state.names,
                  inProgressLoading: state.inProgressLoading,
                );
        }
        if (state is NamesBookLoadFailure) {
          return UiFailurePanel(
            state.text,
            onReload: () => onErrorReload(context),
          );
        }
        return const SizedBox();
      },
    );
  }

  /// метод запускающий событие перезвгрузки страницы при ошибки
  void onErrorReload(BuildContext context) {
    context.read<NamesBookBloc>().add(
          NamesBookStartedLoading(),
        );
  }
}

class _CreateDataCollectionPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        const DataCollectionRoute().go(context);
      },
      child: const Text('Открыть форму загрузки'),
    );
  }
}

/// Виджет панели настроек
class _SettingsPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        _DisplayingListNameSettingsPanel(),
        const FilterButton(),
      ],
    );
  }
}

/// Виджет переключатель отобрадения списка имен
class _DisplayingListNameSettingsPanel extends StatefulWidget {
  @override
  State<_DisplayingListNameSettingsPanel> createState() => _DisplayingListNameSettingsPanelState();
}

class _DisplayingListNameSettingsPanelState extends State<_DisplayingListNameSettingsPanel> {
  DisplayingName displayingName = DisplayingName.stack;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NamesBookBloc, NamesBookState>(builder: (context, state) {
      if (state is NamesBookLoadSuccess) displayingName = state.displayingName;
      return Row(
        children: [
          GestureDetector(
            onTap: state is! NamesBookLoadSuccess
                ? null
                : () => _onChangeDisplayingName(
                      context: context,
                      displayingName: DisplayingName.list,
                    ),
            child: NfbSvgIcon(
              asset: Assets.svg.icons.rowVertical.path,
              color: displayingName == DisplayingName.list
                  ? NfbColors.activeColor
                  : NfbColors.globalBlack,
              size: 24,
            ),
          ),
          const IndentHorizontal(4.0),
          GestureDetector(
            onTap: state is! NamesBookLoadSuccess
                ? null
                : () => _onChangeDisplayingName(
                      context: context,
                      displayingName: DisplayingName.stack,
                    ),
            child: NfbSvgIcon(
              asset: Assets.svg.icons.sliderVertical.path,
              color: displayingName == DisplayingName.stack
                  ? NfbColors.activeColor
                  : NfbColors.globalBlack,
              size: 24,
            ),
          ),
        ],
      );
    });
  }

  void _onChangeDisplayingName({
    required BuildContext context,
    required DisplayingName displayingName,
  }) {
    context.read<NamesBookBloc>().add(
          NamesBookChangedDisplayingNames(displayingName),
        );
  }
}
