import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';

class UserSecureDataRepository implements IUserSecureDataRepository {
  UserSecureDataRepository({
    required FlutterSecureStorage storage,
  }) : _storage = storage;

  final FlutterSecureStorage _storage;

  @override
  Future<bool> get isUserAuthentication async {
    final login = await getLogin;
    final password = await getPassword;
    if (login.isEmpty || password.isEmpty) return false;
    return true;
  }

  @override
  Future<String> get getLogin async {
    return await _storage.read(key: DataSecureStorageKey.login) ?? '';
  }

  @override
  Future<String> get getPassword async {
    return await _storage.read(key: DataSecureStorageKey.password) ?? '';
  }

  @override
  Future<void> saveLogin(String value) async {
    await _storage.write(key: DataSecureStorageKey.login, value: value);
  }

  @override
  Future<void> savePassword(String value) async {
    await _storage.write(key: DataSecureStorageKey.password, value: value);
  }

  @override
  Future<void> cleanData() async {
    await _storage.delete(key: DataSecureStorageKey.login);
    await _storage.delete(key: DataSecureStorageKey.password);
  }
}

/// Хранилище ключей доступов к данным
class DataSecureStorageKey {
  static const String login = 'login';
  static const String password = 'password';
}
