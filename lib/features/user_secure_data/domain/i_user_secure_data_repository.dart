/// Абстрактный репозиторий для получения/созраненения секретных данных пользователя
abstract interface class IUserSecureDataRepository {
  /// Метод проверки авторизации пользователя
  Future<bool> get isUserAuthentication;

  /// Метод получения логина
  Future<String> get getLogin;

  /// Метод получения пароля
  Future<String> get getPassword;

  /// Метод сохраненения логина
  Future<void> saveLogin(String value);

  /// Метод сохраненения пароля
  Future<void> savePassword(String value);

  /// Метод очистки данных пользователя
  Future<void> cleanData();
}
