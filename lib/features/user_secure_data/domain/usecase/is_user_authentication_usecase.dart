import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';

class IsUserAuthenticationUsecase extends BaseUseCase<Future<bool>, NoParams> {
  IsUserAuthenticationUsecase({
    required IUserSecureDataRepository userSecureDataRepository,
  }) : _userSecureDataRepository = userSecureDataRepository;

  final IUserSecureDataRepository _userSecureDataRepository;

  @override
  Future<bool> call(NoParams params) async {
    return _userSecureDataRepository.isUserAuthentication;
  }
}
