import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';

class SaveLoginAndPasswordUsecase extends BaseUseCase<Future<void>, UserDataAuth> {
  SaveLoginAndPasswordUsecase({
    required IUserSecureDataRepository userSecureDataRepository,
  }) : _userSecureDataRepository = userSecureDataRepository;

  final IUserSecureDataRepository _userSecureDataRepository;

  @override
  Future<void> call(UserDataAuth params) async {
    await _userSecureDataRepository.saveLogin(params.login);
    await _userSecureDataRepository.savePassword(params.password);
  }
}

/// Класс описывающий авторизационнве данные пользователя
class UserDataAuth {
  UserDataAuth({
    required this.login,
    required this.password,
  });

  final String login;
  final String password;
}
