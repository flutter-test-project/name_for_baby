import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';

class CleanUserSecureDataUsecase extends BaseUseCase<Future<void>, NoParams> {
  CleanUserSecureDataUsecase({
    required IUserSecureDataRepository userSecureDataRepository,
  }) : _userSecureDataRepository = userSecureDataRepository;

  final IUserSecureDataRepository _userSecureDataRepository;

  @override
  Future<void> call(NoParams params) async {
    await _userSecureDataRepository.cleanData();
  }
}
