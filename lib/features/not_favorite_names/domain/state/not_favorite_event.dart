part of 'not_favorite_bloc.dart';

/// Класс событий Не понравивившмхся имен
@immutable
sealed class NotFavoriteEvent {}

/// Событие начала загрузки данных
final class NotFavoriteNamesStartedLoading extends NotFavoriteEvent {}

/// Удадение имени из списка Не понравившихся имен
final class NotFavoriteNamesDeleted extends NotFavoriteEvent {
  NotFavoriteNamesDeleted(this.nameId);

  /// ид имени
  final String nameId;
}
