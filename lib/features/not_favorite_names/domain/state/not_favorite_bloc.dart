import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/event_bus/event_bus.dart';
import 'package:name_for_baby/features/event_bus/event_bus_events.dart';
import 'package:name_for_baby/features/names_book/domain/usecase/get_names_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/delete_not_favorite_name_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/get_not_favorite_name_ids_usecase.dart';

part 'not_favorite_event.dart';

part 'not_favorite_state.dart';

class NotFavoriteNamesBloc extends Bloc<NotFavoriteEvent, NotFavoriteNameState> {
  NotFavoriteNamesBloc({
    required GetNotFavoriteNamesUsecase getNotFavoriteNamesUsecase,
    required GetNamesUsecase getNamesUsecase,
    required DeleteNotFavoriteNameUsecase deleteNotFavoriteNameUsecase,
  })  : _getNotFavoriteNamesUsecase = getNotFavoriteNamesUsecase,
        _getNamesUsecase = getNamesUsecase,
        _deleteNotFavoriteNameUsecase = deleteNotFavoriteNameUsecase,
        super(NotFavoriteNameInitial()) {
    on<NotFavoriteNamesStartedLoading>(_onLoadStarted);
    on<NotFavoriteNamesDeleted>(_onDeletedNorFavoriteName);
  }

  final GetNotFavoriteNamesUsecase _getNotFavoriteNamesUsecase;
  final GetNamesUsecase _getNamesUsecase;
  final DeleteNotFavoriteNameUsecase _deleteNotFavoriteNameUsecase;

  Future<void> _onLoadStarted(
    NotFavoriteNamesStartedLoading event,
    Emitter<NotFavoriteNameState> emit,
  ) async {
    try {
      emit(NotFavoriteNamesLoading());
      final List<String> favoriteNameIds = await _getNotFavoriteNamesUsecase.call(NoParams());
      final List<CollectorName> notFavoriteNames = await _getNamesUsecase.call(favoriteNameIds);
      emit(NotFavoriteNamesLoadSuccess(
        notFavoriteNames: notFavoriteNames,
      ));
    } catch (error, stackTrace) {
      emit(NotFavoriteNamesFailure('$error'));
      addError(error, stackTrace);
    }
  }

  Future<void> _onDeletedNorFavoriteName(
    NotFavoriteNamesDeleted event,
    Emitter<NotFavoriteNameState> emit,
  ) async {
    final oldState = state;
    try {
      if (oldState is NotFavoriteNamesLoadSuccess) {
        await _deleteNotFavoriteNameUsecase.call(event.nameId);
        final List<String> notFavoriteNameIds = await _getNotFavoriteNamesUsecase.call(NoParams());
        final List<CollectorName> notFavoriteNames = await _getNamesUsecase.call(notFavoriteNameIds);
        EventBus().addEvent(
          EditCountSelectedNameEvent(
            countNotFavoriteName: notFavoriteNames.length,
          ),
        );
        emit(oldState.copyWith(favoriteNames: notFavoriteNames));
      }
    } catch (error, stackTrace) {
      emit(NotFavoriteNamesFailure('$error'));
      addError(error, stackTrace);
    }
  }
}
