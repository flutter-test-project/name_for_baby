part of 'not_favorite_bloc.dart';

/// Класс состояний для работы с данными Не понравившихся имен
@immutable
sealed class NotFavoriteNameState extends Equatable {
  @override
  List<Object> get props => [];
}

/// Класс начального состояния
final class NotFavoriteNameInitial extends NotFavoriteNameState {}

/// Класс состояния загрузки данных
final class NotFavoriteNamesLoading extends NotFavoriteNameState {}

/// Класс состояния полученнных данных о Не понравившихся именах
final class NotFavoriteNamesLoadSuccess extends NotFavoriteNameState {
  NotFavoriteNamesLoadSuccess({
    required this.notFavoriteNames,
  });

  /// Список Не понравившихся имен
  final List<CollectorName> notFavoriteNames;

  NotFavoriteNamesLoadSuccess copyWith({
    List<CollectorName>? favoriteNames,
  }) {
    return NotFavoriteNamesLoadSuccess(
      notFavoriteNames: favoriteNames ?? notFavoriteNames,
    );
  }

  @override
  List<Object> get props => [
        notFavoriteNames,
      ];
}

/// Класс вывода критической ошибки
final class NotFavoriteNamesFailure extends NotFavoriteNameState {
  NotFavoriteNamesFailure(this.text);

  final String text;
}

/// Класс состония вывода сообщения ошибки
final class NotFavoriteNamesErrorMessage extends NotFavoriteNameState {
  NotFavoriteNamesErrorMessage(this.text);

  final String text;
}
