import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/presentation/components/cards/name_card.dart';
import 'package:name_for_baby/app/presentation/components/loading/loading_indicator.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/features/not_favorite_names/domain/state/not_favorite_bloc.dart';
import 'package:name_for_baby/l10n/delegate.dart';
import 'package:name_for_baby/routing/routes.dart';
import 'package:name_for_baby/ui_kit/general/ui_empty_panel.dart';
import 'package:name_for_baby/ui_kit/general/ui_failure_panel.dart';

/// Класс описываюзий страничку Не понравившихся имен пользователя
class NotFavoriteNamesScreen extends StatelessWidget {
  const NotFavoriteNamesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<NotFavoriteNamesBloc>(
      create: (BuildContext context) => GetIt.instance()
        ..add(
          NotFavoriteNamesStartedLoading(),
        ),
      child: _NotFavoriteNamesPage(),
    );
  }
}

class _NotFavoriteNamesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<NotFavoriteNamesBloc, NotFavoriteNameState>(
      listener: (context, state) {
        if (state is NotFavoriteNamesErrorMessage) {
          //todo
        }
      },
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: Text(l10n.namesYouDontLike),
          ),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: RefreshIndicator(
              child: switch (state) {
                NotFavoriteNameInitial() => const SizedBox(),
                NotFavoriteNamesLoading() => const LoadingIndicator(),
                NotFavoriteNamesLoadSuccess() => Column(
                    children: [
                      const IndentHorizontal(6),
                      Text('${state.notFavoriteNames.length}'),
                      const IndentHorizontal(6),
                      Expanded(child: _FavoriteNamesContent()),
                    ],
                  ),
                NotFavoriteNamesFailure() => UiFailurePanel(
                    state.text,
                    onReload: () => onRefresh(context),
                  ),
                NotFavoriteNamesErrorMessage() => const SizedBox(),
              },
              onRefresh: () async => onRefresh(context),
            ),
          ),
        );
      },
      buildWhen: (context, state) {
        if (state is NotFavoriteNamesErrorMessage) return false;
        return true;
      },
    );
  }

  /// метод запускающий событие перезвгрузки страницы
  void onRefresh(BuildContext context) {
    context.read<NotFavoriteNamesBloc>().add(NotFavoriteNamesStartedLoading());
  }
}

class _FavoriteNamesContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NotFavoriteNamesBloc, NotFavoriteNameState>(builder: (context, state) {
      if (state is NotFavoriteNamesLoadSuccess) {
        if (state.notFavoriteNames.isEmpty) {
          return UiEmptyPanel(
            text: l10n.noDataAvailable,
          );
        }
        return ListView(
          children: state.notFavoriteNames
              .map(
                (name) => NameCard(
                  fullName: name.fullName,
                  onInfoPressed: () => _goToCardName(
                    context: context,
                    name: name,
                  ),
                  onDeletedPressed: () {
                    context.read<NotFavoriteNamesBloc>().add((NotFavoriteNamesDeleted(name.id)));
                  },
                ),
              )
              .toList(),
        );
      }

      return const SizedBox();
    });
  }

  /// Перейти к просмотру карточки имени
  void _goToCardName({
    required BuildContext context,
    required CollectorName name,
  }) {
    CardNameRoute(
      nameId: name.id,
      $extra: name,
    ).push(context);
  }
}
