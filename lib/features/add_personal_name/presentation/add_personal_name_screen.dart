import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';
import 'package:name_for_baby/app/presentation/components/dialogs/info_dialog.dart';
import 'package:name_for_baby/app/presentation/components/loading/loading_indicator.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/features/add_personal_name/domain/state/add_personal_name_bloc.dart';
import 'package:name_for_baby/features/add_personal_name/presentation/components/sex_dropdown_button.dart';
import 'package:name_for_baby/l10n/delegate.dart';
import 'package:name_for_baby/ui_kit/buttons/ui_button_outline.dart';
import 'package:name_for_baby/ui_kit/fields/ui_text_field.dart';
import 'package:name_for_baby/ui_kit/general/ui_failure_panel.dart';

/// Экран добавления персонального имени пользователя
class AddPersonalNameScreen extends StatelessWidget {
  const AddPersonalNameScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AddPersonalNameBloc>(
      create: (BuildContext context) => GetIt.instance()..add(AddPersonalNameStartedLoading()),
      child: Scaffold(
        appBar: AppBar(
          title: Text(l10n.addName),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 12,
            vertical: 16,
          ),
          child: _AddPersonalName(),
        ),
      ),
    );
  }
}

class _AddPersonalName extends StatefulWidget {
  @override
  State<_AddPersonalName> createState() => _AddPersonalNameState();
}

/// Класс описывающий вид экрана в зависисмости от состояния
class _AddPersonalNameState extends State<_AddPersonalName> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AddPersonalNameBloc, AddPersonalNameState>(
      listener: (context, state) {
        if (state is AddPersonalNameSuccess) {
          _showSuccessAddPersonalName();
        }
      },
      builder: (context, state) {
        return RefreshIndicator(
          onRefresh: _onRefresh,
          child: switch (state) {
            AddPersonalNameInitial() => const SizedBox(),
            AddPersonalNameLoading() => const LoadingIndicator(),
            AddPersonalNameLoadSuccess() => _AddPersonalNameForm(),
            AddPersonalNameFailure() => UiFailurePanel(
                state.text,
                onReload: () => _onRefresh(),
              ),
            AddPersonalNameErrorMessage() => const SizedBox(),
            AddPersonalNameSuccess() => const SizedBox(),
          },
        );
      },
      buildWhen: (context, state) {
        if (state is AddPersonalNameSuccess || state is AddPersonalNameErrorMessage) {
          return false;
        }
        return true;
      },
    );
  }

  /// Метод перезагрузки экрана
  Future<void> _onRefresh() async {
    context.read<AddPersonalNameBloc>().add(AddPersonalNameStartedLoading());
  }

  /// Метод вывода окна сообщения об успешном добавлении имени
  void _showSuccessAddPersonalName() {
    showInfoDialog(
      context: context,
      description: l10n.addSuccessPersonalName,
      onPressed: () => Navigator.of(context).pop(),
    );
  }
}

/// Класс описываюзий форму добавления персонального имени пользователя
class _AddPersonalNameForm extends StatefulWidget {
  @override
  State<_AddPersonalNameForm> createState() => _AddPersonalNameFormState();
}

/// Класс описываюзий форму добавления персонального имени пользователя
class _AddPersonalNameFormState extends State<_AddPersonalNameForm> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _fullNameController = TextEditingController();
  final TextEditingController _valueNameController = TextEditingController();
  SexValue _sexValue = SexValue.boy;

  @override
  void dispose() {
    _fullNameController.dispose();
    _valueNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          const IndentVertical(12),
          UiTextFormField(
            controller: _fullNameController,
            validator: _fullNameValidator,
            labelText: l10n.fullNameText,
            keyboardType: TextInputType.text,
          ),
          const IndentVertical(12),
          UiTextFormField(
            controller: _valueNameController,
            labelText: l10n.valueNameText,
            keyboardType: TextInputType.text,
          ),
          const IndentVertical(12),
          // Выпадающий список
          SexDropdownButton(
            sexValue: _sexValue,
            onChanged: (sexValue) {
              if (sexValue != null) setState(() => _sexValue = sexValue);
            },
          ),
          const IndentVertical(12),
          UiButtonOutline(
            onPressed: () => _onPressedSave(context),
            child: Text(l10n.save),
          ),
        ],
      ),
    );
  }

  /// Метод вадидации поля полного имени
  String? _fullNameValidator(String? value) {
    if (value == null || value.isEmpty) {
      return l10n.validMessageEmpty;
    }
    return null;
  }

  /// Метод обрабочик нажатия на кнопку сохранения имени
  void _onPressedSave(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      final fullName = _fullNameController.text.trim();
      final valueName = _valueNameController.text.trim();

      context.read<AddPersonalNameBloc>().add(
            AddPersonalNameData(
              fullName: fullName,
              valueName: valueName,
              sexValue: _sexValue,
            ),
          );
    }
  }
}
