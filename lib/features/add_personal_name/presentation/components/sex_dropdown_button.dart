import 'package:flutter/material.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';

/// Виджет для переключения значения пола
class SexDropdownButton extends StatelessWidget {
  const SexDropdownButton({
    super.key,
    required this.sexValue,
    this.onChanged,
  });

  final SexValue sexValue;
  final void Function(SexValue?)? onChanged;

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<SexValue>(
      value: sexValue,
      items: [
        DropdownMenuItem(
          value: SexValue.boy,
          child: Text(SexValue.boy.description),
        ),
        DropdownMenuItem(
          value: SexValue.girl,
          child: Text(SexValue.girl.description),
        ),
      ],
      onChanged: onChanged,
    );
  }
}
