part of 'add_personal_name_bloc.dart';

/// Класс событий для добавления персонального имени пользователя
@immutable
sealed class AddPersonalNameEvent {}

/// Событие начала загрузки данных
final class AddPersonalNameStartedLoading extends AddPersonalNameEvent {}

/// Событие добавления данных персонального имени
final class AddPersonalNameData extends AddPersonalNameEvent {
  AddPersonalNameData({
    required this.fullName,
    required this.valueName,
    required this.sexValue,
  });

  /// Полное имя
  final String fullName;

  /// Значение имени
  final String valueName;

  /// Пол
  final SexValue sexValue;
}
