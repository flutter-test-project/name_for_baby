import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';
import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/app/domain/entities/invented_name.dart';
import 'package:name_for_baby/features/event_bus/event_bus.dart';
import 'package:name_for_baby/features/event_bus/event_bus_events.dart';
import 'package:name_for_baby/features/personal_list_names/domain/usecase/add_personal_name_usecase.dart';
import 'package:name_for_baby/features/personal_list_names/domain/usecase/get_personal_names_usecase.dart';
import 'package:name_for_baby/features/user_secure_data/domain/usecase/is_user_authentication_usecase.dart';
import 'package:name_for_baby/l10n/delegate.dart';

part 'add_personal_name_event.dart';

part 'add_personal_name_state.dart';

/// Блок для работы по добавлению персонального имени пользователя
class AddPersonalNameBloc extends Bloc<AddPersonalNameEvent, AddPersonalNameState> {
  AddPersonalNameBloc({
    required IsUserAuthenticationUsecase isUserAuthenticationUsecase,
    required AddPersonalNameUsecase addPersonalNameUsecase,
    required GetPersonalNamesUsecase getPersonalNamesUsecase,
  })  : _isUserAuthenticationUsecase = isUserAuthenticationUsecase,
        _addPersonalNameUsecase = addPersonalNameUsecase,
        _getPersonalNamesUsecase = getPersonalNamesUsecase,
        super(AddPersonalNameInitial()) {
    on<AddPersonalNameStartedLoading>(_onLoadStarted);
    on<AddPersonalNameData>(_addPersonalName);
  }

  final IsUserAuthenticationUsecase _isUserAuthenticationUsecase;
  final AddPersonalNameUsecase _addPersonalNameUsecase;
  final GetPersonalNamesUsecase _getPersonalNamesUsecase;

  Future<void> _onLoadStarted(
    AddPersonalNameStartedLoading event,
    Emitter<AddPersonalNameState> emit,
  ) async {
    try {
      emit(AddPersonalNameLoading());
      final bool isUserBeenIdentified = await _isUserAuthenticationUsecase.call(NoParams());
      if (!isUserBeenIdentified) {
        // Эта странича только для зареганых пользователей
        emit(AddPersonalNameFailure(l10n.pageIsOnlyUserAuth));
      }
      emit(AddPersonalNameLoadSuccess());
    } catch (error, stackTrace) {
      emit(AddPersonalNameFailure('$error'));
      addError(error, stackTrace);
    }
  }

  Future<void> _addPersonalName(
    AddPersonalNameData event,
    Emitter<AddPersonalNameState> emit,
  ) async {
    try {
      emit(AddPersonalNameLoading());
      List<InventedName> inventedNames = await _getPersonalNamesUsecase.call(NoParams());
      inventedNames.removeWhere((name) => name.sex != event.sexValue);
      await _addPersonalNameUsecase.call(
        InventedName(
          id: 'ru_${event.sexValue.name}_${inventedNames.length + 1}',
          fullName: event.fullName,
          valueName: event.valueName,
          sex: event.sexValue,
        ),
      );
      List<InventedName> newPersonalNames = await _getPersonalNamesUsecase.call(NoParams());
      EventBus().addEvent(
        EditCountSelectedNameEvent(
          countPersonalName: newPersonalNames.length,
        ),
      );
      emit(AddPersonalNameSuccess());
      emit(AddPersonalNameLoadSuccess());
    } catch (error, stackTrace) {
      emit(AddPersonalNameFailure('$error'));
      addError(error, stackTrace);
    }
  }
}
