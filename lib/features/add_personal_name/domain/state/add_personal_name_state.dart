part of 'add_personal_name_bloc.dart';

/// Класс состояний для работы с добавлением имени в личную коллекцию имен пользователя
@immutable
sealed class AddPersonalNameState extends Equatable {
  @override
  List<Object> get props => [];
}

/// Класс начального состояния
final class AddPersonalNameInitial extends AddPersonalNameState {}

/// Класс состояния загрузки данных
final class AddPersonalNameLoading extends AddPersonalNameState {}

/// Класс состояния полученнных данных
final class AddPersonalNameLoadSuccess extends AddPersonalNameState {}

/// Класс вывода критической ошибки
final class AddPersonalNameFailure extends AddPersonalNameState {
  AddPersonalNameFailure(this.text);

  final String text;
}

/// Класс состония вывода сообщения ошибки
final class AddPersonalNameErrorMessage extends AddPersonalNameState {
  AddPersonalNameErrorMessage(this.text);

  final String text;
}

/// Класс состония успешного добавления имени
final class AddPersonalNameSuccess extends AddPersonalNameState {}
