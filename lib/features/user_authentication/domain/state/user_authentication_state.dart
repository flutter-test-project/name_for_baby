part of 'user_authentication_bloc.dart';

/// Класс состояний аутентификации
@immutable
sealed class UserAuthenticationState extends Equatable {
  const UserAuthenticationState();

  @override
  List<Object> get props => [];
}

/// Класс инициализации аутентификации
final class UserAuthenticationInitial extends UserAuthenticationState {}

/// Класс для отображения процесса аутентификации
final class UserAuthenticationInProgress extends UserAuthenticationState {}

/// Класс для отобрадения успешного завешения процесса аутентификации (входа в приложение)
final class UserAuthenticationComplete extends UserAuthenticationState {
  const UserAuthenticationComplete();

  /// Был ли пользователь идентифиуирован
  //final bool isUserBeenIdentified;
}

/// Класс для вывода ошибок возникших в процессе аутентификации
final class UserAuthenticationProcessError extends UserAuthenticationState {
  const UserAuthenticationProcessError(
    this.errorText,
  );

  /// Текст ошибки
  final String errorText;
}
