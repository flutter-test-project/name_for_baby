part of 'user_authentication_bloc.dart';

/// Класс событий для аутентификации
@immutable
sealed class UserAuthenticationEvent extends Equatable {
  const UserAuthenticationEvent();

  @override
  List<Object?> get props => [];
}

/// Событие проверки аутентификации пользователя
final class CheckAuthentication extends UserAuthenticationEvent {}

/// Событие авторизации пользователя
final class UserAuthorizationEvent extends UserAuthenticationEvent {
  const UserAuthorizationEvent({
    required this.email,
    required this.password,
  });

  final String email;
  final String password;
}
