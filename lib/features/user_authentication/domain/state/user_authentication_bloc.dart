import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/update_preference_by_name_usecase.dart';
import 'package:name_for_baby/features/user_secure_data/domain/usecase/is_user_authentication_usecase.dart';
import 'package:name_for_baby/features/user_secure_data/domain/usecase/save_login_and_password_usecase.dart';

part 'user_authentication_event.dart';

part 'user_authentication_state.dart';

class UserAuthenticationBloc extends Bloc<UserAuthenticationEvent, UserAuthenticationState> {
  UserAuthenticationBloc({
    required IsUserAuthenticationUsecase isUserAuthenticationUsecase,
    required SaveLoginAndPasswordUsecase saveLoginAndPasswordUsecasey,
    required UpdatePreferenceByNameUsecase updateUserDataCollectionUsecase,
  })  : _isUserAuthenticationUsecase = isUserAuthenticationUsecase,
        _saveLoginAndPasswordUsecase = saveLoginAndPasswordUsecasey,
        _updateUserDataCollectionUsecase = updateUserDataCollectionUsecase,
        super(UserAuthenticationInitial()) {
    on<CheckAuthentication>(_onCheckAuthentication);
    on<UserAuthorizationEvent>(_onUserAuthorization);
  }

  final IsUserAuthenticationUsecase _isUserAuthenticationUsecase;
  final SaveLoginAndPasswordUsecase _saveLoginAndPasswordUsecase;

  final UpdatePreferenceByNameUsecase _updateUserDataCollectionUsecase;

  Future<void> _onCheckAuthentication(
    CheckAuthentication event,
    Emitter<UserAuthenticationState> emit,
  ) async {
    try {
      emit(UserAuthenticationInProgress());
      final bool isUserBeenIdentified = await _isUserAuthenticationUsecase.call(NoParams());
      //await Future.delayed(Duration(seconds: 5));
      emit(const UserAuthenticationComplete());
    } catch (error, stackTrace) {
      emit(UserAuthenticationProcessError('$error'));
      addError(error, stackTrace);
    }
  }

  Future<void> _onUserAuthorization(
    UserAuthorizationEvent event,
    Emitter<UserAuthenticationState> emit,
  ) async {
    try {
      emit(UserAuthenticationInProgress());
      final UserCredential credential = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: event.email,
        password: event.password,
      );
      final User? user = credential.user;
      if (user == null) {
        emit(
          const UserAuthenticationProcessError(
              'Пользователь с данным логином и паролем не найден.'),
        );
        return;
      } else if (user.email == null || user.uid.isEmpty) {
        emit(
          const UserAuthenticationProcessError(
              'Ошибка в прочтении данных пользователя. Попробуйте повторить попытку авторизации позже.'),
        );
        return;
      }

      await _saveLoginAndPasswordUsecase.call(
        UserDataAuth(
          login: event.email,
          password: event.password,
        ),
      );
      final bool isUserBeenIdentified = await _isUserAuthenticationUsecase.call(NoParams());

      if (!isUserBeenIdentified) {
        emit(const UserAuthenticationProcessError('Даннные пользователя не были сохранены.'));
        return;
      }

      await _updateUserDataCollectionUsecase.call(NoParams());

      emit(const UserAuthenticationComplete());
    } catch (error, stackTrace) {
      emit(UserAuthenticationProcessError('$error'));
      addError(error, stackTrace);
    }
  }
}
