/// Класс для описания интерфейса аутентификации
abstract interface class IUserAuthenticationRepository {
  /// Метод для проверки аутентификации пользователя
  Future<bool> checkUserAuthentication();
}
