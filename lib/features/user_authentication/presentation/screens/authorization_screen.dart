import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:name_for_baby/app/presentation/components/dialogs/error_dialog.dart';
import 'package:name_for_baby/app/presentation/components/loading/loading_indicator.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/app/utils/general_utils.dart';
import 'package:name_for_baby/features/event_bus/event_bus.dart';
import 'package:name_for_baby/features/event_bus/event_bus_events.dart';
import 'package:name_for_baby/features/user_authentication/domain/state/user_authentication_bloc.dart';
import 'package:name_for_baby/l10n/delegate.dart';
import 'package:name_for_baby/routing/routes.dart';
import 'package:name_for_baby/ui_kit/buttons/ui_button_outline.dart';
import 'package:name_for_baby/ui_kit/fields/ui_text_field.dart';

/// Экран авторизации пользователя
class AuthorizationScreen extends StatelessWidget {
  const AuthorizationScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UserAuthenticationBloc>(
      create: (BuildContext context) => GetIt.instance(),
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 16),
          child: _AuthenticationForm(),
        ),
      ),
    );
  }
}

class _AuthenticationForm extends StatefulWidget {
  @override
  State<_AuthenticationForm> createState() => _AuthenticationFormState();
}

class _AuthenticationFormState extends State<_AuthenticationForm> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<UserAuthenticationBloc, UserAuthenticationState>(
      listener: (context, state) {
        if (state is UserAuthenticationProcessError) {
          _showErrorDialog(state.errorText);
        }
        if (state is UserAuthenticationComplete) {
          EventBus().addEvent(
            const UserAuthEvent(),
          );
          _goToProfile();
        }
      },
      builder: (context, state) {
        return Stack(
          children: [
            if (state is UserAuthenticationInProgress) const Center(child: LoadingIndicator()),
            Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(l10n.authorization),
                  const IndentVertical(12),
                  UiTextFormField(
                    controller: _emailController,
                    validator: _emailValidator,
                    labelText: l10n.login,
                    keyboardType: TextInputType.emailAddress,
                  ),
                  const IndentVertical(12),
                  UiTextFormField(
                    controller: _passwordController,
                    validator: _passwordValidator,
                    labelText: l10n.password,
                    obscureText: true,
                  ),
                  const IndentVertical(12),
                  UiButtonOutline(
                    onPressed: state is UserAuthenticationInProgress
                        ? null
                        : () => _onPressedAuthorization(context),
                    child: Text(l10n.entrance),
                  ),
                  const IndentVertical(12),
                  GestureDetector(
                    child: Text(l10n.createAccount),
                    onTap: () => const RegistrationRoute().go(context),
                  ),
                ],
              ),
            ),
          ],
        );
      },
      buildWhen: (context, state) {
        return true;
      },
    );
  }

  /// Перейти на страницу профиля
  void _goToProfile() {
    const ProfileRoute().go(context);
    //context.pop(true);
  }

  /// Метод вывода окна сообщения об ошибке
  void _showErrorDialog(String errorText) {
    showErrorDialog(
      context: context,
      description: errorText,
    );
  }

  /// Метод вадидации поля ввода логина/email
  String? _emailValidator(String? value) {
    if (value == null || value.isEmpty) {
      return l10n.validMessageEmpty;
    } else if (value.isValidEmail) {
      return l10n.validMessageEmailIncorrect;
    }
    return null;
  }

  /// Метод вадидации поля ввода пароля
  String? _passwordValidator(String? value) {
    if (value == null || value.isEmpty) {
      return l10n.validMessageEmpty;
    } else if (value.length < 6) {
      return l10n.validMessageLengthLimitationPassword;
    }
    return null;
  }

  /// Метод обрабочик нажатия на кнопку завешения авторизации
  void _onPressedAuthorization(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      final email = _emailController.text.trim();
      final password = _passwordController.text.trim();

      context.read<UserAuthenticationBloc>().add(
            UserAuthorizationEvent(
              email: email,
              password: password,
            ),
          );
    }
  }
}
