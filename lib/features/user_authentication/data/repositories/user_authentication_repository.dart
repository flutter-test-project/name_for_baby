import 'package:name_for_baby/features/user_authentication/domain/i_user_authentication_repository.dart';

class UserAuthenticationRepository implements IUserAuthenticationRepository {
  @override
  Future<bool> checkUserAuthentication() async {
    await Future.delayed(const Duration(seconds: 2));
    return false;
  }
}
