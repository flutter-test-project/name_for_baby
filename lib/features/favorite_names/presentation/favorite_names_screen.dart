import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/presentation/components/cards/name_card.dart';
import 'package:name_for_baby/app/presentation/components/loading/loading_indicator.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/features/favorite_names/domain/state/favorite_names_bloc.dart';
import 'package:name_for_baby/l10n/delegate.dart';
import 'package:name_for_baby/routing/routes.dart';
import 'package:name_for_baby/ui_kit/general/ui_empty_panel.dart';
import 'package:name_for_baby/ui_kit/general/ui_failure_panel.dart';

/// Класс описываюзий страничку понравившихся имен пользователя
class FavoriteNamesScreen extends StatelessWidget {
  const FavoriteNamesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<FavoriteNamesBloc>(
      create: (BuildContext context) => GetIt.instance()
        ..add(
          FavoriteNamesStartedLoading(),
        ),
      child: _FavoriteNamesPage(),
      //child: _FavoriteNamesPage(),
    );
  }
}

class _FavoriteNamesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<FavoriteNamesBloc, FavoriteNamesState>(
      listener: (context, state) {
        if (state is FavoriteNamesErrorMessage) {
          //todo
        }
      },
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: Text(l10n.namesYouLike),
          ),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: RefreshIndicator(
              child: switch (state) {
                FavoriteNamesInitial() => const SizedBox(),
                FavoriteNamesLoading() => const LoadingIndicator(),
                FavoriteNamesLoadSuccess() => Column(
                    children: [
                      const IndentHorizontal(6),
                      Text('${state.favoriteNames.length}'),
                      const IndentHorizontal(6),
                      Expanded(child: _FavoriteNamesContent()),
                    ],
                  ),
                FavoriteNamesFailure() => UiFailurePanel(
                    state.text,
                    onReload: () => onRefresh(context),
                  ),
                FavoriteNamesErrorMessage() => const SizedBox(),
              },
              onRefresh: () async => onRefresh(context),
            ),
          ),
        );
      },
      buildWhen: (context, state) {
        if (state is FavoriteNamesErrorMessage) return false;
        return true;
      },
    );
  }

  /// метод запускающий событие перезвгрузки страницы
  void onRefresh(BuildContext context) {
    context.read<FavoriteNamesBloc>().add(FavoriteNamesStartedLoading());
  }
}

class _FavoriteNamesContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FavoriteNamesBloc, FavoriteNamesState>(builder: (context, state) {
      if (state is FavoriteNamesLoadSuccess) {
        if (state.favoriteNames.isEmpty) {
          return UiEmptyPanel(
            text: l10n.noDataAvailable,
          );
        }
        return ListView(
          children: state.favoriteNames
              .map(
                (name) => Padding(
                  padding: const EdgeInsets.only(top: 4),
                  child: NameCard(
                    fullName: name.fullName,
                    onInfoPressed: () => _goToCardName(
                      context: context,
                      name: name,
                    ),
                    onDeletedPressed: () {
                      context.read<FavoriteNamesBloc>().add((FavoriteNamesDeleted(name.id)));
                    },
                  ),
                ),
              )
              .toList(),
        );
      }

      return const SizedBox();
    });
  }

  void _goToCardName({
    required BuildContext context,
    required CollectorName name,
  }) {
    CardNameRoute(
      nameId: name.id,
      $extra: name,
    ).push(context);
  }
}
