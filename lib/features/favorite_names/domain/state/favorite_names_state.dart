part of 'favorite_names_bloc.dart';

/// Класс состояний для работы с данными понравившихся имен
@immutable
sealed class FavoriteNamesState extends Equatable {
  @override
  List<Object> get props => [];
}

/// Класс начального состояния
final class FavoriteNamesInitial extends FavoriteNamesState {}

/// Класс состояния загрузки данных
final class FavoriteNamesLoading extends FavoriteNamesState {}

/// Класс состояния полученнных данных о понравившихся именах
final class FavoriteNamesLoadSuccess extends FavoriteNamesState {
  FavoriteNamesLoadSuccess({
    required this.favoriteNames,
  });

  /// Список понравившихся имен
  final List<CollectorName> favoriteNames;

  FavoriteNamesLoadSuccess copyWith({
    List<CollectorName>? favoriteNames,
  }) {
    return FavoriteNamesLoadSuccess(
      favoriteNames: favoriteNames ?? this.favoriteNames,
    );
  }

  @override
  List<Object> get props => [
        favoriteNames,
      ];
}

/// Класс вывода критической ошибки
final class FavoriteNamesFailure extends FavoriteNamesState {
  FavoriteNamesFailure(this.text);

  final String text;
}

/// Класс состония вывода сообщения ошибки
final class FavoriteNamesErrorMessage extends FavoriteNamesState {
  FavoriteNamesErrorMessage(this.text);

  final String text;
}
