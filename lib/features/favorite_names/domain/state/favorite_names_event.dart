part of 'favorite_names_bloc.dart';

/// Класс событий понравившихся имен
@immutable
sealed class FavoriteNamesEvent {}

/// Событие начала загрузки данных
final class FavoriteNamesStartedLoading extends FavoriteNamesEvent {}

/// Удадение понравившегося имени из списка понравившихся имен
final class FavoriteNamesDeleted extends FavoriteNamesEvent {
  FavoriteNamesDeleted(this.nameId);

  /// ид имени
  final String nameId;
}
