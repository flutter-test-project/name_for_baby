import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/event_bus/event_bus.dart';
import 'package:name_for_baby/features/event_bus/event_bus_events.dart';
import 'package:name_for_baby/features/names_book/domain/usecase/get_names_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/delete_favorite_name_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/get_favorite_name_ids_usecase.dart';

part 'favorite_names_event.dart';

part 'favorite_names_state.dart';

/// Блок работе с понравившимеся именами
class FavoriteNamesBloc extends Bloc<FavoriteNamesEvent, FavoriteNamesState> {
  FavoriteNamesBloc({
    required GetFavoriteNamesUsecase getFavoriteNamesUsecase,
    required GetNamesUsecase getNamesUsecase,
    required DeleteFavoriteNameUsecase deleteFavoriteNameUsecase,
  })  : _getFavoriteNamesUsecase = getFavoriteNamesUsecase,
        _getNamesUsecase = getNamesUsecase,
        _deleteFavoriteNameUsecase = deleteFavoriteNameUsecase,
        super(FavoriteNamesInitial()) {
    on<FavoriteNamesStartedLoading>(_onLoadStarted);
    on<FavoriteNamesDeleted>(_onDeletedFavoriteName);
  }

  final GetFavoriteNamesUsecase _getFavoriteNamesUsecase;
  final GetNamesUsecase _getNamesUsecase;
  final DeleteFavoriteNameUsecase _deleteFavoriteNameUsecase;

  Future<void> _onLoadStarted(
    FavoriteNamesStartedLoading event,
    Emitter<FavoriteNamesState> emit,
  ) async {
    try {
      emit(FavoriteNamesLoading());
      final List<String> favoriteNameIds = await _getFavoriteNamesUsecase.call(NoParams());
      final List<CollectorName> favoriteNames = await _getNamesUsecase.call(favoriteNameIds);
      emit(FavoriteNamesLoadSuccess(
        favoriteNames: favoriteNames,
      ));
    } catch (error, stackTrace) {
      emit(FavoriteNamesFailure('$error'));
      addError(error, stackTrace);
    }
  }

  Future<void> _onDeletedFavoriteName(
    FavoriteNamesDeleted event,
    Emitter<FavoriteNamesState> emit,
  ) async {
    final oldState = state;
    try {
      if (oldState is FavoriteNamesLoadSuccess) {
        await _deleteFavoriteNameUsecase.call(event.nameId);
        final List<String> favoriteNameIds = await _getFavoriteNamesUsecase.call(NoParams());
        final List<CollectorName> favoriteNames = await _getNamesUsecase.call(favoriteNameIds);
        EventBus().addEvent(
          EditCountSelectedNameEvent(
            countNotFavoriteName: favoriteNames.length,
          ),
        );
        emit(oldState.copyWith(favoriteNames: favoriteNames));
      }
    } catch (error, stackTrace) {
      emit(FavoriteNamesFailure('$error'));
      addError(error, stackTrace);
    }
  }
}
