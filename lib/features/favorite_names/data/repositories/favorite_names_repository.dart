import 'package:name_for_baby/features/favorite_names/domain/i_favorite_names_repository.dart';

/// Реализация репозитория для работы с данными понравившихся имен пользователя
class FavoriteNamesRepository implements IFavoriteNamesRepository {}
