import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:name_for_baby/app/data/collections/firestore_collections.dart';
import 'package:name_for_baby/app/domain/entities/preference_by_name.dart';
import 'package:name_for_baby/features/user_registration/domain/i_user_registration_repository.dart';

/// Реализация репозитория при работе с регистрацией пользователя
class UserRegistrationRepository implements IUserRegistrationRepository {
  final FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  Future<void> createDataUsersCollection({
    required String userLogin,
    PreferenceByName? preferenceByName,
  }) async {
    await db.collection(FirestoreCollections.userData).doc(userLogin).set(
      {
        FirestoreUsersFields.favoriteNameIds: preferenceByName?.favoriteNameIds,
        FirestoreUsersFields.notFavoriteNameIds: preferenceByName?.notFavoriteNameIds,
      },
    );
  }
}
