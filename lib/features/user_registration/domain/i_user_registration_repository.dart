import 'package:name_for_baby/app/domain/entities/preference_by_name.dart';

/// Интерфейс абстрактного репозитория описывающий работу с данными при регистрации пользователя
abstract interface class IUserRegistrationRepository {
  /// Метод который создает коллекцию с данными пользователя
  ///
  /// Принимает
  /// - [userLogin] - логин пользователя
  /// - [preferenceByName] - данные предаочитаемых именах пользователя
  Future<void> createDataUsersCollection({
    required String userLogin,
    PreferenceByName? preferenceByName,
  });
}
