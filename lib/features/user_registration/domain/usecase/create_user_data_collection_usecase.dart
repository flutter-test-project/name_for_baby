import 'package:name_for_baby/app/domain/entities/preference_by_name.dart';
import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/i_local_preference_by_name_repository.dart';
import 'package:name_for_baby/features/user_registration/domain/i_user_registration_repository.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';

/// Юз кейс который созлает новую коллекуию с данными пользователя и его прелпочтениями по выбору имени
class CreateUserDataCollectionUsecase extends BaseUseCase<Future<void>, NoParams> {
  CreateUserDataCollectionUsecase({
    required IUserRegistrationRepository userRegistrationRepository,
    required IUserSecureDataRepository userSecureDataRepository,
    required ILocalPreferenceByNameRepository localPreferenceByNameRepository,
  })  : _userRegistrationRepository = userRegistrationRepository,
        _userSecureDataRepository = userSecureDataRepository,
        _localPreferenceByNameRepository = localPreferenceByNameRepository;

  final IUserRegistrationRepository _userRegistrationRepository;
  final IUserSecureDataRepository _userSecureDataRepository;
  final ILocalPreferenceByNameRepository _localPreferenceByNameRepository;

  @override
  Future<void> call(NoParams params) async {
    final bool isUserAuthentication = await _userSecureDataRepository.isUserAuthentication;
    if (!isUserAuthentication) {
      throw Exception(
          'Данная операция предназначена только пользователей успешно создаших свой аккаунт');
    }
    final String userLogin = await _userSecureDataRepository.getLogin;

    await _userRegistrationRepository.createDataUsersCollection(
      userLogin: userLogin,
      preferenceByName: PreferenceByName(
        favoriteNameIds: _localPreferenceByNameRepository.getFavoriteNameIds(),
        notFavoriteNameIds: _localPreferenceByNameRepository.getNotFavoriteNameIds(),
      ),
    );

    // так как пользователь был авторизован и коллекция с данными предаочтительных имен была создана
    // удаляем данные предпочтительных имен из кеша

    await _localPreferenceByNameRepository.cleanData();
  }
}
