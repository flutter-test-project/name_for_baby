part of 'user_registration_bloc.dart';

/// Класс состояний регистрации
@immutable
sealed class UserRegistrationState extends Equatable {
  const UserRegistrationState();

  @override
  List<Object> get props => [];
}

/// Класс инициализации регистрации
final class UserRegistrationInitial extends UserRegistrationState {}

/// Класс для отображения процесса регистрации
final class UserRegistrationInProgress extends UserRegistrationState {}

/// Состояние завршение создания нового пользователя
final class UserRegistrationComplete extends UserRegistrationState {}

/// Класс для вывода ошибок возникших в процессе аутентификации
final class UserRegistrationProcessError extends UserRegistrationState {
  const UserRegistrationProcessError(
    this.errorText,
  );

  /// Текст ошибки
  final String errorText;
}
