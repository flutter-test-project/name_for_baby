part of 'user_registration_bloc.dart';

/// Класс событий для регистрации
@immutable
sealed class UserRegistrationEvent extends Equatable {
  const UserRegistrationEvent();

  @override
  List<Object?> get props => [];
}

/// Событие нажатия на кнопку зарегестрироваться
final class PressedOnRegisterEvent extends UserRegistrationEvent {
  const PressedOnRegisterEvent({
    required this.email,
    required this.password,
  });

  final String email;
  final String password;
}
