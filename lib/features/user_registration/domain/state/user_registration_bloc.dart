import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/user_registration/domain/usecase/create_user_data_collection_usecase.dart';
import 'package:name_for_baby/features/user_secure_data/domain/usecase/is_user_authentication_usecase.dart';
import 'package:name_for_baby/features/user_secure_data/domain/usecase/save_login_and_password_usecase.dart';

part 'user_registration_event.dart';

part 'user_registration_state.dart';

class UserRegistrationBloc extends Bloc<UserRegistrationEvent, UserRegistrationState> {
  UserRegistrationBloc({
    required IsUserAuthenticationUsecase isUserAuthenticationUsecase,
    required SaveLoginAndPasswordUsecase saveLoginAndPasswordUsecasey,
    required CreateUserDataCollectionUsecase createUserDataCollectionUsecase,
  })  : _isUserAuthenticationUsecase = isUserAuthenticationUsecase,
        _saveLoginAndPasswordUsecase = saveLoginAndPasswordUsecasey,
        _createUserDataCollectionUsecase = createUserDataCollectionUsecase,
        super(UserRegistrationInitial()) {
    on<UserRegistrationEvent>((event, emit) async {
      switch (event) {
        case PressedOnRegisterEvent _:
          await _onUserRegistration(event, emit);
      }
    });
  }

  final IsUserAuthenticationUsecase _isUserAuthenticationUsecase;
  final SaveLoginAndPasswordUsecase _saveLoginAndPasswordUsecase;
  final CreateUserDataCollectionUsecase _createUserDataCollectionUsecase;

  Future<void> _onUserRegistration(
    PressedOnRegisterEvent event,
    Emitter<UserRegistrationState> emit,
  ) async {
    final oldState = state;
    try {
      emit(UserRegistrationInProgress());
      final UserCredential credential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: event.email,
        password: event.password,
      );
      final User? user = credential.user;
      if (user == null) {
        emit(const UserRegistrationProcessError('Пользователь не был создан.'));
        emit(oldState);
        return;
      } else if (user.email == null || user.uid.isEmpty) {
        emit(const UserRegistrationProcessError('Пользователь не был идентифицикрован.'));
        emit(oldState);
        return;
      }

      await _saveLoginAndPasswordUsecase.call(
        UserDataAuth(
          login: event.email,
          password: event.password,
        ),
      );

      final bool isUserBeenIdentified = await _isUserAuthenticationUsecase.call(NoParams());

      if (!isUserBeenIdentified) {
        emit(const UserRegistrationProcessError('Даннные пользователя не были сохранены.'));
        emit(oldState);
        return;
      }

      await _createUserDataCollectionUsecase.call(NoParams());

      emit(UserRegistrationComplete());
    } on FirebaseAuthException catch (error, stackTrace) {
      emit(UserRegistrationProcessError('${error.message}'));
      emit(oldState);
      addError(error, stackTrace);
    } catch (error, stackTrace) {
      emit(UserRegistrationProcessError('$error'));
      emit(oldState);
      addError(error, stackTrace);
    }
  }
}
