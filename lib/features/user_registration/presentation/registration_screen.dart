import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:name_for_baby/app/presentation/components/dialogs/error_dialog.dart';
import 'package:name_for_baby/app/presentation/components/dialogs/info_dialog.dart';
import 'package:name_for_baby/app/presentation/components/loading/loading_indicator.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/app/utils/general_utils.dart';
import 'package:name_for_baby/features/event_bus/event_bus.dart';
import 'package:name_for_baby/features/event_bus/event_bus_events.dart';
import 'package:name_for_baby/features/user_registration/domain/state/user_registration_bloc.dart';
import 'package:name_for_baby/l10n/delegate.dart';
import 'package:name_for_baby/routing/routes.dart';
import 'package:name_for_baby/ui_kit/buttons/ui_button_outline.dart';
import 'package:name_for_baby/ui_kit/fields/ui_text_field.dart';

/// Экран регистрации пользователя
class RegistrationScreen extends StatelessWidget {
  const RegistrationScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UserRegistrationBloc>(
      create: (BuildContext context) => GetIt.instance(),
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 16),
          child: _RegistrationForm(),
        ),
      ),
    );
  }
}

class _RegistrationForm extends StatefulWidget {
  @override
  State<_RegistrationForm> createState() => _RegistrationFormState();
}

class _RegistrationFormState extends State<_RegistrationForm> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordReplayController = TextEditingController();

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _passwordReplayController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<UserRegistrationBloc, UserRegistrationState>(
      listener: (context, state) {
        if (state is UserRegistrationProcessError) {
          _showErrorDialog(state.errorText);
        }
        if (state is UserRegistrationComplete) {
          EventBus().addEvent(
            const UserAuthEvent(),
          );
          _showSuccessUserRegisterDialog();
        }
      },
      builder: (context, state) {
        return Stack(
          children: [
            if (state is UserRegistrationInProgress) const Center(child: LoadingIndicator()),
            Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(l10n.registration),
                  const IndentVertical(12),
                  UiTextFormField(
                    controller: _emailController,
                    validator: _emailValidator,
                    labelText: l10n.email,
                    keyboardType: TextInputType.emailAddress,
                  ),
                  const IndentVertical(12),
                  UiTextFormField(
                    controller: _passwordController,
                    validator: _passwordValidator,
                    labelText: l10n.password,
                    obscureText: true,
                  ),
                  const IndentVertical(12),
                  UiTextFormField(
                    controller: _passwordReplayController,
                    validator: _passwordReplayValidator,
                    labelText: l10n.passwordReplay,
                    obscureText: true,
                  ),
                  const IndentVertical(12),
                  UiButtonOutline(
                    onPressed: state is UserRegistrationInProgress
                        ? null
                        : () => _onPressedRegistration(context),
                    child: Text(l10n.createAccount),
                  ),
                  const IndentVertical(12),
                  //уже есть аккаент
                  if (state is! UserRegistrationInProgress)
                    GestureDetector(
                      child: Text(l10n.iHaveProfile),
                      onTap: () => const AuthorizationRoute().go(context),
                    ),
                ],
              ),
            ),
          ],
        );
      },
      buildWhen: (context, state) {
        if (state is UserRegistrationProcessError) return false;
        return true;
      },
    );
  }

  /// Метод вывода окна сообщения об успешной регистрации
  void _showSuccessUserRegisterDialog() {
    showInfoDialog(
      context: context,
      description: l10n.newUserHasBeenRegistered,
      onPressed: () => const ProfileRoute().go(context),
    );
  }

  /// Метод вывода окна сообщения об ошибке
  void _showErrorDialog(String errorText) {
    showErrorDialog(
      context: context,
      description: errorText,
    );
  }

  /// Метод вадидации поля ввода логина/email
  String? _emailValidator(String? value) {
    if (value == null || value.isEmpty) {
      return l10n.validMessageEmpty;
    } else if (value.isValidEmail) {
      return l10n.validMessageEmailIncorrect;
    }
    return null;
  }

  /// Метод вадидации поля ввода пароля
  String? _passwordValidator(String? value) {
    if (value == null || value.isEmpty) {
      return l10n.validMessageEmpty;
    } else if (value.length < 6) {
      return l10n.validMessageLengthLimitationPassword;
    }
    return null;
  }

  /// Метод вадидации поля повторного ввода пароля
  String? _passwordReplayValidator(String? value) {
    if (value == null || value.isEmpty) {
      return l10n.validMessageEmpty;
    } else if (value.length < 6) {
      return l10n.validMessageLengthLimitationPassword;
    }
    final password = _passwordController.text.trim();
    final passwordReplay = _passwordReplayController.text.trim();

    if (password != passwordReplay) {
      return l10n.validMessagePasswordsAreNotIdentical;
    }
    return null;
  }

  /// Метод обрабочик нажатия на кнопку завешения регистрации
  void _onPressedRegistration(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      final email = _emailController.text.trim();
      final password = _passwordController.text.trim();

      context.read<UserRegistrationBloc>().add(
            PressedOnRegisterEvent(
              email: email,
              password: password,
            ),
          );
    }
  }
}
