import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:name_for_baby/app/presentation/components/dialogs/error_dialog.dart';
import 'package:name_for_baby/app/presentation/components/loading/loading_indicator.dart';
import 'package:name_for_baby/app/presentation/helpers/indents.dart';
import 'package:name_for_baby/features/select_name/domain/state/select_name_bloc.dart';
import 'package:name_for_baby/l10n/delegate.dart';
import 'package:name_for_baby/routing/routes.dart';
import 'package:name_for_baby/ui_kit/buttons/ui_button_outline.dart';
import 'package:name_for_baby/ui_kit/general/ui_failure_panel.dart';

/// Экран для работы с финальными выбранными именами
class SelectNameScreen extends StatelessWidget {
  const SelectNameScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SelectNameBloc>(
      create: (BuildContext context) => GetIt.instance()
        ..add(
          SelectNameStarted(),
        ),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('SelectName'),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: _SelectNameContent(),
        ),
      ),
    );
  }
}

/// Виджет определяюший наполненение эрана в зависимости от состояния
class _SelectNameContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SelectNameBloc, SelectNameState>(
      listener: (context, state) {},
      builder: (context, state) {
        return switch (state) {
          SelectNameInitial() => const SizedBox(),
          SelectNameLoading() => const LoadingIndicator(),
          SelectNameSuccess() => _SelectNameSuccessContent(
              isAuthorization: state.isAuthorization,
            ),
          SelectNameMessage() => const SizedBox(),
          SelectNameFailure() => UiFailurePanel(
              state.text,
              onReload: () => _onErrorReload(context),
            ),
        };
      },
      buildWhen: (context, state) {
        if (state is SelectNameMessage) return false;
        return true;
      },
    );
  }

  /// метод запускающий событие перезвгрузки страницы при ошибки
  void _onErrorReload(BuildContext context) {
    context.read<SelectNameBloc>().add(SelectNameStarted());
  }
}

/// Основной контент страницы
class _SelectNameSuccessContent extends StatelessWidget {
  const _SelectNameSuccessContent({
    required this.isAuthorization,
  });

  final bool isAuthorization;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Expanded(child: SizedBox()),
        Center(
          child: UiButtonOutline(
            child: Text(
              l10n.chooseAName,
            ),
            onPressed: () {
              if (isAuthorization) {
                const DataForGameRoute().go(context);
              } else {
                _pageIsOnlyUserAuthMessage(context);
              }
            },
          ),
        ),
        const IndentVertical(12),
      ],
    );
  }

  /// Метод вывода диолога о недоступности перехода на страничку финального выбора имени
  /// для не авторизованных пользователей
  void _pageIsOnlyUserAuthMessage(BuildContext context) {
    showErrorDialog(
      context: context,
      title: l10n.mistake,
      description: l10n.pageIsOnlyUserAuth,
    );
  }
}
