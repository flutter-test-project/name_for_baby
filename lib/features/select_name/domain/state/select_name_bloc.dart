import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:name_for_baby/app/domain/usecase/base_usecase.dart';
import 'package:name_for_baby/features/event_bus/event_bus.dart';
import 'package:name_for_baby/features/event_bus/event_bus_events.dart';
import 'package:name_for_baby/features/user_secure_data/domain/usecase/is_user_authentication_usecase.dart';

part 'select_name_event.dart';

part 'select_name_state.dart';

/// Блок для работы с экраном выбранных имен
class SelectNameBloc extends Bloc<SelectNameEvent, SelectNameState> {
  SelectNameBloc({
    required IsUserAuthenticationUsecase isUserAuthenticationUsecase,
    required EventBus eventBus,
  })  : _isUserAuthenticationUsecase = isUserAuthenticationUsecase,
        _eventBus = eventBus,
        super(SelectNameInitial()) {
    on<SelectNameStarted>(_onLoadStarted);

    _streamSubscription = _eventBus.on<UserAuthEvent>().listen(
      (event) {
        add(SelectNameStarted());
      },
    );
  }

  final IsUserAuthenticationUsecase _isUserAuthenticationUsecase;

  final EventBus _eventBus;
  late final StreamSubscription _streamSubscription;

  @override
  Future<void> close() {
    _streamSubscription.cancel();
    return super.close();
  }

  Future<void> _onLoadStarted(
    SelectNameStarted event,
    Emitter<SelectNameState> emit,
  ) async {
    try {
      emit(SelectNameLoading());
      final bool isAuthentication = await _isUserAuthenticationUsecase.call(NoParams());
      emit(SelectNameSuccess(
        isAuthorization: isAuthentication,
      ));
    } catch (error, stackTrace) {
      emit(SelectNameFailure('$error'));
      addError(error, stackTrace);
    }
  }
}
