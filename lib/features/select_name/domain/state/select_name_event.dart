part of 'select_name_bloc.dart';

/// Класс для работы с состояниями экрана выбранных имен
@immutable
sealed class SelectNameEvent {}

/// Событие загрузки страницы профиля
final class SelectNameStarted extends SelectNameEvent {}
