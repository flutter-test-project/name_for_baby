part of 'select_name_bloc.dart';

/// Класс состояний для работы с экраном выбранных имен
@immutable
sealed class SelectNameState extends Equatable {
  @override
  List<Object> get props => [];
}

/// Класс начального состояния
final class SelectNameInitial extends SelectNameState {}

/// Класс состояния загрузки данных
final class SelectNameLoading extends SelectNameState {}

/// Класс состояния полученнных данных для экрана выбранных имен
final class SelectNameSuccess extends SelectNameState {
  SelectNameSuccess({
    required this.isAuthorization,
  });

  /// Флаг показывающий авторизован ли пользователь
  final bool isAuthorization;
}

/// Класс вывода сообщения
final class SelectNameMessage extends SelectNameState {
  SelectNameMessage({
    required this.title,
    required this.description,
  });

  /// Заголовок
  final String title;

  /// Описание
  final String description;
}

/// Класс вывода критической ошибки
final class SelectNameFailure extends SelectNameState {
  SelectNameFailure(this.text);

  final String text;
}
