/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart' as _svg;
import 'package:vector_graphics/vector_graphics.dart' as _vg;

class $AssetsCsvGen {
  const $AssetsCsvGen();

  /// File path: assets/csv/boy_collection.xlsx
  String get boyCollection => 'assets/csv/boy_collection.xlsx';

  /// File path: assets/csv/girl_collection.xlsx
  String get girlCollection => 'assets/csv/girl_collection.xlsx';

  /// File path: assets/csv/tags_collections.xlsx
  String get tagsCollections => 'assets/csv/tags_collections.xlsx';

  /// File path: assets/csv/text.txt
  String get text => 'assets/csv/text.txt';

  /// List of all assets
  List<String> get values =>
      [boyCollection, girlCollection, tagsCollections, text];
}

class $AssetsSvgGen {
  const $AssetsSvgGen();

  /// Directory path: assets/svg/icons
  $AssetsSvgIconsGen get icons => const $AssetsSvgIconsGen();
}

class $AssetsSvgIconsGen {
  const $AssetsSvgIconsGen();

  /// File path: assets/svg/icons/add.svg
  SvgGenImage get add => const SvgGenImage('assets/svg/icons/add.svg');

  /// File path: assets/svg/icons/arrow-right.svg
  SvgGenImage get arrowRight =>
      const SvgGenImage('assets/svg/icons/arrow-right.svg');

  /// File path: assets/svg/icons/dislike.svg
  SvgGenImage get dislike => const SvgGenImage('assets/svg/icons/dislike.svg');

  /// File path: assets/svg/icons/filter.svg
  SvgGenImage get filter => const SvgGenImage('assets/svg/icons/filter.svg');

  /// File path: assets/svg/icons/heart.svg
  SvgGenImage get heart => const SvgGenImage('assets/svg/icons/heart.svg');

  /// File path: assets/svg/icons/row-vertical.svg
  SvgGenImage get rowVertical =>
      const SvgGenImage('assets/svg/icons/row-vertical.svg');

  /// File path: assets/svg/icons/slider-vertical.svg
  SvgGenImage get sliderVertical =>
      const SvgGenImage('assets/svg/icons/slider-vertical.svg');

  /// List of all assets
  List<SvgGenImage> get values =>
      [add, arrowRight, dislike, filter, heart, rowVertical, sliderVertical];
}

class Assets {
  Assets._();

  static const $AssetsCsvGen csv = $AssetsCsvGen();
  static const $AssetsSvgGen svg = $AssetsSvgGen();
}

class SvgGenImage {
  const SvgGenImage(
    this._assetName, {
    this.size,
    this.flavors = const {},
  }) : _isVecFormat = false;

  const SvgGenImage.vec(
    this._assetName, {
    this.size,
    this.flavors = const {},
  }) : _isVecFormat = true;

  final String _assetName;
  final Size? size;
  final Set<String> flavors;
  final bool _isVecFormat;

  _svg.SvgPicture svg({
    Key? key,
    bool matchTextDirection = false,
    AssetBundle? bundle,
    String? package,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    AlignmentGeometry alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder? placeholderBuilder,
    String? semanticsLabel,
    bool excludeFromSemantics = false,
    _svg.SvgTheme? theme,
    ColorFilter? colorFilter,
    Clip clipBehavior = Clip.hardEdge,
    @deprecated Color? color,
    @deprecated BlendMode colorBlendMode = BlendMode.srcIn,
    @deprecated bool cacheColorFilter = false,
  }) {
    final _svg.BytesLoader loader;
    if (_isVecFormat) {
      loader = _vg.AssetBytesLoader(
        _assetName,
        assetBundle: bundle,
        packageName: package,
      );
    } else {
      loader = _svg.SvgAssetLoader(
        _assetName,
        assetBundle: bundle,
        packageName: package,
        theme: theme,
      );
    }
    return _svg.SvgPicture(
      loader,
      key: key,
      matchTextDirection: matchTextDirection,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      placeholderBuilder: placeholderBuilder,
      semanticsLabel: semanticsLabel,
      excludeFromSemantics: excludeFromSemantics,
      colorFilter: colorFilter ??
          (color == null ? null : ColorFilter.mode(color, colorBlendMode)),
      clipBehavior: clipBehavior,
      cacheColorFilter: cacheColorFilter,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
