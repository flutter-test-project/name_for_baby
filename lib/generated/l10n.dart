// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Да`
  String get yes {
    return Intl.message(
      'Да',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  /// `Нет`
  String get no {
    return Intl.message(
      'Нет',
      name: 'no',
      desc: '',
      args: [],
    );
  }

  /// `Ошибка`
  String get mistake {
    return Intl.message(
      'Ошибка',
      name: 'mistake',
      desc: '',
      args: [],
    );
  }

  /// `Предупреждение`
  String get warning {
    return Intl.message(
      'Предупреждение',
      name: 'warning',
      desc: '',
      args: [],
    );
  }

  /// `Полное имя:`
  String get fullNameText {
    return Intl.message(
      'Полное имя:',
      name: 'fullNameText',
      desc: '',
      args: [],
    );
  }

  /// `Церковное имя:`
  String get churchNameText {
    return Intl.message(
      'Церковное имя:',
      name: 'churchNameText',
      desc: '',
      args: [],
    );
  }

  /// `Краткая форма:`
  String get shortFormNameText {
    return Intl.message(
      'Краткая форма:',
      name: 'shortFormNameText',
      desc: '',
      args: [],
    );
  }

  /// `Синонимы:`
  String get synonymsNameText {
    return Intl.message(
      'Синонимы:',
      name: 'synonymsNameText',
      desc: '',
      args: [],
    );
  }

  /// `Происхождение и значение:`
  String get originAndValueNameText {
    return Intl.message(
      'Происхождение и значение:',
      name: 'originAndValueNameText',
      desc: '',
      args: [],
    );
  }

  /// `Значение:`
  String get valueNameText {
    return Intl.message(
      'Значение:',
      name: 'valueNameText',
      desc: '',
      args: [],
    );
  }

  /// `Характер`
  String get personalityText {
    return Intl.message(
      'Характер',
      name: 'personalityText',
      desc: '',
      args: [],
    );
  }

  /// `Карточка имени`
  String get cardNameTitle {
    return Intl.message(
      'Карточка имени',
      name: 'cardNameTitle',
      desc: '',
      args: [],
    );
  }

  /// `Мой партнер`
  String get myPartner {
    return Intl.message(
      'Мой партнер',
      name: 'myPartner',
      desc: '',
      args: [],
    );
  }

  /// `Отчество`
  String get patronymic {
    return Intl.message(
      'Отчество',
      name: 'patronymic',
      desc: '',
      args: [],
    );
  }

  /// `Выйти`
  String get exit {
    return Intl.message(
      'Выйти',
      name: 'exit',
      desc: '',
      args: [],
    );
  }

  /// `Войти`
  String get entrance {
    return Intl.message(
      'Войти',
      name: 'entrance',
      desc: '',
      args: [],
    );
  }

  /// `Создать аккаунт`
  String get createAccount {
    return Intl.message(
      'Создать аккаунт',
      name: 'createAccount',
      desc: '',
      args: [],
    );
  }

  /// `Фильтр`
  String get filter {
    return Intl.message(
      'Фильтр',
      name: 'filter',
      desc: '',
      args: [],
    );
  }

  /// `Пол:`
  String get sexColonText {
    return Intl.message(
      'Пол:',
      name: 'sexColonText',
      desc: '',
      args: [],
    );
  }

  /// `Группа имен:`
  String get namesGroupText {
    return Intl.message(
      'Группа имен:',
      name: 'namesGroupText',
      desc: '',
      args: [],
    );
  }

  /// `Перезагрузить`
  String get toReloadText {
    return Intl.message(
      'Перезагрузить',
      name: 'toReloadText',
      desc: '',
      args: [],
    );
  }

  /// `Авторизация`
  String get authorization {
    return Intl.message(
      'Авторизация',
      name: 'authorization',
      desc: '',
      args: [],
    );
  }

  /// `Регистрация`
  String get registration {
    return Intl.message(
      'Регистрация',
      name: 'registration',
      desc: '',
      args: [],
    );
  }

  /// `Логин`
  String get login {
    return Intl.message(
      'Логин',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `E-mail`
  String get email {
    return Intl.message(
      'E-mail',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `Пароль`
  String get password {
    return Intl.message(
      'Пароль',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Упс`
  String get ups {
    return Intl.message(
      'Упс',
      name: 'ups',
      desc: '',
      args: [],
    );
  }

  /// `Понятно`
  String get clear {
    return Intl.message(
      'Понятно',
      name: 'clear',
      desc: '',
      args: [],
    );
  }

  /// `Сохранить`
  String get save {
    return Intl.message(
      'Сохранить',
      name: 'save',
      desc: '',
      args: [],
    );
  }

  /// `Далее`
  String get further {
    return Intl.message(
      'Далее',
      name: 'further',
      desc: '',
      args: [],
    );
  }

  /// `Повтор пароля`
  String get passwordReplay {
    return Intl.message(
      'Повтор пароля',
      name: 'passwordReplay',
      desc: '',
      args: [],
    );
  }

  /// `У меня уже есть аккаунт`
  String get iHaveProfile {
    return Intl.message(
      'У меня уже есть аккаунт',
      name: 'iHaveProfile',
      desc: '',
      args: [],
    );
  }

  /// `Это поле не может быть пустым`
  String get validMessageEmpty {
    return Intl.message(
      'Это поле не может быть пустым',
      name: 'validMessageEmpty',
      desc: '',
      args: [],
    );
  }

  /// `Пожалуйста, введите адрес электронной почты`
  String get validMessageEnterTheEmail {
    return Intl.message(
      'Пожалуйста, введите адрес электронной почты',
      name: 'validMessageEnterTheEmail',
      desc: '',
      args: [],
    );
  }

  /// `Не корректно введен адрес электронной почты`
  String get validMessageEmailIncorrect {
    return Intl.message(
      'Не корректно введен адрес электронной почты',
      name: 'validMessageEmailIncorrect',
      desc: '',
      args: [],
    );
  }

  /// `Пожалуйста, введите пароль`
  String get validMessageEnterThePassword {
    return Intl.message(
      'Пожалуйста, введите пароль',
      name: 'validMessageEnterThePassword',
      desc: '',
      args: [],
    );
  }

  /// `Длина пароля не может быть меньше 6 символов`
  String get validMessageLengthLimitationPassword {
    return Intl.message(
      'Длина пароля не может быть меньше 6 символов',
      name: 'validMessageLengthLimitationPassword',
      desc: '',
      args: [],
    );
  }

  /// `Пароли не совпадают`
  String get validMessagePasswordsAreNotIdentical {
    return Intl.message(
      'Пароли не совпадают',
      name: 'validMessagePasswordsAreNotIdentical',
      desc: '',
      args: [],
    );
  }

  /// `Введенное Вами значение не может быть меньше {count}`
  String valueCannotBeLess(String count) {
    return Intl.message(
      'Введенное Вами значение не может быть меньше $count',
      name: 'valueCannotBeLess',
      desc: 'description',
      args: [count],
    );
  }

  /// `Введенное Вами значение не может быть больше {count}`
  String valueCannotBeMore(String count) {
    return Intl.message(
      'Введенное Вами значение не может быть больше $count',
      name: 'valueCannotBeMore',
      desc: 'description',
      args: [count],
    );
  }

  /// `Вы можете добавить до {count} имен в выборку`
  String youCanAddToCountName(String count) {
    return Intl.message(
      'Вы можете добавить до $count имен в выборку',
      name: 'youCanAddToCountName',
      desc: '',
      args: [count],
    );
  }

  /// `Выбрано {from} из {to}`
  String selectedCountName(String from, String to) {
    return Intl.message(
      'Выбрано $from из $to',
      name: 'selectedCountName',
      desc: '',
      args: [from, to],
    );
  }

  /// `Новый пользователь был зарегестрирован`
  String get newUserHasBeenRegistered {
    return Intl.message(
      'Новый пользователь был зарегестрирован',
      name: 'newUserHasBeenRegistered',
      desc: '',
      args: [],
    );
  }

  /// `Вы уверены что хотите выйти из приложения?`
  String get areYouWantToGetOut {
    return Intl.message(
      'Вы уверены что хотите выйти из приложения?',
      name: 'areYouWantToGetOut',
      desc: '',
      args: [],
    );
  }

  /// `Понравившиеся имена`
  String get namesYouLike {
    return Intl.message(
      'Понравившиеся имена',
      name: 'namesYouLike',
      desc: '',
      args: [],
    );
  }

  /// `Не понравившиеся имена`
  String get namesYouDontLike {
    return Intl.message(
      'Не понравившиеся имена',
      name: 'namesYouDontLike',
      desc: '',
      args: [],
    );
  }

  /// `По этому запросу нет данных`
  String get noDataAvailable {
    return Intl.message(
      'По этому запросу нет данных',
      name: 'noDataAvailable',
      desc: '',
      args: [],
    );
  }

  /// `Данный раздел доступен только для авторизованных пользователей`
  String get sectionForAuthorizedUsers {
    return Intl.message(
      'Данный раздел доступен только для авторизованных пользователей',
      name: 'sectionForAuthorizedUsers',
      desc: '',
      args: [],
    );
  }

  /// `Добавленные имена`
  String get addedNames {
    return Intl.message(
      'Добавленные имена',
      name: 'addedNames',
      desc: '',
      args: [],
    );
  }

  /// `Добавить имя`
  String get addName {
    return Intl.message(
      'Добавить имя',
      name: 'addName',
      desc: '',
      args: [],
    );
  }

  /// `Эта страница доступна только для авторизованных пользователей`
  String get pageIsOnlyUserAuth {
    return Intl.message(
      'Эта страница доступна только для авторизованных пользователей',
      name: 'pageIsOnlyUserAuth',
      desc: '',
      args: [],
    );
  }

  /// `Данная операция доступна только для авторизованных пользователей`
  String get operationIsOnlyUserAuth {
    return Intl.message(
      'Данная операция доступна только для авторизованных пользователей',
      name: 'operationIsOnlyUserAuth',
      desc: '',
      args: [],
    );
  }

  /// `Имя быдо успешно добавлено в вашу личную уоллекцию`
  String get addSuccessPersonalName {
    return Intl.message(
      'Имя быдо успешно добавлено в вашу личную уоллекцию',
      name: 'addSuccessPersonalName',
      desc: '',
      args: [],
    );
  }

  /// `Для мальчика`
  String get forBoy {
    return Intl.message(
      'Для мальчика',
      name: 'forBoy',
      desc: '',
      args: [],
    );
  }

  /// `Для девочки`
  String get forGirl {
    return Intl.message(
      'Для девочки',
      name: 'forGirl',
      desc: '',
      args: [],
    );
  }

  /// `Данные отчества для малыша были обновлены`
  String get updateDataBabiesPatronymic {
    return Intl.message(
      'Данные отчества для малыша были обновлены',
      name: 'updateDataBabiesPatronymic',
      desc: '',
      args: [],
    );
  }

  /// `Выбрать имя`
  String get chooseAName {
    return Intl.message(
      'Выбрать имя',
      name: 'chooseAName',
      desc: '',
      args: [],
    );
  }

  /// `Добавление имени`
  String get addingName {
    return Intl.message(
      'Добавление имени',
      name: 'addingName',
      desc: '',
      args: [],
    );
  }

  /// `Для малыша какого пола выбираем имя?`
  String get whatGenderForGame {
    return Intl.message(
      'Для малыша какого пола выбираем имя?',
      name: 'whatGenderForGame',
      desc: '',
      args: [],
    );
  }

  /// `Выберете максимальное колличество имен учавствующих в финальной выборке`
  String get selectMaximumQuantityNamesForGame {
    return Intl.message(
      'Выберете максимальное колличество имен учавствующих в финальной выборке',
      name: 'selectMaximumQuantityNamesForGame',
      desc: '',
      args: [],
    );
  }

  /// `Вести свое значение`
  String get keepYourOwnValue {
    return Intl.message(
      'Вести свое значение',
      name: 'keepYourOwnValue',
      desc: '',
      args: [],
    );
  }

  /// `Выбрать из списка`
  String get selectFromTheList {
    return Intl.message(
      'Выбрать из списка',
      name: 'selectFromTheList',
      desc: '',
      args: [],
    );
  }

  /// `Добавьте имена из своего списка для того что бы запустить игру для случайного выбора имени`
  String get addNamesFromYourListInOrder {
    return Intl.message(
      'Добавьте имена из своего списка для того что бы запустить игру для случайного выбора имени',
      name: 'addNamesFromYourListInOrder',
      desc: '',
      args: [],
    );
  }

  /// `Запустить игру`
  String get launchTheGame {
    return Intl.message(
      'Запустить игру',
      name: 'launchTheGame',
      desc: '',
      args: [],
    );
  }

  /// `Добавить имена`
  String get addNames {
    return Intl.message(
      'Добавить имена',
      name: 'addNames',
      desc: '',
      args: [],
    );
  }

  /// `Процесс выбора имени уже был запущен`
  String get nameSelectionProcessHasBeenStarted {
    return Intl.message(
      'Процесс выбора имени уже был запущен',
      name: 'nameSelectionProcessHasBeenStarted',
      desc: '',
      args: [],
    );
  }

  /// `Ваш список`
  String get yourList {
    return Intl.message(
      'Ваш список',
      name: 'yourList',
      desc: '',
      args: [],
    );
  }

  /// `Найти еще имена`
  String get findMoreNames {
    return Intl.message(
      'Найти еще имена',
      name: 'findMoreNames',
      desc: '',
      args: [],
    );
  }

  /// `Добавить еще имена`
  String get addMoreNames {
    return Intl.message(
      'Добавить еще имена',
      name: 'addMoreNames',
      desc: '',
      args: [],
    );
  }

  /// `Вы выбрали слишком много имен`
  String get youHaveChosenTooManyNames {
    return Intl.message(
      'Вы выбрали слишком много имен',
      name: 'youHaveChosenTooManyNames',
      desc: '',
      args: [],
    );
  }

  /// `Добавление имени в игру`
  String get addingNameToGame {
    return Intl.message(
      'Добавление имени в игру',
      name: 'addingNameToGame',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'ru'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
