// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru';

  static String m0(from, to) => "Выбрано ${from} из ${to}";

  static String m1(count) =>
      "Введенное Вами значение не может быть меньше ${count}";

  static String m2(count) =>
      "Введенное Вами значение не может быть больше ${count}";

  static String m3(count) => "Вы можете добавить до ${count} имен в выборку";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "addMoreNames":
            MessageLookupByLibrary.simpleMessage("Добавить еще имена"),
        "addName": MessageLookupByLibrary.simpleMessage("Добавить имя"),
        "addNames": MessageLookupByLibrary.simpleMessage("Добавить имена"),
        "addNamesFromYourListInOrder": MessageLookupByLibrary.simpleMessage(
            "Добавьте имена из своего списка для того что бы запустить игру для случайного выбора имени"),
        "addSuccessPersonalName": MessageLookupByLibrary.simpleMessage(
            "Имя быдо успешно добавлено в вашу личную уоллекцию"),
        "addedNames": MessageLookupByLibrary.simpleMessage("Добавленные имена"),
        "addingName": MessageLookupByLibrary.simpleMessage("Добавление имени"),
        "addingNameToGame":
            MessageLookupByLibrary.simpleMessage("Добавление имени в игру"),
        "areYouWantToGetOut": MessageLookupByLibrary.simpleMessage(
            "Вы уверены что хотите выйти из приложения?"),
        "authorization": MessageLookupByLibrary.simpleMessage("Авторизация"),
        "cardNameTitle": MessageLookupByLibrary.simpleMessage("Карточка имени"),
        "chooseAName": MessageLookupByLibrary.simpleMessage("Выбрать имя"),
        "churchNameText":
            MessageLookupByLibrary.simpleMessage("Церковное имя:"),
        "clear": MessageLookupByLibrary.simpleMessage("Понятно"),
        "createAccount":
            MessageLookupByLibrary.simpleMessage("Создать аккаунт"),
        "email": MessageLookupByLibrary.simpleMessage("E-mail"),
        "entrance": MessageLookupByLibrary.simpleMessage("Войти"),
        "exit": MessageLookupByLibrary.simpleMessage("Выйти"),
        "filter": MessageLookupByLibrary.simpleMessage("Фильтр"),
        "findMoreNames":
            MessageLookupByLibrary.simpleMessage("Найти еще имена"),
        "forBoy": MessageLookupByLibrary.simpleMessage("Для мальчика"),
        "forGirl": MessageLookupByLibrary.simpleMessage("Для девочки"),
        "fullNameText": MessageLookupByLibrary.simpleMessage("Полное имя:"),
        "further": MessageLookupByLibrary.simpleMessage("Далее"),
        "iHaveProfile":
            MessageLookupByLibrary.simpleMessage("У меня уже есть аккаунт"),
        "keepYourOwnValue":
            MessageLookupByLibrary.simpleMessage("Вести свое значение"),
        "launchTheGame": MessageLookupByLibrary.simpleMessage("Запустить игру"),
        "login": MessageLookupByLibrary.simpleMessage("Логин"),
        "mistake": MessageLookupByLibrary.simpleMessage("Ошибка"),
        "myPartner": MessageLookupByLibrary.simpleMessage("Мой партнер"),
        "nameSelectionProcessHasBeenStarted":
            MessageLookupByLibrary.simpleMessage(
                "Процесс выбора имени уже был запущен"),
        "namesGroupText": MessageLookupByLibrary.simpleMessage("Группа имен:"),
        "namesYouDontLike":
            MessageLookupByLibrary.simpleMessage("Не понравившиеся имена"),
        "namesYouLike":
            MessageLookupByLibrary.simpleMessage("Понравившиеся имена"),
        "newUserHasBeenRegistered": MessageLookupByLibrary.simpleMessage(
            "Новый пользователь был зарегестрирован"),
        "no": MessageLookupByLibrary.simpleMessage("Нет"),
        "noDataAvailable":
            MessageLookupByLibrary.simpleMessage("По этому запросу нет данных"),
        "operationIsOnlyUserAuth": MessageLookupByLibrary.simpleMessage(
            "Данная операция доступна только для авторизованных пользователей"),
        "originAndValueNameText":
            MessageLookupByLibrary.simpleMessage("Происхождение и значение:"),
        "pageIsOnlyUserAuth": MessageLookupByLibrary.simpleMessage(
            "Эта страница доступна только для авторизованных пользователей"),
        "password": MessageLookupByLibrary.simpleMessage("Пароль"),
        "passwordReplay": MessageLookupByLibrary.simpleMessage("Повтор пароля"),
        "patronymic": MessageLookupByLibrary.simpleMessage("Отчество"),
        "personalityText": MessageLookupByLibrary.simpleMessage("Характер"),
        "registration": MessageLookupByLibrary.simpleMessage("Регистрация"),
        "save": MessageLookupByLibrary.simpleMessage("Сохранить"),
        "sectionForAuthorizedUsers": MessageLookupByLibrary.simpleMessage(
            "Данный раздел доступен только для авторизованных пользователей"),
        "selectFromTheList":
            MessageLookupByLibrary.simpleMessage("Выбрать из списка"),
        "selectMaximumQuantityNamesForGame": MessageLookupByLibrary.simpleMessage(
            "Выберете максимальное колличество имен учавствующих в финальной выборке"),
        "selectedCountName": m0,
        "sexColonText": MessageLookupByLibrary.simpleMessage("Пол:"),
        "shortFormNameText":
            MessageLookupByLibrary.simpleMessage("Краткая форма:"),
        "synonymsNameText": MessageLookupByLibrary.simpleMessage("Синонимы:"),
        "toReloadText": MessageLookupByLibrary.simpleMessage("Перезагрузить"),
        "updateDataBabiesPatronymic": MessageLookupByLibrary.simpleMessage(
            "Данные отчества для малыша были обновлены"),
        "ups": MessageLookupByLibrary.simpleMessage("Упс"),
        "validMessageEmailIncorrect": MessageLookupByLibrary.simpleMessage(
            "Не корректно введен адрес электронной почты"),
        "validMessageEmpty": MessageLookupByLibrary.simpleMessage(
            "Это поле не может быть пустым"),
        "validMessageEnterTheEmail": MessageLookupByLibrary.simpleMessage(
            "Пожалуйста, введите адрес электронной почты"),
        "validMessageEnterThePassword":
            MessageLookupByLibrary.simpleMessage("Пожалуйста, введите пароль"),
        "validMessageLengthLimitationPassword":
            MessageLookupByLibrary.simpleMessage(
                "Длина пароля не может быть меньше 6 символов"),
        "validMessagePasswordsAreNotIdentical":
            MessageLookupByLibrary.simpleMessage("Пароли не совпадают"),
        "valueCannotBeLess": m1,
        "valueCannotBeMore": m2,
        "valueNameText": MessageLookupByLibrary.simpleMessage("Значение:"),
        "warning": MessageLookupByLibrary.simpleMessage("Предупреждение"),
        "whatGenderForGame": MessageLookupByLibrary.simpleMessage(
            "Для малыша какого пола выбираем имя?"),
        "yes": MessageLookupByLibrary.simpleMessage("Да"),
        "youCanAddToCountName": m3,
        "youHaveChosenTooManyNames": MessageLookupByLibrary.simpleMessage(
            "Вы выбрали слишком много имен"),
        "yourList": MessageLookupByLibrary.simpleMessage("Ваш список")
      };
}
