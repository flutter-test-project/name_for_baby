import 'package:flutter/material.dart';

/// Виджет круглой кнопки
class UiButtonRound extends StatelessWidget {
  const UiButtonRound({
    required this.child,
    super.key,
    this.onPressed,
    this.backgroundColor,
  });

  /// потомок данной кнопки
  final Widget child;

  /// Событие нажатие на кнопку
  final void Function()? onPressed;

  /// Внутренний цвет кнопки
  final Color? backgroundColor;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        shape: const CircleBorder(),
        padding: const EdgeInsets.symmetric(vertical: 12),
        backgroundColor: backgroundColor ?? Colors.white,
        foregroundColor: Colors.pinkAccent,
      ),
      child: child,
    );
  }
}
