import 'package:flutter/material.dart';
import 'package:name_for_baby/ui_kit/ui_tokens.dart';

enum UiButtonSize {
  l(
    56,
    padding: EdgeInsets.symmetric(
      horizontal: UiTokens.buttonPaddingPadding16,
      vertical: UiTokens.buttonPaddingPadding12,
    ),
  ),
  m(
    48,
    padding: EdgeInsets.symmetric(
      horizontal: UiTokens.buttonPaddingPadding16,
      vertical: UiTokens.buttonPaddingPadding12,
    ),
  ),
  s(
    36,
    padding: EdgeInsets.symmetric(
      horizontal: UiTokens.buttonPaddingPadding16,
      vertical: UiTokens.buttonPaddingPadding6,
    ),
  ),
  xs(
    32,
    padding: EdgeInsets.symmetric(
      horizontal: UiTokens.buttonPaddingPadding16,
      vertical: UiTokens.buttonPaddingPadding2,
    ),
  );

  const UiButtonSize(
    this.height, {
    required this.padding,
  });

  final double height;
  final EdgeInsets padding;
}
