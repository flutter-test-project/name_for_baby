import 'package:flutter/material.dart';
import 'package:name_for_baby/ui_kit/buttons/ui_button_size.dart';
import 'package:name_for_baby/ui_kit/ui_text_styles.dart';
import 'package:name_for_baby/ui_kit/ui_tokens.dart';

class UiButtonOutline extends StatelessWidget {
  const UiButtonOutline({
    required this.child,
    this.buttonSize = UiButtonSize.m,
    this.padding,
    this.onPressed,
    this.style,
    this.foregroundColor,
    this.disabledForegroundColor,
    this.borderColor,
    this.disabledBorderColor,
    this.borderRadius = UiTokens.roundingRounding12,
    super.key,
  });

  final UiButtonSize buttonSize;
  final EdgeInsets? padding;
  final VoidCallback? onPressed;
  final TextStyle? style;
  final Widget child;
  final Color? foregroundColor;
  final Color? disabledForegroundColor;
  final Color? borderColor;
  final Color? disabledBorderColor;
  final double borderRadius;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: buttonSize.height,
      child: TextButton(
        onPressed: onPressed,
        style: ButtonStyle(
          padding: WidgetStateProperty.all<EdgeInsets>(
            padding ?? buttonSize.padding,
          ),
          textStyle: WidgetStateProperty.all<TextStyle>(
            style ?? UiTextStyles.body1SemiBold,
          ),
          foregroundColor: WidgetStateProperty.resolveWith<Color>(
            (states) {
              if (states.contains(WidgetState.disabled)) {
                return disabledForegroundColor ?? UiTokens.buttonTextTextDisabled;
              }

              return foregroundColor ?? UiTokens.buttonTextTextActive;
            },
          ),
          shape: WidgetStateProperty.resolveWith(
            (states) {
              if (states.contains(WidgetState.disabled)) {
                return RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(borderRadius),
                  side: BorderSide(
                    color: disabledBorderColor ?? UiTokens.buttonOutlineOutlineDisabled,
                    width: 1,
                  ),
                );
              }

              return RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(borderRadius),
                side: BorderSide(
                  color: borderColor ?? UiTokens.buttonOutlineOutlineActive,
                  width: 1,
                ),
              );
            },
          ),
        ),
        child: child,
      ),
    );
  }
}
