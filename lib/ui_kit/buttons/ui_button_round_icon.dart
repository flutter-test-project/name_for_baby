import 'package:flutter/material.dart';

class UiButtonRoundIcon extends StatelessWidget {
  const UiButtonRoundIcon({
    super.key,
    required this.icon,
    this.onPressed,
    this.backgroundColor,
  });

  final IconData icon;
  final void Function()? onPressed;
  /// Цвет кнопки
  final Color? backgroundColor;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        shape: const CircleBorder(),
        padding: const EdgeInsets.symmetric(vertical: 12),
        backgroundColor: backgroundColor ?? Colors.blue,
        foregroundColor: Colors.red,
      ),
      child: Icon(icon, color: Colors.white),
    );
  }
}
