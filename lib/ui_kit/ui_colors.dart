import 'package:flutter/material.dart';

/// Примитивы цветов для использования при редизайне вместо [DixyColors]
/// Нейминг сохраняется согласно текущим примитивам в переменных DIXY UI KIT
/// так как именно так их будут вибирать в дизайнах и токенах
/// https://www.figma.com/file/s5pkF4N5Id7N3hV8pShABu/DIXY-UI-KIT?type=design&node-id=6734-15813&mode=design&t=5CHT6KcgCdvugaCh-11
abstract class UiColors {
  const UiColors._();

  /// Global
  static const Color globalBlack = Color(0xFF000000);
  static const Color globalBlackBase = Color(0xFF000000);
  static const Color globalBlackDark = Color(0xFF1F2429);
  static const Color globalBlackLight = Color(0xFF272D33);
  static const Color globalWhite = Color(0xFFFFFFFF);

  /// Secondary
  static const Color secondaryRed = Color(0xFFE64646);
  static const Color secondaryDixyGreen = Color(0xFF60C339);
  static const Color secondaryDixyYellow = Color(0xFFFBA819);
  static const Color secondaryDark = Color(0xFF6A7177);
  static const Color secondaryLight = Color(0xFFC6CED3);
  static const Color secondaryMedium = Color(0xFF9FAAB2);

  /// Background
  static const Color backgroundLight = Color(0xFFFAFAFA);
  static const Color backgroundBgDark = Color(0xFFE4E8EA);
  static const Color backgroundBgLight = Color(0xFFFAFAFA);
  static const Color backgroundBgDark50 = Color(0x80E4E8EA);
  static final Color backgroundDark = const Color(0xFFE4E8EA).withOpacity(.5);
  static const Color backgroundDark50 = Color(0xFFF1F3F5);

  /// Kdd colors
  static const Color kddColorsGreenGuest = Color(0xFF5BA731);
  static const Color kddColorsYellowFamiliar = Color(0xFFF7A800);
  static const Color kddColorsOrangeFriend = Color(0xFFFF902A);
  static const Color kddColorsTheBestFriend = Color(0xFFFF6B00);

  /// Accent
  static const Color accentViolet = Color(0xFF5F259F);
  static const Color accentOrange = Color(0xFFFF8201);
  static const Color accentYellow = Color(0xFFFFDC04);
  static const Color accentGreenLight = Color(0xFF93D501);
  static const Color accentGreenBlock = Color(0xFF76BB20);

  /// Temporary
  static const Color temporaryLight10 = Color(0xE6FF8403);

  /// Кастомный цвет без нейминга - отсутствует цвет в Figma
  static const Color orangeError = Color(0xFFFF5F00);

  /// Перенесено из [DixyColors] - отсутствует цвет в Figma
  static const Color barrier = Color.fromRGBO(0, 0, 0, 0.25);
  static const Color cardBorder = Color.fromRGBO(0, 0, 0, 0.08);
  static const Color cardShadow = Color.fromRGBO(0, 0, 0, 0.04);
  static const Color grayDark = Color(0xFF6A7177);
}
