import 'package:flutter/cupertino.dart';
import 'package:name_for_baby/l10n/delegate.dart';

class UiFailurePanel extends StatelessWidget {
  /// Панель отображения ошибок
  ///
  /// Принимает
  /// - [errorText] - текст возникшей ошибки
  /// - [onReload] - событие перезвгрузки страницы с ошибкой
  const UiFailurePanel(
    this.errorText, {
    this.onReload,
    super.key,
  });

  final String errorText;
  final VoidCallback? onReload;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(errorText),
        if (onReload != null)
          GestureDetector(
            onTap: onReload,
            child: Text(l10n.toReloadText),
          ),
      ],
    );
  }
}
