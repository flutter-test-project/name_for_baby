import 'package:flutter/cupertino.dart';

class UiEmptyPanel extends StatelessWidget {
  /// Панель отобращения информации пустоты страницы
  ///
  /// Принимает
  /// - [text] - описание
  const UiEmptyPanel({
    super.key,
    required this.text,
  });

  final String text;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(text),
    );
  }
}
