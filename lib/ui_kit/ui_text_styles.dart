import 'package:flutter/material.dart';
import 'package:name_for_baby/ui_kit/ui_colors.dart';
import 'package:name_for_baby/ui_kit/ui_fonts.dart';

/// Стили текста из DIXY UI KIT
/// https://www.figma.com/file/s5pkF4N5Id7N3hV8pShABu/DIXY-UI-KIT?type=design&node-id=1944-6193&mode=design&t=SajMhrDrUlZPC1sM-11
abstract class UiTextStyles {
  const UiTextStyles._();

  static const title1 = TextStyle(
    color: UiColors.globalBlackBase,
    fontFamily: UiFonts.primary,
    fontWeight: FontWeight.w800,
    fontSize: 32.0,
    height: 36.0 / 32.0,
  );

  static const title2 = TextStyle(
    color: UiColors.globalBlackBase,
    fontFamily: UiFonts.primary,
    fontWeight: FontWeight.w800,
    fontSize: 21.0,
    height: 28.0 / 21.0,
  );

  static const body1Bold = TextStyle(
    color: UiColors.globalBlackBase,
    fontFamily: UiFonts.primary,
    fontWeight: FontWeight.w700,
    fontSize: 16.0,
    height: 22.0 / 16.0,
  );

  static const body1SemiBold = TextStyle(
    color: UiColors.globalBlackBase,
    fontFamily: UiFonts.primary,
    fontWeight: FontWeight.w600,
    fontSize: 16.0,
    height: 22.0 / 16.0,
  );

  static const body2Bold = TextStyle(
    color: UiColors.globalBlackBase,
    fontFamily: UiFonts.primary,
    fontWeight: FontWeight.w700,
    fontSize: 14.0,
    height: 20.0 / 14.0,
  );

  static const body2SemiBold = TextStyle(
    color: UiColors.globalBlackBase,
    fontFamily: UiFonts.primary,
    fontWeight: FontWeight.w600,
    fontSize: 14.0,
    height: 20.0 / 14.0,
  );

  static const captionMedium = TextStyle(
    color: UiColors.globalBlackBase,
    fontFamily: UiFonts.primary,
    fontWeight: FontWeight.w500,
    fontSize: 12.0,
    height: 16.0 / 12.0,
  );

  static const captionSemiBold = TextStyle(
    color: UiColors.globalBlackBase,
    fontFamily: UiFonts.primary,
    fontWeight: FontWeight.w600,
    fontSize: 12.0,
    height: 16.0 / 12.0,
  );

  static const smallSemiBold = TextStyle(
    color: UiColors.globalBlackBase,
    fontFamily: UiFonts.primary,
    fontWeight: FontWeight.w600,
    fontSize: 10.0,
    height: 16.0 / 10.0,
  );
}
