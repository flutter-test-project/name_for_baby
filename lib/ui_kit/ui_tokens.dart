import 'package:flutter/material.dart';
import 'package:name_for_baby/ui_kit/ui_colors.dart';

/// Дизайн токены вынесенные в переменных Figma DIXY UI KIT
abstract class UiTokens {
  const UiTokens._();

  static const double roundingRounding12 = 12.0;
  static const double buttonPaddingPadding2 = 2.0;
  static const double buttonPaddingPadding6 = 6.0;
  static const double buttonPaddingPadding12 = 12.0;
  static const double buttonPaddingPadding16 = 16.0;
  static const double formsTextFieldHeight = 56.0;

  static const Color buttonTextTextPrimaryActive = UiColors.globalWhite;
  static const Color buttonTextTextActive = UiColors.accentOrange;
  static const Color buttonTextTextDisabled = UiColors.secondaryMedium;
  static const Color buttonBackgroundBGPrimaryActive = UiColors.accentOrange;
  static const Color buttonBackgroundBGPrimaryDisabled = UiColors.secondaryLight;
  static const Color buttonOutlineOutlineActive = UiColors.accentOrange;
  static const Color buttonOutlineOutlineDisabled = UiColors.secondaryMedium;
}
