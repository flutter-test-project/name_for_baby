import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
export 'package:flutter/services.dart' show SmartQuotesType, SmartDashesType;

class UiTextFormField extends StatelessWidget {
  const UiTextFormField({
    super.key,
    this.controller,
    this.labelText,
    this.focusNode,
    this.decoration,
    this.keyboardType = TextInputType.multiline,
    this.textInputAction,
    this.style,
    this.textAlign = TextAlign.start,
    this.textAlignVertical,
    this.textCapitalization = TextCapitalization.none,
    this.autofocus = false,
    this.readOnly = false,
    this.autocorrect = true,
    this.maxLines = 1,
    this.minLines = 1,
    this.maxLength,
    this.onChanged,
    this.onTap,
    this.onEditingComplete,
    this.validator,
    this.autovalidateMode = AutovalidateMode.onUserInteraction,
    this.inputFormatters,
    this.enabled,
    this.cursorColor,
    this.scrollPadding = const EdgeInsets.all(20.0),
    this.obscureText = false,
    this.obscuringCharacter = '*',
  });

  final TextEditingController? controller;
  final String? labelText;
  final FocusNode? focusNode;
  final InputDecoration? decoration;
  final TextInputType keyboardType;
  final TextInputAction? textInputAction;
  final TextStyle? style;
  final TextAlign textAlign;
  final TextAlignVertical? textAlignVertical;
  final TextCapitalization textCapitalization;
  final bool autofocus;
  final bool readOnly;
  final bool autocorrect;
  final int maxLines;
  final int minLines;
  final int? maxLength;
  final ValueChanged<String>? onChanged;
  final GestureTapCallback? onTap;
  final VoidCallback? onEditingComplete;
  final String? Function(String?)? validator;
  final AutovalidateMode autovalidateMode;
  final List<TextInputFormatter>? inputFormatters;
  final bool? enabled;
  final Color? cursorColor;
  final EdgeInsets scrollPadding;
  final bool obscureText;
  final String obscuringCharacter;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      focusNode: focusNode,
      decoration: decoration ??
          InputDecoration(
            labelText: labelText,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4),
            ),
          ),
      validator: validator,
      autovalidateMode: autovalidateMode,
      keyboardType: keyboardType,
      textInputAction: textInputAction,
      style: style,
      textAlign: textAlign,
      textAlignVertical: textAlignVertical,
      textCapitalization: textCapitalization,
      autofocus: autofocus,
      readOnly: readOnly,
      autocorrect: autocorrect,
      maxLines: maxLines,
      minLines: minLines,
      maxLength: maxLength,
      onChanged: onChanged,
      onTap: onTap,
      onEditingComplete: onEditingComplete,
      inputFormatters: inputFormatters,
      enabled: enabled,
      cursorColor: cursorColor,
      scrollPadding: scrollPadding,
      enableInteractiveSelection: true,
      obscureText: obscureText,
      obscuringCharacter: obscuringCharacter,
    );
  }
}
