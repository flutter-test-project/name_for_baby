extension StringExtension on String {
  /// Метод который делает первый символ текста заглавной буквой
  String capitalize() {
    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }

  /// Метод который делает все символы строчными
  String uppercase() {
    return "${this[0].toLowerCase()}${substring(1).toLowerCase()}";
  }
}
