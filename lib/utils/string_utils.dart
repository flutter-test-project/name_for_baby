class StringUtils {
  /// instead use [StringExtension.isNullOrEmpty]'
  static bool isNullOrEmpty(String? string) {
    return string == null || string.isEmpty;
  }
}
