import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:name_for_baby/features/user_authentication/domain/state/user_authentication_bloc.dart';
import 'package:name_for_baby/routing/router.dart';

import 'l10n/delegate.dart';

class NameForBabyApp extends StatefulWidget {
  const NameForBabyApp({super.key});

  @override
  State<NameForBabyApp> createState() => _NameForBabyAppState();
}

class _NameForBabyAppState extends State<NameForBabyApp> {
  @override
  void initState() {
    context.read<UserAuthenticationBloc>().add(CheckAuthentication());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Flutter Demo',
      localizationsDelegates: const <LocalizationsDelegate>[
        appL10nDelegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      locale: const Locale('ru'),
      supportedLocales: appL10nDelegate.supportedLocales,
      routerConfig: router,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      //home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}
