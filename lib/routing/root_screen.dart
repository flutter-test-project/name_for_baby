import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:go_router/go_router.dart';
import 'package:name_for_baby/features/user_authentication/domain/state/user_authentication_bloc.dart';

class RootScreen extends StatelessWidget {
  const RootScreen({
    super.key,
    required this.navigationShell,
  });

  /// Контейнер для навигационного бара.
  final StatefulNavigationShell navigationShell;

  @override
  Widget build(BuildContext context) {
    return BlocListener<UserAuthenticationBloc, UserAuthenticationState>(
      listener: (context, state) {
        if (state is UserAuthenticationComplete) {
          FlutterNativeSplash.remove();
        }
      },
      child: Scaffold(
        body: navigationShell,
        bottomNavigationBar: BottomNavigationBar(
          /// Лист элементов для нижнего навигационного бара.
          items: _buildBottomNavBarItems,

          /// Текущий индекс нижнего навигационного бара.
          currentIndex: navigationShell.currentIndex,

          /// Обработчик нажатия на элемент нижнего навигационного бара.
          onTap: (index) => navigationShell.goBranch(
            index,
            initialLocation: index == navigationShell.currentIndex,
          ),
        ),
      ),
    );
  }

  // Возвращает лист элементов для нижнего навигационного бара.
  List<BottomNavigationBarItem> get _buildBottomNavBarItems => [
        const BottomNavigationBarItem(
          icon: Icon(Icons.book),
          label: 'Книга имен',
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.child_care),
          label: 'Выбрать имя',
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.person),
          label: 'Профиль',
        ),
      ];
}
