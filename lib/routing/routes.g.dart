// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'routes.dart';

// **************************************************************************
// GoRouterGenerator
// **************************************************************************

List<RouteBase> get $appRoutes => [
      $mainRoute,
      $cardNameRoute,
    ];

RouteBase get $mainRoute => StatefulShellRouteData.$route(
      parentNavigatorKey: MainRoute.$parentNavigatorKey,
      factory: $MainRouteExtension._fromState,
      branches: [
        StatefulShellBranchData.$branch(
          navigatorKey: BookBranch.$navigatorKey,
          routes: [
            GoRouteData.$route(
              path: '/book',
              factory: $BookRouteExtension._fromState,
              routes: [
                GoRouteData.$route(
                  path: 'data_collection',
                  parentNavigatorKey: DataCollectionRoute.$parentNavigatorKey,
                  factory: $DataCollectionRouteExtension._fromState,
                ),
              ],
            ),
          ],
        ),
        StatefulShellBranchData.$branch(
          navigatorKey: SelectNameBranch.$navigatorKey,
          routes: [
            GoRouteData.$route(
              path: '/select_name',
              factory: $SelectNameRouteExtension._fromState,
              routes: [
                GoRouteData.$route(
                  path: 'data_for_game',
                  parentNavigatorKey: DataForGameRoute.$parentNavigatorKey,
                  factory: $DataForGameRouteExtension._fromState,
                  routes: [
                    GoRouteData.$route(
                      path: 'names_for_game',
                      parentNavigatorKey: NamesForGameRoute.$parentNavigatorKey,
                      factory: $NamesForGameRouteExtension._fromState,
                    ),
                    GoRouteData.$route(
                      path: 'game',
                      parentNavigatorKey: GameRoute.$parentNavigatorKey,
                      factory: $GameRouteExtension._fromState,
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
        StatefulShellBranchData.$branch(
          navigatorKey: ProfileBranch.$navigatorKey,
          routes: [
            GoRouteData.$route(
              path: '/profile',
              factory: $ProfileRouteExtension._fromState,
              routes: [
                GoRouteData.$route(
                  path: 'authorization',
                  parentNavigatorKey: AuthorizationRoute.$parentNavigatorKey,
                  factory: $AuthorizationRouteExtension._fromState,
                ),
                GoRouteData.$route(
                  path: 'registration',
                  parentNavigatorKey: RegistrationRoute.$parentNavigatorKey,
                  factory: $RegistrationRouteExtension._fromState,
                ),
                GoRouteData.$route(
                  path: 'favorite_names',
                  parentNavigatorKey: FavoriteNamesRoute.$parentNavigatorKey,
                  factory: $FavoriteNamesRouteExtension._fromState,
                ),
                GoRouteData.$route(
                  path: 'not_favorite_names',
                  parentNavigatorKey: NotFavoriteNamesRoute.$parentNavigatorKey,
                  factory: $NotFavoriteNamesRouteExtension._fromState,
                ),
                GoRouteData.$route(
                  path: 'personal_list_names',
                  parentNavigatorKey:
                      PersonalListNamesRoute.$parentNavigatorKey,
                  factory: $PersonalListNamesRouteExtension._fromState,
                  routes: [
                    GoRouteData.$route(
                      path: 'add_personal_name',
                      parentNavigatorKey:
                          AddPersonalNameRoute.$parentNavigatorKey,
                      factory: $AddPersonalNameRouteExtension._fromState,
                    ),
                  ],
                ),
                GoRouteData.$route(
                  path: 'babies_patronymic',
                  parentNavigatorKey: BabiesPatronymicRoute.$parentNavigatorKey,
                  factory: $BabiesPatronymicRouteExtension._fromState,
                ),
              ],
            ),
          ],
        ),
      ],
    );

extension $MainRouteExtension on MainRoute {
  static MainRoute _fromState(GoRouterState state) => const MainRoute();
}

extension $BookRouteExtension on BookRoute {
  static BookRoute _fromState(GoRouterState state) => const BookRoute();

  String get location => GoRouteData.$location(
        '/book',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $DataCollectionRouteExtension on DataCollectionRoute {
  static DataCollectionRoute _fromState(GoRouterState state) =>
      const DataCollectionRoute();

  String get location => GoRouteData.$location(
        '/book/data_collection',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $SelectNameRouteExtension on SelectNameRoute {
  static SelectNameRoute _fromState(GoRouterState state) =>
      const SelectNameRoute();

  String get location => GoRouteData.$location(
        '/select_name',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $DataForGameRouteExtension on DataForGameRoute {
  static DataForGameRoute _fromState(GoRouterState state) =>
      const DataForGameRoute();

  String get location => GoRouteData.$location(
        '/select_name/data_for_game',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $NamesForGameRouteExtension on NamesForGameRoute {
  static NamesForGameRoute _fromState(GoRouterState state) => NamesForGameRoute(
        sex: _$SexValueEnumMap._$fromName(state.uri.queryParameters['sex']!),
        quantityName: int.parse(state.uri.queryParameters['quantity-name']!),
        selectedNameIds: state.uri.queryParametersAll['selected-name-ids']
                ?.map((e) => e)
                .toList() ??
            const [],
      );

  String get location => GoRouteData.$location(
        '/select_name/data_for_game/names_for_game',
        queryParams: {
          'sex': _$SexValueEnumMap[sex],
          'quantity-name': quantityName.toString(),
          'selected-name-ids': selectedNameIds.map((e) => e).toList(),
        },
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

const _$SexValueEnumMap = {
  SexValue.boy: 'boy',
  SexValue.girl: 'girl',
  SexValue.notDefined: 'not-defined',
};

extension $GameRouteExtension on GameRoute {
  static GameRoute _fromState(GoRouterState state) => const GameRoute();

  String get location => GoRouteData.$location(
        '/select_name/data_for_game/game',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $ProfileRouteExtension on ProfileRoute {
  static ProfileRoute _fromState(GoRouterState state) => const ProfileRoute();

  String get location => GoRouteData.$location(
        '/profile',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $AuthorizationRouteExtension on AuthorizationRoute {
  static AuthorizationRoute _fromState(GoRouterState state) =>
      const AuthorizationRoute();

  String get location => GoRouteData.$location(
        '/profile/authorization',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $RegistrationRouteExtension on RegistrationRoute {
  static RegistrationRoute _fromState(GoRouterState state) =>
      const RegistrationRoute();

  String get location => GoRouteData.$location(
        '/profile/registration',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $FavoriteNamesRouteExtension on FavoriteNamesRoute {
  static FavoriteNamesRoute _fromState(GoRouterState state) =>
      const FavoriteNamesRoute();

  String get location => GoRouteData.$location(
        '/profile/favorite_names',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $NotFavoriteNamesRouteExtension on NotFavoriteNamesRoute {
  static NotFavoriteNamesRoute _fromState(GoRouterState state) =>
      const NotFavoriteNamesRoute();

  String get location => GoRouteData.$location(
        '/profile/not_favorite_names',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $PersonalListNamesRouteExtension on PersonalListNamesRoute {
  static PersonalListNamesRoute _fromState(GoRouterState state) =>
      const PersonalListNamesRoute();

  String get location => GoRouteData.$location(
        '/profile/personal_list_names',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $AddPersonalNameRouteExtension on AddPersonalNameRoute {
  static AddPersonalNameRoute _fromState(GoRouterState state) =>
      const AddPersonalNameRoute();

  String get location => GoRouteData.$location(
        '/profile/personal_list_names/add_personal_name',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $BabiesPatronymicRouteExtension on BabiesPatronymicRoute {
  static BabiesPatronymicRoute _fromState(GoRouterState state) =>
      const BabiesPatronymicRoute();

  String get location => GoRouteData.$location(
        '/profile/babies_patronymic',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension<T extends Enum> on Map<T, String> {
  T _$fromName(String value) =>
      entries.singleWhere((element) => element.value == value).key;
}

RouteBase get $cardNameRoute => GoRouteData.$route(
      path: '/card_name/:nameId',
      parentNavigatorKey: CardNameRoute.$parentNavigatorKey,
      factory: $CardNameRouteExtension._fromState,
    );

extension $CardNameRouteExtension on CardNameRoute {
  static CardNameRoute _fromState(GoRouterState state) => CardNameRoute(
        nameId: state.pathParameters['nameId']!,
        $extra: state.extra as CollectorName?,
      );

  String get location => GoRouteData.$location(
        '/card_name/${Uri.encodeComponent(nameId)}',
      );

  void go(BuildContext context) => context.go(location, extra: $extra);

  Future<T?> push<T>(BuildContext context) =>
      context.push<T>(location, extra: $extra);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location, extra: $extra);

  void replace(BuildContext context) =>
      context.replace(location, extra: $extra);
}
