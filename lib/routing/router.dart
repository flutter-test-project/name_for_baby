import 'package:go_router/go_router.dart';
import 'package:name_for_baby/routing/routes.dart';

final router = GoRouter(
  navigatorKey: globalKey,
  initialLocation: '/select_name',
  routes: $appRoutes,
);
