import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:name_for_baby/app/domain/entities/collector_name.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';
import 'package:name_for_baby/features/add_personal_name/presentation/add_personal_name_screen.dart';
import 'package:name_for_baby/features/babies_patronymic/presentation/babies_patronymic_screen.dart';
import 'package:name_for_baby/features/data_collection/presentation/data_collection_screen.dart';
import 'package:name_for_baby/features/data_for_game/presentation/screens/data_for_game_screen.dart';
import 'package:name_for_baby/features/favorite_names/presentation/favorite_names_screen.dart';
import 'package:name_for_baby/features/game/presentation/screens/game_screen.dart';
import 'package:name_for_baby/features/names_book/presentation/screens/names_book_screen.dart';
import 'package:name_for_baby/features/names_for_game/presentation/names_for_game_screen.dart';
import 'package:name_for_baby/features/not_favorite_names/presentation/not_favorite_names_screen.dart';
import 'package:name_for_baby/features/page_name/presentation/screens/card_name_screen.dart';
import 'package:name_for_baby/features/personal_list_names/presentation/personal_list_names_screen.dart';
import 'package:name_for_baby/features/profile/presentation/screens/profile_screen.dart';
import 'package:name_for_baby/features/select_name/presentation/screens/select_name_screen.dart';
import 'package:name_for_baby/features/user_authentication/presentation/screens/authorization_screen.dart';
import 'package:name_for_baby/features/user_registration/presentation/registration_screen.dart';
import 'package:name_for_baby/routing/root_screen.dart';

part 'routes.g.dart';

final GlobalKey<NavigatorState> globalKey = GlobalKey<NavigatorState>();
final GlobalKey<NavigatorState> _bookKey = GlobalKey<NavigatorState>();
final GlobalKey<NavigatorState> _selectNameKey = GlobalKey<NavigatorState>();
final GlobalKey<NavigatorState> _profileKey = GlobalKey<NavigatorState>();

@TypedStatefulShellRoute<MainRoute>(
  branches: [
    TypedStatefulShellBranch<BookBranch>(
      routes: [
        TypedGoRoute<BookRoute>(path: '/${Url.book}', routes: [
          TypedGoRoute<DataCollectionRoute>(path: 'data_collection'),
        ]),
      ],
    ),
    TypedStatefulShellBranch<SelectNameBranch>(
      routes: [
        TypedGoRoute<SelectNameRoute>(
          path: '/${Url.selectName}',
          routes: [
            TypedGoRoute<DataForGameRoute>(
              path: Url.dataForGame,
              routes: [
                TypedGoRoute<NamesForGameRoute>(
                  path: Url.namesForGame,
                ),
                TypedGoRoute<GameRoute>(path: 'game'),
              ],
            ),
          ],
        ),
      ],
    ),
    TypedStatefulShellBranch<ProfileBranch>(
      routes: [
        TypedGoRoute<ProfileRoute>(
          path: '/${Url.profile}',
          routes: [
            TypedGoRoute<AuthorizationRoute>(path: Url.authorization),
            TypedGoRoute<RegistrationRoute>(path: Url.registration),
            TypedGoRoute<FavoriteNamesRoute>(
              path: Url.favoriteNames,
            ),
            TypedGoRoute<NotFavoriteNamesRoute>(
              path: Url.notFavoriteNames,
            ),
            TypedGoRoute<PersonalListNamesRoute>(
              path: Url.personalListNames,
              routes: [
                TypedGoRoute<AddPersonalNameRoute>(path: Url.addPersonalName),
              ],
            ),
            TypedGoRoute<BabiesPatronymicRoute>(path: Url.babiesPatronymic),
          ],
        ),
      ],
    ),
  ],
)
class MainRoute extends StatefulShellRouteData {
  const MainRoute();

  @override
  Widget builder(
    BuildContext context,
    GoRouterState state,
    StatefulNavigationShell navigationShell,
  ) {
    return RootScreen(navigationShell: navigationShell);
  }

  static final GlobalKey<NavigatorState> $parentNavigatorKey = globalKey;
}

class BookBranch extends StatefulShellBranchData {
  const BookBranch();

  static final $navigatorKey = _bookKey;
}

class SelectNameBranch extends StatefulShellBranchData {
  const SelectNameBranch();

  static final $navigatorKey = _selectNameKey;
}

class ProfileBranch extends StatefulShellBranchData {
  const ProfileBranch();

  static final $navigatorKey = _profileKey;
}

class BookRoute extends GoRouteData {
  const BookRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const NamesBookScreen();
  }
}

class DataCollectionRoute extends GoRouteData {
  const DataCollectionRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const DataCollectionScreen();
  }

  static final $parentNavigatorKey = globalKey;
}

class SelectNameRoute extends GoRouteData {
  const SelectNameRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const SelectNameScreen();
  }
}

class ProfileRoute extends GoRouteData {
  const ProfileRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const ProfileScreen();
  }
}

class AuthorizationRoute extends GoRouteData {
  const AuthorizationRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const AuthorizationScreen();
  }

  static final $parentNavigatorKey = globalKey;
}

class RegistrationRoute extends GoRouteData {
  const RegistrationRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const RegistrationScreen();
  }

  static final $parentNavigatorKey = globalKey;
}

class FavoriteNamesRoute extends GoRouteData {
  const FavoriteNamesRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const FavoriteNamesScreen();
  }

  static final $parentNavigatorKey = globalKey;
}

class NotFavoriteNamesRoute extends GoRouteData {
  const NotFavoriteNamesRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const NotFavoriteNamesScreen();
  }

  static final $parentNavigatorKey = globalKey;
}

class PersonalListNamesRoute extends GoRouteData {
  const PersonalListNamesRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const PersonalListNamesScreen();
  }

  static final $parentNavigatorKey = globalKey;
}

class AddPersonalNameRoute extends GoRouteData {
  const AddPersonalNameRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const AddPersonalNameScreen();
  }

  static final $parentNavigatorKey = globalKey;
}

class BabiesPatronymicRoute extends GoRouteData {
  const BabiesPatronymicRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const BabiesPatronymicScreen();
  }

  static final $parentNavigatorKey = globalKey;
}

/// Роут для открытия экрана по заполненеию даннымх для запуска ишры по выбору имени
class DataForGameRoute extends GoRouteData {
  const DataForGameRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const DataForGameScreen();
  }

  static final $parentNavigatorKey = globalKey;
}

class GameRoute extends GoRouteData {
  const GameRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const GameScreen();
  }

  static final $parentNavigatorKey = globalKey;
}

/// Роут у экрану выбора имен для игры по выбору имен
class NamesForGameRoute extends GoRouteData {
  const NamesForGameRoute({
    required this.sex,
    required this.quantityName,
    required this.selectedNameIds,
  });

  /// пол по которому делаем выборку
  final SexValue sex;

  /// Кол-во имен для учасвия в выборке
  final int quantityName;

  /// Список id выбранных имен для участия в игре
  final List<String> selectedNameIds;

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return NamesForGameScreen(
      sex: sex,
      quantityName: quantityName,
      selectedNameIds: selectedNameIds,
    );
  }

  static final $parentNavigatorKey = globalKey;
}

/// Класс описывающий роут до карточки имени
@TypedGoRoute<CardNameRoute>(path: '/${Url.cardName}')
class CardNameRoute extends GoRouteData {
  const CardNameRoute({
    required this.nameId,
    required this.$extra,
  });

  final String nameId;
  final CollectorName? $extra;

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return CardNameScreen(
      nameId: nameId,
      collectorName: $extra,
    );
  }

  static final $parentNavigatorKey = globalKey;
}

class Url {
  //book
  static const String book = 'book';

  //select_name
  static const String selectName = 'select_name';
  static const String dataForGame = 'data_for_game';
  static const String namesForGame = 'names_for_game';

  //profile
  static const String profile = 'profile';
  static const String authorization = 'authorization';
  static const String registration = 'registration';
  static const String favoriteNames = 'favorite_names';
  static const String notFavoriteNames = 'not_favorite_names';
  static const String personalListNames = 'personal_list_names';
  static const String addPersonalName = 'add_personal_name';
  static const String babiesPatronymic = 'babies_patronymic';

  // general
  static const String cardName = 'card_name/:nameId';
}
