import 'package:flutter/cupertino.dart';
import 'package:name_for_baby/app/presentation/helpers/helpers.dart';

class NfbSvgIcon extends StatelessWidget {
  NfbSvgIcon({
    required this.asset,
    this.size = 40.0,
    this.iconSize = 24.0,
    this.color,
    super.key,
  })  : assert(size >= iconSize),
        assert(asset.endsWith('.svg'));

  final String asset;
  final double size;
  final double iconSize;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: size,
      width: size,
      child: Center(
        child: SizedBox(
          width: iconSize,
          height: iconSize,
          child: ImageAsset(
            asset,
            fit: BoxFit.scaleDown,
            width: iconSize,
            height: iconSize,
            color: color,
          ),
        ),
      ),
    );
  }
}
