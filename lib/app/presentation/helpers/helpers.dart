import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ImageAsset extends StatelessWidget {
  /// Универсальный виджет для изображений. Для svg используется [SvgPicture],
  /// для всех остальных типов [Image]
  const ImageAsset(
    this.path, {
    super.key,
    this.height,
    this.width,
    this.color,
    this.alignment = Alignment.center,
    this.fit,
    this.gaplessPlayback = false,
    this.placeholderBuilder,
  });

  final String path;
  final double? height;
  final double? width;
  final Color? color;
  final Alignment alignment;
  final BoxFit? fit;
  final bool gaplessPlayback;
  final WidgetBuilder? placeholderBuilder;

  @override
  Widget build(BuildContext context) {
    if (path.endsWith('svg')) {
      return SvgPicture.asset(
        path,
        key: key,
        height: height,
        width: width,
        colorFilter: ColorFilter.mode(color ?? Colors.black, BlendMode.srcIn),
        alignment: alignment,
        fit: fit ?? BoxFit.contain,
        placeholderBuilder: placeholderBuilder,
      );
    }

    return Image.asset(
      path,
      key: key,
      height: height,
      width: width,
      color: color,
      alignment: alignment,
      fit: fit,
      gaplessPlayback: gaplessPlayback,
    );
  }
}
