import 'package:flutter/material.dart';

/// Класс описывающий вертикальный отсуп
class IndentVertical extends StatelessWidget {
  /// Вертикальный отсуп
  ///
  /// Принимает:
  /// - [size] - размер вертикального отсупа
  const IndentVertical(
    this.size, {
    super.key,
  });

  final double size;

  @override
  Widget build(BuildContext context) {
    return SizedBox(height: size);
  }
}

/// Класс описывающий горизонтальный отсуп
class IndentHorizontal extends StatelessWidget {
  /// Вертикальный отсуп
  ///
  /// Принимает:
  /// - [size] - размер горизонтального отсупа
  const IndentHorizontal(
    this.size, {
    super.key,
  });

  final double size;

  @override
  Widget build(BuildContext context) {
    return SizedBox(width: size);
  }
}
