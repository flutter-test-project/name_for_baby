import 'package:flutter/material.dart';

/// Примитивы цветов
abstract class NfbColors {
  const NfbColors._();

  static const Color activeColor = Color(0xFF968DD4);

  /// Global
  static const Color globalBlack = Color(0xFF000000);

}
