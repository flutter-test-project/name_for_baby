import 'package:name_for_baby/generated/assets/fonts.gen.dart';

abstract class NfbFonts {
  const NfbFonts._();

  static const String primary = FontFamily.manrope;
  static const String roboto = FontFamily.roboto;
}
