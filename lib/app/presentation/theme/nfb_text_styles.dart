import 'package:flutter/material.dart';
import 'package:name_for_baby/app/presentation/theme/nfb_colors.dart';
import 'package:name_for_baby/app/presentation/theme/nfb_fonts.dart';

/// Стили текста
abstract class NfbTextStyles {
  const NfbTextStyles._();

  static const largeTitle = TextStyle(
    color: NfbColors.globalBlack,
    fontFamily: NfbFonts.roboto,
    fontWeight: FontWeight.w700,
    fontSize: 36.0,
    height: 22.0 / 36.0,
  );

  static const normalTitle = TextStyle(
    color: NfbColors.globalBlack,
    fontFamily: NfbFonts.roboto,
    fontWeight: FontWeight.w700,
    fontSize: 24.0,
    height: 22.0 / 24.0,
  );
  static const body1Bold = TextStyle(
    color: NfbColors.globalBlack,
    fontFamily: NfbFonts.primary,
    fontWeight: FontWeight.w700,
    fontSize: 16.0,
    height: 22.0 / 16.0,
  );
  static const body1Regular = TextStyle(
    color: NfbColors.globalBlack,
    fontFamily: NfbFonts.primary,
    fontWeight: FontWeight.w400,
    fontSize: 16.0,
    height: 22.0 / 16.0,
  );

  static const body1SemiBold = TextStyle(
    color: NfbColors.globalBlack,
    fontFamily: NfbFonts.primary,
    fontWeight: FontWeight.w600,
    fontSize: 16.0,
    height: 22.0 / 16.0,
  );

  static const body2Bold = TextStyle(
    color: NfbColors.globalBlack,
    fontFamily: NfbFonts.primary,
    fontWeight: FontWeight.w700,
    fontSize: 14.0,
    height: 20.0 / 14.0,
  );

  static const body2SemiBold = TextStyle(
    color: NfbColors.globalBlack,
    fontFamily: NfbFonts.primary,
    fontWeight: FontWeight.w600,
    fontSize: 14.0,
    height: 20.0 / 14.0,
  );

  static const body2Medium = TextStyle(
    color: NfbColors.globalBlack,
    fontFamily: NfbFonts.primary,
    fontWeight: FontWeight.w500,
    fontSize: 14.0,
    height: 20.0 / 14.0,
  );

  static const body2Regular = TextStyle(
    color: NfbColors.globalBlack,
    fontFamily: NfbFonts.primary,
    fontWeight: FontWeight.w400,
    fontSize: 14.0,
    height: 20.0 / 14.0,
  );

  static const captionBold = TextStyle(
    color: NfbColors.globalBlack,
    fontFamily: NfbFonts.primary,
    fontWeight: FontWeight.w700,
    fontSize: 12.0,
    height: 16.0 / 12.0,
  );
  static const captionSemiBold = TextStyle(
    color: NfbColors.globalBlack,
    fontFamily: NfbFonts.primary,
    fontWeight: FontWeight.w600,
    fontSize: 12.0,
    height: 16.0 / 12.0,
  );
  static const captionMedium = TextStyle(
    color: NfbColors.globalBlack,
    fontFamily: NfbFonts.primary,
    fontWeight: FontWeight.w500,
    fontSize: 12.0,
    height: 16.0 / 12.0,
  );

  static const smallSemiBold = TextStyle(
    color: NfbColors.globalBlack,
    fontFamily: NfbFonts.primary,
    fontWeight: FontWeight.w600,
    fontSize: 10.0,
    height: 16.0 / 10.0,
  );
}
