import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:name_for_baby/l10n/delegate.dart';

/// Открыть диолошоаое окно с возможностью выбора да/нет
///
/// Принимает
/// - [context] - Контекст
/// - [description] - Описание
/// - [title] - Заглавие
/// - [buttonLeftText] - Текст кнопки слева
/// - [buttonRightText] - Текст кнопки справа
/// - [onLeftPressed] - Дейсвие нажатия на кнопку ответа слева
/// - [onRightPressed] - Дейсвие нажатия на кнопку справа
/// - [isShowIcon] - Флаг показывать ли иконку
Future<void> showOrDialog({
  required BuildContext context,
  required String description,
  String? title,
  String? buttonLeftText,
  String? buttonRightText,
  VoidCallback? onLeftPressed,
  VoidCallback? onRightPressed,
  bool isShowIcon = true,
}) {
  return showDialog<String>(
    context: context,
    builder: (BuildContext context) => OrDialog(
      context: context,
      title: title,
      description: description,
      buttonLeftText: buttonLeftText,
      buttonRightText: buttonRightText,
      onLeftPressed: onLeftPressed,
      onRightPressed: onRightPressed,
      isShowIcon: isShowIcon,
    ),
  );
}

/// Клас описывающий диалоговоее окно с возможностью выбора да/нет
class OrDialog extends StatelessWidget {
  const OrDialog({
    super.key,
    required this.context,
    required this.description,
    this.title,
    this.onLeftPressed,
    this.onRightPressed,
    this.buttonLeftText,
    this.buttonRightText,
    this.isShowIcon = true,
  });

  final BuildContext context;

  /// Заглавие
  final String? title;

  /// Описание
  final String description;

  /// Текст кнопки слева
  final String? buttonLeftText;

  /// Текст кнопки справа
  final String? buttonRightText;

  /// Дейсвие нажатия на кнопку ответа слева
  final VoidCallback? onLeftPressed;

  /// Дейсвие нажатия на кнопку справа
  final VoidCallback? onRightPressed;

  /// Флаг показывать ли иконку
  final bool isShowIcon;

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (isShowIcon) const SizedBox(height: 8),
          if (isShowIcon)
            const Icon(
              Icons.question_mark,
              size: 90,
            ),
          if (title != null) const SizedBox(height: 20),
          if (title != null)
            Text(
              title!,
              style: Theme.of(context).textTheme.headlineSmall?.copyWith(
                    color: Colors.black,
                  ),
            ),
          const SizedBox(height: 8),
          Text(
            description,
            style: Theme.of(context).textTheme.titleSmall?.copyWith(
                  color: Colors.black,
                ),
          ),
        ],
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.pop(context);
            onLeftPressed?.call();
          },
          child: Text(
            buttonLeftText ?? l10n.yes,
          ),
        ),
        TextButton(
          onPressed: () {
            Navigator.pop(context);
            onRightPressed?.call();
          },
          child: Text(
            buttonRightText ?? l10n.no,
          ),
        ),
      ],
    );
  }
}
