import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Future<void> showErrorDialog({
  required BuildContext context,
  required String description,
  String? title,
  String? buttonText,
  VoidCallback? onPressed,
}) {
  return showDialog<String>(
    context: context,
    builder: (BuildContext context) => ErrorDialog(
      context: context,
      title: title,
      description: description,
      buttonText: buttonText,
      onPressed: onPressed,
    ),
  );
}

class ErrorDialog extends StatelessWidget {
  final BuildContext context;
  final String? title;
  final String description;
  final String? buttonText;
  final VoidCallback? onPressed;

  const ErrorDialog({
    super.key,
    required this.context,
    required this.description,
    this.title,
    this.onPressed,
    this.buttonText,
  });

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(height: 8),
          const Icon(
            Icons.error_outline,
            size: 90,
          ),
          const SizedBox(height: 20),
          Text(
            title ?? 'Ошибка',
            style: Theme.of(context).textTheme.headlineSmall?.copyWith(
                  color: Colors.black,
                ),
          ),
          const SizedBox(height: 8),
          Text(
            description,
            style: Theme.of(context).textTheme.titleSmall?.copyWith(
                  color: Colors.black,
                ),
          ),
        ],
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.pop(context);
            onPressed?.call();
          },
          child: Text(
            buttonText ?? 'ОК',
          ),
        ),
      ],
    );
  }
}
