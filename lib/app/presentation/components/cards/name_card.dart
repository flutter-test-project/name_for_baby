import 'package:flutter/material.dart';

/// Класс описыыаюзий карточку имени
class NameCard extends StatelessWidget {
  const NameCard({
    super.key,
    required this.fullName,
    this.onInfoPressed,
    this.onDeletedPressed,
  });

  /// Полное имя
  final String fullName;

  /// Дейсвие при нажатии на иконку информации
  final void Function()? onInfoPressed;

  /// Дейсвие при нажатии на иконку информации
  final void Function()? onDeletedPressed;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 1,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 12),
        child: Row(
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(fullName),
            ),
            if (onInfoPressed != null)
              _IconButton(
                icon: Icons.info_outline,
                onPressed: onInfoPressed,
              ),
            if (onDeletedPressed != null)
              _IconButton(
                icon: Icons.delete,
                onPressed: onDeletedPressed,
              ),
          ],
        ),
      ),
    );
  }
}

/// Информационная иконка
class _IconButton extends StatelessWidget {
  const _IconButton({
    required this.icon,
    this.onPressed,
  });

  /// Надатие на кнопку
  final void Function()? onPressed;

  /// Иконка
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: onPressed,
      icon: Icon(icon),
    );
  }
}
