/// Класс для преобразования объектов из слоя data и domain в друг друга
abstract interface class Mapper<From, To> {
  /// Метод для преобразовании объекта From в объект To
  To serialize(From data);

  /// Метод обратного преобразования объекта To в объект From
  From deserialize(To data);
}