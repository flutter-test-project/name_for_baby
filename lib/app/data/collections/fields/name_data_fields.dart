abstract final class NameDataFields {
  static const id = 'id';
  static const fullName = 'full_name';
  static const churchName = 'church_name';
  static const shortForms = 'short_forms';
  static const synonyms = 'synonyms';
  static const sex = 'sex';
  static const origin = 'origin';
  static const value = 'value';
  static const history = 'history';
  static const popularity = 'popularity';
  static const personality = 'personality';
  static const tags = 'tags';
}