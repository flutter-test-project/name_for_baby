abstract final class CelebrityDataFields {
  static const fullName = 'full_name';
  static const description = 'description';
  static const photo = 'photo';
}
