/// Класс доступных коллекций бд firebase
abstract final class FirestoreCollections {
  static const nameCollection = 'name_collection';
  static const tagCollection = 'tag_collection';
  static const userData = 'user_data';
  static const personalNameCollection = 'personal_name_collection';
}

/// Класс описыающий поля коллекции users
abstract final class FirestoreUsersFields {
  static const favoriteNameIds = 'favorite_name_ids';
  static const notFavoriteNameIds = 'not_favorite_name_ids';
  static const boysPatronymic = 'boys_patronymic';
  static const girlsPatronymic = 'girls_patronymic';
}

abstract final class FirestoreNameCollections {
  static const celebrityCollection = 'celebrities';
}
