import 'package:equatable/equatable.dart';
import 'package:name_for_baby/app/data/collections/fields/tag_data_fields.dart';

/// Класс описывающий тег имени
class TagDto extends Equatable {
  const TagDto({
    required this.id,
    required this.value,
  });

  factory TagDto.fromFirestore(
    Map<String, dynamic> data,
    String docId,
  ) {
    return TagDto(
      id: docId,
      value: data[TagDataFields.value],
    );
  }

  final String id;
  final String value;

  @override
  List<Object?> get props => [
        id,
        value,
      ];
}
