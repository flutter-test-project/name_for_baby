import 'package:equatable/equatable.dart';
import 'package:name_for_baby/app/data/collections/fields/name_data_fields.dart';

/// Класс описывающий имя и его хараетеристики для отображения
class NameDto extends Equatable {
  const NameDto({
    required this.id,
    required this.fullName,
    required this.churchName,
    required this.shortForms,
    required this.synonyms,
    required this.sex,
    required this.origin,
    required this.value,
    required this.history,
    required this.personality,
  });

  factory NameDto.fromFirestore(
    Map<String, dynamic> data,
    String docId,
  ) {
    return NameDto(
      id: docId,
      fullName: data[NameDataFields.fullName],
      churchName: data[NameDataFields.churchName],
      shortForms: data[NameDataFields.shortForms],
      synonyms: data[NameDataFields.synonyms],
      sex: data[NameDataFields.sex],
      origin: data[NameDataFields.origin],
      value: data[NameDataFields.value],
      history: data[NameDataFields.history],
      personality: data[NameDataFields.personality],
    );
  }

  final String id;

  /// Полное имя
  final String fullName;

  /// Церковное имя
  final String churchName;

  /// короткие формы имени
  final String shortForms;

  /// Синонимы имени
  final String synonyms;

  /// пол
  final String sex;

  /// происзождение
  final String origin;

  /// значение
  final String value;

  /// история
  final String history;

  /// черты зараеткра
  final String personality;

  @override
  List<Object?> get props => [
        fullName,
        churchName,
        shortForms,
        synonyms,
        sex,
        origin,
        value,
        history,
        personality,
      ];
}
