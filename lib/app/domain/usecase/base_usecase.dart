abstract class BaseUseCase<Result, Params> {
  Result call(Params params);
}

class NoParams {}
