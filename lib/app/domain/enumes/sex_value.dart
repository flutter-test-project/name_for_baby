/// Пол
enum SexValue {
  boy('Мальчик'),
  girl('Девочка'),
  notDefined('');

  final String description;

  const SexValue(this.description);
}

SexValue? getMarkState(String status) {
  return SexValue.values.where((value) => value.name == status).firstOrNull;
}
