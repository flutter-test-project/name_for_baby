import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';

/// Класс описывающий имя
@immutable
class Name extends Equatable {
  const Name({
    required this.id,
    required this.fullName,
    required this.sex,
  });

  //Уникальный идентификатор модели
  final String id;

  /// Полное имя
  final String fullName;

  /// пол
  final SexValue sex;

  @override
  List<Object?> get props => [
        id,
        fullName,
        sex,
      ];
}
