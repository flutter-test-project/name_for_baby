import 'package:name_for_baby/app/domain/entities/name.dart';
import 'package:name_for_baby/app/domain/enumes/sex_value.dart';

/// Класс описывающий имя добавленное самим пользователя
class InventedName extends Name {
  const InventedName({
    required super.id,
    required super.fullName,
    required this.valueName,
    required super.sex,
  });

  /// Значение имени
  final String valueName;

  factory InventedName.fromJson({
    required Map<String, dynamic> json,
    required String id,
  }) {
    return InventedName(
      id: id,
      fullName: json[MapParameter.fullName] as String,
      valueName: json[MapParameter.valueName] as String,
      sex: getMarkState(json[MapParameter.sex]) ?? SexValue.notDefined,
    );
  }

  /// Метод для превращения модельки PersonalName в мапу
  Map<String, dynamic> toJson() => <String, dynamic>{
        MapParameter.fullName: fullName,
        MapParameter.valueName: valueName,
        MapParameter.sex: sex.name,
      };

  @override
  List<Object?> get props => [
        fullName,
        valueName,
        sex,
      ];
}

/// Класс описывающий ключи значений параметров мапы
class MapParameter {
  static const String fullName = 'full_name';
  static const String valueName = 'value_name';
  static const String sex = 'sex';
}
