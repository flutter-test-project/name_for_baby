import 'package:name_for_baby/app/domain/entities/name.dart';
import 'package:name_for_baby/app/domain/entities/tag.dart';
import 'package:name_for_baby/features/data_collection/domain/entities/celebrity.dart';

/// Класс описывающий имя и его хараетеристики для отображения из коллекции системы
final class CollectorName extends Name {
  const CollectorName({
    required super.id,
    required super.fullName,
    required this.churchName,
    required this.shortForms,
    required this.synonyms,
    required super.sex,
    required this.origin,
    required this.value,
    required this.history,
    required this.personality,
    required this.popularity,
    required this.tags,
    required this.celebrities,
  });

  //Уникальный идентификатор модели
  //final String id;

  /// Полное имя
  //final String fullName;

  /// Церковное имя
  final String churchName;

  /// короткие формы имени
  final String shortForms;

  /// Синонимы имени
  final String synonyms;

  /// пол
  //final SexValue sex;

  /// происзождение и значение
  final String origin;

  /// значение
  final String value;

  /// история
  final String history;

  /// черты зараеткра
  final String personality;

  /// попутярность
  final int popularity;

  /// Теги
  final List<Tag> tags;

  /// Значенитьсти
  final List<Celebrity> celebrities;

  @override
  List<Object?> get props => [
        id,
        fullName,
        churchName,
        shortForms,
        synonyms,
        sex,
        origin,
        value,
        history,
        personality,
        popularity,
        tags,
        celebrities,
      ];
}
