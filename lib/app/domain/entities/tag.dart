import 'package:equatable/equatable.dart';

/// Класс для описания тегов имен
class Tag extends Equatable {
  const Tag({
    required this.id,
    required this.value,
  });

  final String id;
  final String value;

  @override
  List<Object?> get props => [
        id,
        value,
      ];
}
