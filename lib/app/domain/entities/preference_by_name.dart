/// Класс описыающий предпочтения пользователя по именам
class PreferenceByName {
  PreferenceByName({
    required this.favoriteNameIds,
    required this.notFavoriteNameIds,
  });

  /// id понравившихся имен
  final List<String> favoriteNameIds;

  /// id не понравившихся имен
  final List<String> notFavoriteNameIds;
}
