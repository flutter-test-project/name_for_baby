import 'package:equatable/equatable.dart';

/// Класс описываюзий значения отчесв для ребенка разных полов
class BabiesPatronymic extends Equatable {
  const BabiesPatronymic({
    required this.boysValue,
    required this.girlsValue,
  });

  /// Отчесво для мальчиков
  final String boysValue;

  /// Отчесво для девочек
  final String girlsValue;

  @override
  List<Object?> get props => [
        boysValue,
        girlsValue,
      ];
}
