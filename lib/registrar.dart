import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:name_for_baby/features/add_personal_name/domain/state/add_personal_name_bloc.dart';
import 'package:name_for_baby/features/babies_patronymic/data/repositories/babies_patronymic_repository.dart';
import 'package:name_for_baby/features/babies_patronymic/domain/i_babies_patronymic_repository.dart';
import 'package:name_for_baby/features/babies_patronymic/domain/state/babies_patronymic_bloc.dart';
import 'package:name_for_baby/features/babies_patronymic/domain/usecase/get_babies_patronymic_usecase.dart';
import 'package:name_for_baby/features/babies_patronymic/domain/usecase/update_babies_patronymic_usecase.dart';
import 'package:name_for_baby/features/data_for_game/domain/state/data_for_game_bloc.dart';
import 'package:name_for_baby/features/names_for_game/domain/state/names_for_game_bloc.dart';
import 'package:name_for_baby/features/personal_list_names/data/repositories/personal_list_names_repository.dart';
import 'package:name_for_baby/features/personal_list_names/domain/i_personal_list_names_repository.dart';
import 'package:name_for_baby/features/personal_list_names/domain/usecase/add_personal_name_usecase.dart';
import 'package:name_for_baby/features/data_collection/data/repositories/data_collection_repository.dart';
import 'package:name_for_baby/features/data_collection/data/services/data_collection_service.dart';
import 'package:name_for_baby/features/data_collection/domain/i_data_collection_repository.dart';
import 'package:name_for_baby/features/data_collection/domain/services/i_data_collection_service.dart';
import 'package:name_for_baby/features/event_bus/event_bus.dart';
import 'package:name_for_baby/features/favorite_names/domain/state/favorite_names_bloc.dart';
import 'package:name_for_baby/features/names_book/data/repositories/names_book_repository.dart';
import 'package:name_for_baby/features/names_book/data/repositories/old_names_book_repository.dart';
import 'package:name_for_baby/features/names_book/data/services/names_book_firestore_service.dart';
import 'package:name_for_baby/features/names_book/domain/i_names_book_repository.dart';
import 'package:name_for_baby/features/names_book/domain/i_old_names_book_repository.dart';
import 'package:name_for_baby/features/names_book/domain/services/i_names_book_remove_db_service.dart';
import 'package:name_for_baby/features/names_book/domain/state/names_book_bloc.dart';
import 'package:name_for_baby/features/names_book/domain/usecase/delete_last_visible_name_id_usecase.dart';
import 'package:name_for_baby/features/names_book/domain/usecase/get_names_usecase.dart';
import 'package:name_for_baby/features/not_favorite_names/domain/state/not_favorite_bloc.dart';
import 'package:name_for_baby/features/page_name/data/repositories/page_name_repository.dart';
import 'package:name_for_baby/features/page_name/data/services/page_name_firestore_service.dart';
import 'package:name_for_baby/features/page_name/domain/services/i_page_name_remove_db_service.dart';
import 'package:name_for_baby/features/page_name/domain/i_page_name_repository.dart';
import 'package:name_for_baby/features/personal_list_names/domain/state/personal_list_names_bloc.dart';
import 'package:name_for_baby/features/personal_list_names/domain/usecase/delete_personal_name_usecase.dart';
import 'package:name_for_baby/features/personal_list_names/domain/usecase/get_personal_names_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/data/repositories/local_preference_by_name_repository.dart';
import 'package:name_for_baby/features/preference_by_name/data/repositories/preference_by_name_repository.dart';
import 'package:name_for_baby/features/preference_by_name/domain/i_local_preference_by_name_repository.dart';
import 'package:name_for_baby/features/preference_by_name/domain/i_preference_by_name_repository.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/delete_favorite_name_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/delete_not_favorite_name_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/get_favorite_name_ids_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/get_not_favorite_name_ids_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/save_favorite_name_id_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/save_not_favorite_name_id_usecase.dart';
import 'package:name_for_baby/features/profile/data/repositories/profile_repository.dart';
import 'package:name_for_baby/features/profile/domain/i_profile_repository.dart';
import 'package:name_for_baby/features/profile/domain/state/profile_bloc.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/get_count_preference_by_name_usecase.dart';
import 'package:name_for_baby/features/select_name/domain/state/select_name_bloc.dart';
import 'package:name_for_baby/features/user_registration/data/repositories/user_registration_repository.dart';
import 'package:name_for_baby/features/user_registration/domain/i_user_registration_repository.dart';
import 'package:name_for_baby/features/user_registration/domain/usecase/create_user_data_collection_usecase.dart';
import 'package:name_for_baby/features/preference_by_name/domain/usecase/update_preference_by_name_usecase.dart';
import 'package:name_for_baby/features/user_authentication/data/repositories/user_authentication_repository.dart';
import 'package:name_for_baby/features/user_authentication/domain/i_user_authentication_repository.dart';
import 'package:name_for_baby/features/user_authentication/domain/state/user_authentication_bloc.dart';
import 'package:name_for_baby/features/user_secure_data/domain/usecase/clean_user_secure_data_usecase.dart';
import 'package:name_for_baby/features/user_secure_data/domain/usecase/is_user_authentication_usecase.dart';
import 'package:name_for_baby/features/user_secure_data/domain/usecase/save_login_and_password_usecase.dart';
import 'package:name_for_baby/features/user_registration/domain/state/user_registration_bloc.dart';
import 'package:name_for_baby/features/user_secure_data/data/repositories/user_secure_data_repository.dart';
import 'package:name_for_baby/features/user_secure_data/domain/i_user_secure_data_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

final getIt = GetIt.instance;

class AppRegistrar {
  Future<void> register() async {
    // SharedPreferences
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    //FlutterSecureStorage
    const FlutterSecureStorage secureStorage = FlutterSecureStorage();

    // Объявление глобальной шины событий
    final eventBus = EventBus();
    getIt.registerSingleton(eventBus);

    //PreferenceByName
    getIt.registerFactory(
      () => UpdatePreferenceByNameUsecase(
        preferenceByNameRepository: getIt(),
        userSecureDataRepository: getIt(),
        localPreferenceByNameRepository: getIt(),
      ),
    );

    getIt.registerFactory(
      () => GetCountPreferenceByNameUsecase(
        localPreferenceByNameRepository: getIt(),
        preferenceByNameRepository: getIt(),
        userSecureDataRepository: getIt(),
        personalListNamesRepository: getIt(),
      ),
    );

    getIt.registerFactory(
      () => GetFavoriteNamesUsecase(
        localPreferenceByNameRepository: getIt(),
        preferenceByNameRepository: getIt(),
        userSecureDataRepository: getIt(),
      ),
    );
    getIt.registerFactory(
      () => GetNotFavoriteNamesUsecase(
        localPreferenceByNameRepository: getIt(),
        preferenceByNameRepository: getIt(),
        userSecureDataRepository: getIt(),
      ),
    );

    getIt.registerFactory(
      () => SaveFavoriteNameIdUsecase(
        localPreferenceByNameRepository: getIt(),
        preferenceByNameRepository: getIt(),
        userSecureDataRepository: getIt(),
      ),
    );

    getIt.registerFactory(
      () => SaveNotFavoriteNameIdUsecase(
        localPreferenceByNameRepository: getIt(),
        preferenceByNameRepository: getIt(),
        userSecureDataRepository: getIt(),
      ),
    );

    getIt.registerFactory(
      () => DeleteFavoriteNameUsecase(
        localPreferenceByNameRepository: getIt(),
        preferenceByNameRepository: getIt(),
        userSecureDataRepository: getIt(),
      ),
    );

    getIt.registerFactory(
      () => DeleteNotFavoriteNameUsecase(
        localPreferenceByNameRepository: getIt(),
        preferenceByNameRepository: getIt(),
        userSecureDataRepository: getIt(),
      ),
    );

    getIt.registerSingleton<ILocalPreferenceByNameRepository>(
      LocalPreferenceByNameRepository(prefs: prefs),
    );
    getIt.registerSingleton<IPreferenceByNameRepository>(
      PreferenceByNameRepository(),
    );

    /// UserSecureData
    getIt.registerFactory(
      () => IsUserAuthenticationUsecase(
        userSecureDataRepository: getIt(),
      ),
    );

    getIt.registerFactory(
      () => SaveLoginAndPasswordUsecase(
        userSecureDataRepository: getIt(),
      ),
    );

    getIt.registerFactory(
      () => CleanUserSecureDataUsecase(
        userSecureDataRepository: getIt(),
      ),
    );

    getIt.registerSingleton<IUserSecureDataRepository>(
      UserSecureDataRepository(
        storage: secureStorage,
      ),
    );

    //UserAuthentication
    getIt.registerSingleton<IUserAuthenticationRepository>(UserAuthenticationRepository());
    getIt.registerFactory(
      () => UserAuthenticationBloc(
        isUserAuthenticationUsecase: getIt(),
        saveLoginAndPasswordUsecasey: getIt(),
        updateUserDataCollectionUsecase: getIt(),
      ),
    );

    // UserRegistration
    getIt.registerFactory(
      () => CreateUserDataCollectionUsecase(
        userRegistrationRepository: getIt(),
        userSecureDataRepository: getIt(),
        localPreferenceByNameRepository: getIt(),
      ),
    );

    getIt.registerSingleton<IUserRegistrationRepository>(UserRegistrationRepository());

    getIt.registerFactory(
      () => UserRegistrationBloc(
        isUserAuthenticationUsecase: getIt(),
        saveLoginAndPasswordUsecasey: getIt(),
        createUserDataCollectionUsecase: getIt(),
      ),
    );

    // namesBook
    getIt.registerFactory(
      () => GetNamesUsecase(
        namesBlocRepository: getIt(),
      ),
    );
    getIt.registerFactory(
      () => DeleteLastVisibleNameIdUsecase(
        oldNamesBlocRepository: getIt(),
      ),
    );

    getIt.registerSingleton<INamesBookRemoveDbService>(NamesBookFirestoreService());
    getIt.registerSingleton<IOldNamesBookRepository>(
      OldNamesBookRepository(
        removeDbService: getIt(),
        localPreferenceByNameRepository: getIt(),
        userSecureDataRepository: getIt(),
        prefs: prefs,
      ),
    );

    getIt.registerSingleton<INamesBookRepository>(NamesBookRepository());

    getIt.registerFactory(
      () => NamesBookBloc(
        oldNamesBlocRepository: getIt(),
        getFavoriteNameIdsUsecase: getIt(),
        getNotFavoriteNameIdsUsecase: getIt(),
        saveFavoriteNameIdUsecase: getIt(),
        saveNotFavoriteNameIdUsecase: getIt(),
      ),
    );

    // page name
    getIt.registerSingleton<IPageNameRemoveDbService>(PageNameFirestoreService());
    getIt.registerSingleton<IPageNameRepository>(
      PageNameRepository(
        pageNameRemoveDbService: getIt(),
      ),
    );

    //crete new names
    getIt.registerSingleton<IDataCollectionService>(DataCollectionService());
    getIt.registerSingleton<IDataCollectionRepository>(
      DataCollectionRepository(
        dataCollectionService: getIt(),
      ),
    );

    // profile
    getIt.registerSingleton<IProfileRepository>(
      ProfileRepository(
        localPreferenceByNameRepository: getIt(),
        userSecureDataRepository: getIt(),
      ),
    );

    getIt.registerFactory(
      () => ProfileBloc(
        localPreferenceByNameRepository: getIt(),
        getCountPreferenceByNameUsecase: getIt(),
        isUserAuthenticationUsecase: getIt(),
        cleanUserSecureDataUsecase: getIt(),
        deleteLastVisibleNameIdUsecase: getIt(),
        eventBus: eventBus,
      ),
    );

    // favoriteNames
    getIt.registerFactory(
      () => FavoriteNamesBloc(
        getFavoriteNamesUsecase: getIt(),
        getNamesUsecase: getIt(),
        deleteFavoriteNameUsecase: getIt(),
      ),
    );

    // notFavoriteNames
    getIt.registerFactory(
      () => NotFavoriteNamesBloc(
        getNotFavoriteNamesUsecase: getIt(),
        getNamesUsecase: getIt(),
        deleteNotFavoriteNameUsecase: getIt(),
      ),
    );
    // PersonalListNames
    getIt.registerFactory(
      () => GetPersonalNamesUsecase(
        userSecureDataRepository: getIt(),
        personalListNameRepository: getIt(),
      ),
    );
    getIt.registerFactory(
      () => AddPersonalNameUsecase(
        userSecureDataRepository: getIt(),
        personalListNameRepository: getIt(),
      ),
    );
    getIt.registerFactory(
      () => DeletePersonalNameUsecase(
        userSecureDataRepository: getIt(),
        personalListNameRepository: getIt(),
      ),
    );
    getIt.registerSingleton<IPersonalListNamesRepository>(
      PersonalListNamesRepository(),
    );
    getIt.registerFactory(
      () => PersonalListNamesBloc(
        isUserAuthenticationUsecase: getIt(),
        getPersonalNamesUsecase: getIt(),
        deletePersonalNameUsecase: getIt(),
        eventBus: eventBus,
      ),
    );

    // AddPersonalName

    getIt.registerFactory(
      () => AddPersonalNameBloc(
        isUserAuthenticationUsecase: getIt(),
        addPersonalNameUsecase: getIt(),
        getPersonalNamesUsecase: getIt(),
      ),
    );

    // BabiesPatronymic
    getIt.registerFactory(
      () => GetBabiesPatronymicUsecase(
        userSecureDataRepository: getIt(),
        babiesPatronymicRepository: getIt(),
      ),
    );
    getIt.registerFactory(
      () => UpdateBabiesPatronymicUsecase(
        userSecureDataRepository: getIt(),
        babiesPatronymicRepository: getIt(),
      ),
    );
    getIt.registerSingleton<IBabiesPatronymicRepository>(
      BabiesPatronymicRepository(),
    );
    getIt.registerFactory(
      () => BabiesPatronymicBloc(
        getBabiesPatronymicUsecase: getIt(),
        updateBabiesPatronymicUsecase: getIt(),
      ),
    );

    // SelectName
    getIt.registerFactory(
      () => SelectNameBloc(
        isUserAuthenticationUsecase: getIt(),
        eventBus: eventBus,
      ),
    );
    // DataForGame
    getIt.registerFactory(
      () => DataForGameBloc(
        eventBus: eventBus,
      ),
    );

    // NamesForGame
    getIt.registerFactory(
      () => NamesForGameBloc(
        getPersonalNamesUsecase: getIt(),
        getFavoriteNamesUsecase: getIt(),
        getNamesUsecase: getIt(),
      ),
    );
  }
}
