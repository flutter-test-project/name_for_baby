import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:name_for_baby/features/user_authentication/domain/state/user_authentication_bloc.dart';
import 'package:name_for_baby/firebase_options.dart';
import 'package:name_for_baby/registrar.dart';

import 'app.dart';

void main() async{
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  await AppRegistrar().register();
  runApp(BlocProvider<UserAuthenticationBloc>(
    create: (BuildContext context) => getIt(),
    child: const NameForBabyApp(),
  ));
}
