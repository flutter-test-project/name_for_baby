import 'dart:ui';

import 'package:name_for_baby/generated/l10n.dart';

const AppL10nDelegate appL10nDelegate = AppL10nDelegate();

S get l10n => AppL10nDelegate.current!;

class AppL10nDelegate extends AppLocalizationDelegate {
  const AppL10nDelegate();

  static S? current;

  @override
  Future<S> load(Locale locale) async {
    return current = await super.load(locale);
  }
}
